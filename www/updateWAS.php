<?php

$dbh = oci_new_connect('restng', 'Hello123', 'XE');

$uid = strtolower($_REQUEST['uid']);
$app = $_REQUEST['extapp'];

$result = oci_parse($dbh,
    "select count(*) from contactinfo_test_update where contactinfo_uid = '$uid' and contactinfo_app = '$app'");
catchError($dbh);
oci_execute($result, OCI_COMMIT_ON_SUCCESS);
catchError($result);
$resultRow = oci_fetch_array($result, OCI_NUM+OCI_RETURN_NULLS);
catchError($result);

if ($resultRow[0] == 0) {
    $result = oci_parse($dbh,
        "insert into contactinfo_test_update (contactinfo_uid, contactinfo_app)
            values ('$uid', '$app')");
    catchError($dbh);
    oci_execute($result, OCI_COMMIT_ON_SUCCESS);
    catchError($result);
}

if (isset($_REQUEST['reviewPending'])) {
    $reviewPending = $_REQUEST['reviewPending'];

    $result = oci_parse($dbh,
        "update contactinfo_test_update set contactinfo_reviewRequested = '$reviewPending'
            where contactinfo_uid = '$uid' and contactinfo_app = '$app'");
    catchError($dbh);
    oci_execute($result, OCI_COMMIT_ON_SUCCESS);
    catchError($result);
}

if (isset($_REQUEST['complete'])) {
    $complete = $_REQUEST['complete'];
    $result = oci_parse($dbh,
        "update contactinfo_test_update set contactinfo_complete = '$complete',
              contactinfo_completed_date = to_char(sysdate, 'YYYYMMDD')
            where contactinfo_uid = '$uid' and contactinfo_app = '$app'");
    catchError($dbh);
    oci_execute($result, OCI_COMMIT_ON_SUCCESS);
    catchError($result);
}

print "<xml><status>1</status><message>logged</message></xml>";

function catchError($oci) {
    $ociError = oci_error($oci);
    if (is_array($ociError)) {
        print_r($ociError);
        exit;
    }
}