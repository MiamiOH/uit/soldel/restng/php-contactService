<?php

namespace MiamiOH\RestngContactService\Tests\Feature;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\Data\UsesTestData;
use MiamiOH\RESTng\Testing\UsesDatasource;
use MiamiOH\RestngContactService\Tests\BaseTestCase;
use MiamiOH\RestngContactService\Tests\Data\Table\CommunityPledge;

class ResourceCommunityPledgeTest extends BaseTestCase
{
    use UsesDatasource;
    use UsesTestData;

    protected function setUp(): void
    {
        parent::setUp();
        $db = $this->createSqlite3DatabaseHandle();
        $this->useData()
            ->withSqlite($db->getDbConnection())
            ->withTable(CommunityPledge::class)
            ->populate();

        $this->installDatasourceService($db);
        $this->showExceptions();
    }

    public function testUnAuthorized()
    {
        $this->markTestSkipped('must be revisited.');

        $response = $this->getJson('/person/communityPledge/v1/doej?token=badToken');
        $response->assertStatus(APP::API_UNAUTHORIZED);
    }

    public function testCanAuthenticateToGetPledgeInfo()
    {
        $this->markTestSkipped('must be revisited.');

        $this->willAuthenticateUser();
        $this->willAuthorizeUser();
        $response = $this->getJson('/person/communityPledge/v1/doej');
        $response->assertStatus(APP::API_OK);
    }

    public function testCanGetSinglePledgeRecordByUniqueID()
    {
        $this->markTestSkipped('must be revisited.');

        $this->willAuthenticateUser();
        $this->willAuthorizeUser();
        $response = $this->getJson('/person/communityPledge/v1/doej');
        $response->assertStatus(APP::API_OK);
        $response->assertJsonCount(1, 'data');


    }
}
