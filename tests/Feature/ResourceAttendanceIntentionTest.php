<?php

namespace MiamiOH\RestngContactService\Tests\Feature;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\Data\UsesTestData;
use MiamiOH\RESTng\Testing\UsesDatasource;
use MiamiOH\RestngContactService\Tests\BaseTestCase;
use MiamiOH\RestngContactService\Tests\Data\Table\AttendanceIntention;

class ResourceAttendanceIntentionTest extends BaseTestCase
{
    use UsesDatasource;
    use UsesTestData;

    protected function setUp(): void
    {
        parent::setUp();
        $db = $this->createSqlite3DatabaseHandle();
        $this->useData()
            ->withSqlite($db->getDbConnection())
            ->withTable(AttendanceIntention::class)
            ->populate();

        $this->installDatasourceService($db);
        $this->showExceptions();

    }

    public function testUnAuthorized()
    {
        $this->markTestSkipped('must be revisited.');

        $response = $this->getJson('/person/attendanceIntention/v1/doej?token=badToken');
        $response->assertStatus(APP::API_UNAUTHORIZED);
    }

    public function testCanAuthenticateToGetIntention()
    {
        $this->markTestSkipped('must be revisited.');

        $this->willAuthenticateUser();
        $this->willAuthorizeUser();
        $response = $this->getJson('/person/attendanceIntention/v1/doej');

        $response->assertStatus(APP::API_OK);
    }

    public function testGetIntention()
    {
        $this->markTestSkipped('must be revisited.');

        $this->willAuthenticateUser();
        $this->willAuthorizeUser();
        $response = $this->getJson('/person/attendanceIntention/v1/doej');
        $response->assertStatus(APP::API_OK);
        $response->assertJsonCount(1, 'data');
        $response->assertJson([
            'data' => [
                [
                    'intention_id' => '11111',
                    'uniqueId' => 'doej',
                    'choice' => 'delayedStart',
                    'delayedStartDate' => '2020-09-07',
                    'responseTime' => '2020-07-22 18:22:12'
                ]
            ]
        ]);
    }

    public function testCanReturnEmptyDataSet()
    {
        $this->markTestSkipped('must be revisited.');

        $this->willAuthenticateUser();
        $this->willAuthorizeUser();
        $response = $this->getJson('/person/attendanceIntention/v1/doej0');
        $response->assertStatus(APP::API_OK);
        $response->assertJsonCount(0, 'data');
    }

    public function testCanReturnMultipleIntentionsForOneUser()
    {
        $this->markTestSkipped('must be revisited.');

        $this->willAuthenticateUser();
        $this->willAuthorizeUser();
        $response = $this->getJson('/person/attendanceIntention/v1/doej3');
        $response->assertStatus(APP::API_OK);
        $response->assertJsonCount(3, 'data');
    }

    public function testCanCreateOneDelayedStartAttendanceIntention()
    {
        $this->markTestSkipped('must be revisited.');

        $this->willAuthenticateUser();
        $this->willAuthorizeUser();
        $postBody = [
                        "uniqueId"=> "doejC",
                        "choice"=> "delayedStart",
                        "delayedStartDate"=> "2020-08-07",
                        "responseTime"=> "2020-08-10 10:04:11"
                    ];
        $response = $this->postJson('/person/attendanceIntention/v1', $postBody);
    }
}
