<?php

namespace MiamiOH\RestngContactService\Tests;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\Testing\UsesAuthentication;
use MiamiOH\RESTng\Testing\UsesAuthorization;

abstract class BaseTestCase extends TestCase
{
    use UsesAuthentication;
    use UsesAuthorization;

    /**
     * @var App|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $api;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    protected $request;
    /**
     * @var \MiamiOH\RESTng\Connector\DatabaseFactory|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $db;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    protected $dbh;

    protected function setUp():void
    {
        parent::setUp();
        $this->api = $this->createMock(App::class);
        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstrow_assoc','queryall_array','perform'))
            ->getMock();
        $this->db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);


        $this->db->method('getHandle')->willReturn($this->dbh);

        $bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $this->bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $this->bannerUtil->method('getId')->willReturn($bannerId);
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions','getData'))
            ->getMock();

    }



}
