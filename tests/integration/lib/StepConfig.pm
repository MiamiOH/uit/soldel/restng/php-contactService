#!perl

package StepConfig;
use strict;
use warnings;

use Data::Dumper;
use DBI;

use Exporter;
our @ISA = 'Exporter';
our @EXPORT = qw(
                    $server
                    $resourcePaths
                    getResourcePath
                    $authCredentials
                    $requestData
                    $authInfo
                    $dbh
                    $bannerDbh
                    $profileDbh
              );

our $server = 'http://ws/api';

our $resourcePaths = {
    'authentication' => '/authentication/v1',
};

our $authInfo = {
    'path' => '/authentication/v1',
};
sub getResourcePath {
    my $pathKey = shift;
    my $values = shift;

    my $path = '';

    if (defined($resourcePaths->{$pathKey})) {
        $path = $resourcePaths->{$pathKey};

        if ($values && ref($values) eq 'HASH') {
            foreach my $key ( keys %{$values}) {
                $path =~ s/\{$key\}/$values->{$key}/g;
            }
        }
    }

    return $path;
}

our $authCredentials = {
    'CONTACT_WS_USER' => {
                     'username' => 'CONTACT_WS_USER',
                     'password' => 'emergencycontact',
                 },
    'TEST_SELF' => {
                     'username' => 'TEST_SELF',
                     'password' => 'contact',
                   },
    'DOEJ' => {
                     'username' => 'doej',
                     'password' => 'doej',
              },
    'SMITHD' => {
                     'username' => 'smithd',
                     'password' => 'smithd',
                 },
    'ADMIN' => {
                     'username' => 'ADMIN',
                     'password' => 'admin',
                }
};

our $requestData = {
    'status update models' => [
        {
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'reviewStatus' => 'complete',
            'type' => 'student'
        },
        {
            'pidm' => 876253,
            'uniqueId' => 'souther',
            'reviewStatus' => 'complete',
            'type' => 'student'
        },
    ]
};

our $dbh = DBI->connect("DBI:Oracle:XE", "restng", "Hello123") || die DBI->errstr;
our $profileDbh = DBI->connect("DBI:Oracle:XE","safmgr","Hello123") || die DBI->errstr;
our $bannerDbh = DBI->connect("DBI:Oracle:XE","baninst1","Hello123") || die DBI->errstr;
#our $bannerDbh = DBI->connect("DBI:Oracle:XE","system","manager") || die DBI->errstr;

;