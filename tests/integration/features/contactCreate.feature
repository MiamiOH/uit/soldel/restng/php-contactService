Feature: Create emergency contact record of a user
  As a developer using the contact API
  I want to create the emergency contact record of a user
  In order to make sure it works

  Background:
    Given the test data is ready

  Scenario: Require authentication for the contact resource
    Given a REST client
    When I make a POST request for /person/contact/v1/jonesl
    Then the HTTP status code is 401

  Scenario: Create a new emergency contact record for a user
    Given a REST client
    And a token for the CONTACT_WS_USER user
    And a collection of contact records to create in the payload
    And that an emergency contact record for jonesl with pidm "567890" does not exist
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/contact/v1/jonesl?type=emergency
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "201"
    And that an emergency contact record for jonesl with pidm "567890" does exist

  Scenario: Create a new missing person contact record for a user
    Given a REST client
    And a token for the CONTACT_WS_USER user
    And a collection of contact records missing person to create in the payload
    And that an emergency contact record for jonesl with pidm "567890" does not exist
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/contact/v1/jonesl?type=missingPerson
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "201"
    And that an emergency contact record for jonesl with pidm "567890" does exist

  Scenario: Create a new emergency contact record with incorrect details for a user(500 error)
    Given a REST client
    And a token for the CONTACT_WS_USER user
    And a collection of contact records with incorrect details to create in the payload
    And that an emergency contact record for jonesl with pidm "567890" does not exist
    When I give the HTTP Content-type header with the value "application/json"
    And I make a POST request for /person/contact/v1/jonesl?type=emergency
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "500"
