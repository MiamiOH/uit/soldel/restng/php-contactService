# Test the basic api by querying api resources and services
Feature: Get the collection of people due for contact review
  As a concerned entity
  I want to get the list of people due for contact review
  In order to ensure the required people are notified

  Background:
    Given the test data is ready

  Scenario: Require authentication for the reviewPeriod resource
    Given a REST client
    When I make a GET request for /person/contact/reviewPeriod/v1/current
    Then the HTTP status code is 401

  Scenario: Require the type option
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/reviewPeriod/v1/current
    Then the HTTP status code is 500

  Scenario: Require the type option be a supported value (i.e. student)
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/reviewPeriod/v1/current?type=bob
    Then the HTTP status code is 400

  Scenario: Collection contains correct model
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/reviewPeriod/v1/current?type=student
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element state equal to "requested"
    And the subject has an element type equal to "student"
    And I get the people element from the subject
    And I get the first entry with uniqueId equal to "souther" from the subject
    And the subject has an element pidm equal to "876253"
    And the subject has an element uniqueId equal to "souther"
    And the subject has an element reviewStatus equal to "requested"

  Scenario: Review period should include records with no current status
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/reviewPeriod/v1/current?type=student
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And I get the people element from the subject
    And I get the first entry with uniqueId equal to "souther" from the subject
    And the subject has an element pidm equal to "876253"
    And the subject has an element uniqueId equal to "souther"
    And the subject has an element reviewStatus equal to "requested"

  Scenario: Review period should include records with current status
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/reviewPeriod/v1/current?type=student
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And I get the people element from the subject
    And I get the first entry with uniqueId equal to "souther" from the subject
    And the subject has an element pidm equal to "876253"
    And the subject has an element uniqueId equal to "souther"
    And the subject has an element reviewStatus equal to "requested"

  Scenario: Review period should be empty for no active term
    Given a REST client
    And a token for the CONTACT_WS_USER user
    And the term 201710 starts in 10 days
    When I make a GET request for /person/contact/reviewPeriod/v1/current?type=student
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element type equal to "student"
    And the subject has an element requiredStartDate equal to empty
    And the subject has an element requestedStartDate equal to empty
    And the subject has an element state equal to empty
    And I get the people element from the subject
    And the subject contains 0 entries
