# Test the configuration resource
Feature: Get the person contact configuration resource
  As a developer using the Contact API
  I want to get the person contact configuration information
  In order to use the information in the front end application

  Background:
    Given the test data is ready

  Scenario: Require a category to fetch
    Given a REST client
    When I make a GET request for /person/contact/config/v1
    Then the HTTP status code is 500

  Scenario: Require a valid category to fetch
    Given a REST client
    When I make a GET request for /person/contact/config/v1?category=bob
    Then the HTTP status code is 400

  Scenario: Get the UI String configuration
    Given a REST client
    When I make a GET request for /person/contact/config/v1?category=test
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    