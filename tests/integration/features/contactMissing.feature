# Test the basic api by querying api resources and services
Feature: Get the person contact resource
  As a developer using the Contact API
  I want to get the person contact information
  In order to use the information during the review of emergency conctact

  Background:
    Given the test data is ready

#Scenario: Get a missing person contact information with explicit K relation
#Given a REST client
#And a token for the CONTACT_WS_USER user
#When I make a GET request for /person/contact/v1/smithd/missingPerson
#And I dump the response
#Then the HTTP status code is 200
#And the response can be parsed
#And I get the data element from the response object
#And the subject contains 1 entry
#And I get entry 1 from the subject
#And the subject has an element relation equal to "Missing Person Contact"
#And the subject has an element relationCode equal to "K"
#And the subject has an element pidm equal to "987654"
#And the subject has an element priority equal to "2"
#And the subject has an element firstName equal to "Michael"
#And the subject has an element lastName equal to "Caprez"
#And the subject has an element middleName equal to "Joseph"
#And the subject has an element streetAddressLine1 equal to "313 Mill Race Run"
#And the subject has an element streetAddressLine2 equal to empty
#And the subject has an element city equal to "Akron"
#And the subject has an element state equal to "OH"
#And the subject has an element postalCode equal to "44312-1984"
#And the subject has an element phoneNumber equal to "330 3519780"
#And the subject has an element addressType equal to empty

#Scenario: Get a missing person contact information without explicit K relation
#Given a REST client
#And a token for the CONTACT_WS_USER user
#When I make a GET request for /person/contact/v1/doej/missingPerson
#Then the HTTP status code is 200
#And the response can be parsed
#And I get the data element from the response object
#And the subject contains 1 entry
