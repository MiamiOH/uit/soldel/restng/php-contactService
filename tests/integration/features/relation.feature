# Test the basic api by querying api resources and services
Feature: Get the population of types of relationships that are provided to students
  As a developer using the Contact API
  I want to get  list of the relations
  In order to use the list during the selection of emergenecy conctact

  Background:
  Given the test data is ready

  Scenario: Get a list of relations
    Given a REST client
    When I make a GET request for /person/contact/relation/v1
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 22 entries
    And I get entry 1 from the subject
    And the subject has an element relationCode equal to "G"
    And the subject has an element description equal to "Guardian"

