# Test the basic api by querying api resources and services
Feature: Get the person contact resource
  As a developer using the Contact API
  I want to get the person contact information
  In order to use the information during the review of emergency conctact

  Background:
    Given the test data is ready

  Scenario: Require authentication to get the profile list
    Given a REST client
    When I make a GET request for /person/contact/v1/doej
    Then the HTTP status code is 401


  Scenario: Get a person contact information with unauthorized user
    Given a REST client
    And a token for the TEST_SELF user
    When I make a GET request for /person/contact/v1/doej
    Then the HTTP status code is 401

  Scenario: Get a person contact information
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/v1/doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element relation equal to "Father"
    And the subject has an element relationCode equal to "F"
    And the subject has an element pidm equal to "123456"
    And the subject has an element priority equal to "1"
    And the subject has an element firstName equal to "Troy"
    And the subject has an element lastName equal to "Troy"
    And the subject has an element middleName equal to empty
    And the subject has an element phoneNumber equal to "+15131234567"

  Scenario: Get a emergency person contact information with hyphens in phone number
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/v1/souther?type=emergency
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 3 entry
    And I get entry 3 from the subject
    And the subject has an element relation equal to "Guardian"
    And the subject has an element relationCode equal to "G"
    And the subject has an element pidm equal to "876253"
    And the subject has an element priority equal to "4"
    And the subject has an element firstName equal to "Smith"
    And the subject has an element lastName equal to "Smith"
    And the subject has an element middleName equal to empty
    And the subject has an element phoneNumber equal to "+15136777789"


  Scenario: Get a missing person contact information with explicit K relation
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/v1/smithd/missingPerson
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element relation equal to "Missing Person Contact"
    And the subject has an element relationCode equal to "K"
    And the subject has an element pidm equal to "987654"
    And the subject has an element priority equal to "2"
    And the subject has an element firstName equal to "Michael"
    And the subject has an element lastName equal to "Caprez"
    And the subject has an element middleName equal to "Joseph"
    And the subject has an element phoneNumber equal to "+13303519780"

  Scenario: Get a missing person contact information without explicit K relation
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/v1/doej/missingPerson
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry

  Scenario: Get emergency contact only without missing person contacts
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/v1/smithd?type=emergency
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry

  Scenario: Get a person contact information with self authorization
    Given a REST client
    And a token for the DOEJ user
    When I make a GET request for /person/contact/v1/doej
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element relation equal to "Father"
    And the subject has an element relationCode equal to "F"
    And the subject has an element pidm equal to "123456"
    And the subject has an element priority equal to "1"
    And the subject has an element firstName equal to "Troy"
    And the subject has an element lastName equal to "Troy"
    And the subject has an element middleName equal to empty
    And the subject has an element phoneNumber equal to "+15131234567"

  Scenario: Get a person contact Information with incorrect self authorization
    Given a REST client
    And a token for the DOEJ user
    When I make a GET request for /person/contact/v1/smithd
    Then the HTTP status code is 401
