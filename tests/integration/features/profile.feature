Feature: Get the profile of Miami students to be provided to the emergency contact data

  As a developer using the Contact API
  I want to get  list of the profiles
  In order to use the list during the selection of emergency data

  Background:
    Given the test data is ready

  Scenario: Require authentication to get the profile list
    Given a REST client
    When I make a GET request for /person/contact/profile/v1
    Then the HTTP status code is 401

  Scenario: Get profile collection with unauthorized user
    Given a REST client
    And a token for the TEST_SELF user
    When I make a GET request for /person/contact/profile/v1
    Then the HTTP status code is 401

  Scenario: Get profile for user
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/profile/v1/smithd
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element pidm equal to "987654"
    And the subject has an element addDate equal to "2015-01-10"
    And the subject has an element reviewDate equal to empty
    And the subject has an element reviewStatus equal to "requested"
    And the subject has an element primaryAffiliationCode equal to empty
    And the subject has an element primaryAffiliationDescription equal to empty
    And the subject has an element primaryAffiliationType equal to "other"
    And the subject has an element offCampusStudent equal to "true"

  Scenario: Get profile collection
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/profile/v1?pidm=987654,876253
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 2 entries

  Scenario: Get profile for user with primary affiliation as Student
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/profile/v1/souther
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element pidm equal to "876253"
    And the subject has an element primaryAffiliationCode equal to "und"
    And the subject has an element primaryAffiliationDescription equal to "student"
    And the subject has an element primaryAffiliationType equal to "student"
    And the subject has an element offCampusStudent equal to "true"

  Scenario: Get profile for user with type as commuter
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/profile/v1/tststd39
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element pidm equal to "990038"
    And the subject has an element commuter equal to "true"

  Scenario: Get profile for user with type as commuter for older term
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/profile/v1/tststd40
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element pidm equal to "990039"
    And the subject has an element commuter equal to "false"  

  Scenario: Get profile for user with campus code as oxford
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/profile/v1/souther
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element pidm equal to "876253"
    And the subject has an element campusCode equal to "oxf"  

