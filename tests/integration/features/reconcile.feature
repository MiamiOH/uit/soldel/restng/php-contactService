# Test the configuration resource
Feature: Reconcile review period state and people with current status
  As an administrator
  I want state given by the review period to be accurately reflected in individual status
  In order to show the review request to the correct people

  Background:
    Given the test data is ready

  Scenario: Run the student reconciliation before the term review start
    Given the student reconciliation config is ready
    And the term 201710 starts in 10 days
    And there are 3 outstanding requested reviews
    And a requested activity record for souther does exist
    When the reconciliation script is run for students
    Then an activity record reviewed today for souther with status "cancelled" does exist
    And there are 0 outstanding requested reviews

  Scenario: Run the student reconciliation after the term review start
    Given the student reconciliation config is ready
    And the term 201710 starts in 1 days
    And there are 3 outstanding requested reviews
    And a requested activity record for souther does exist
    When the reconciliation script is run for students
    Then there is an outstanding review for souther
    And there are 39 outstanding requested reviews

  Scenario: Run the student reconciliation after the term required review start
    Given the student reconciliation config is ready
    And the term 201710 starts in -15 days
    And there are 3 outstanding requested reviews
    And a requested activity record for souther does exist
    When the reconciliation script is run for students
    Then there is an outstanding review for souther
    And there are 39 outstanding required reviews

  Scenario: Run the student reconciliation after the term ends
    Given the student reconciliation config is ready
    And the term 201710 starts in -95 days
    And the term 201710 ends in -5 days
    And there are 3 outstanding requested reviews
    And a requested activity record for souther does exist
    When the reconciliation script is run for students
    Then there are 0 outstanding required reviews

  Scenario: Run the student reconciliation with a removed student
    Given the student reconciliation config is ready
    And the term 201710 starts in 1 days
    And souther has cancelled enrollment for term 201710
    And a requested activity record for souther does exist
    When the reconciliation script is run for students
    Then an activity record reviewed today for souther with status "cancelled" does exist

  Scenario: Run the student reconciliation with a new student
    Given the student reconciliation config is ready
    And the term 201710 starts in 1 days
    And howardt has enrolled for term 201710
    And a requested activity record for howardt does not exist
    When the reconciliation script is run for students
    Then there is an outstanding review for howardt

  Scenario: Run the student reconciliation with a completed student
    Given the student reconciliation config is ready
    And the term 201710 starts in 1 days
    And souther completed the review yesterday
    When the reconciliation script is run for students
    Then the log does not contain an update for souther

  Scenario: Run the student reconciliation with a cancelled student
    Given the student reconciliation config is ready
    And the term 201710 starts in 1 days
    And souther cancelled the review yesterday
    When the reconciliation script is run for students
    Then the log contains an update for souther to requested

  Scenario: Run the student reconciliation with a cancelled student after required point
    Given the student reconciliation config is ready
    And the term 201710 starts in -15 days
    And souther cancelled the review yesterday
    When the reconciliation script is run for students
    Then the log contains an update for souther to required
