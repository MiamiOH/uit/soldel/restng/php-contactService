Feature: Delete emergency contact record of a user
  As a developer using the contact API
  I want to update the emergency contact record of a user
  In order to make sure it works

  Background:
    Given the test data is ready

  Scenario: Require authentication for the contact resource
    Given a REST client
    When I make a DELETE request for /person/contact/v1/smithd
    Then the HTTP status code is 401

  Scenario: Delete a new emergency contact record for a user
    Given a REST client
    And a token for the CONTACT_WS_USER user
    And a collection of contact records to delete in the payload
    When I give the HTTP Content-type header with the value "application/json"
    And I make a DELETE request for /person/contact/v1/smithd
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "200"
