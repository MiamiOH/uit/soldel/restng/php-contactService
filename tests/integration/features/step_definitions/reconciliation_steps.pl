#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

my $studentConfig = '../config/integration_reconcile.txt';

Given q/the student reconciliation config is ready/, sub {

    my $error = '';
    open CONFIG, '>' . $studentConfig or $error = $!;
    ok(!$error, "Opened $studentConfig: $error");
    print CONFIG q{log_dir = ../logs
log = process_log.txt
log_history = 5
server = http://ws/api
type = student
username = CONTACT_WS_USER
password = emergencycontact
};
    close CONFIG;
};

When qr/the reconciliation script is run for students/, sub {

    my $results = `../bin/reconcile.pl --config $studentConfig --verbose`;

    S->{'reconcileResults'} = $results;

};

Then qr/I look at the log/, sub {
    print "\n" . S->{'reconcileResults'} . "\n";
};

Then qr/there are (\d+) outstanding (\S+) reviews/, sub {
    my $expectedCount = $1;
    my $status = $2;

	my($realCount) = $bannerDbh->selectrow_array(q{
		select count(*)
			from safmgr.person_contact_activity
			where review_status = ?
			    and review_date is null
		}, undef, $status);

	ok($realCount == $expectedCount, "Expected $expectedCount $status records, but found $realCount");
};

Then qr/the log (does not contain|contains) an update for (\S+)/, sub {
    my $spec = $1;
    my $uid = $2;

    my $check = 0;
    if ($spec eq 'contains') {
        $check = 1 if (S->{'reconcileResults'} =~ /Review state for $uid was successfully updated/s);
    } else {
        $check = 1 unless (S->{'reconcileResults'} =~ /Review state for $uid was successfully updated/s);
    }

    ok($check, "Log $spec a record for $uid");
};

Then qr/the log contains an update for (\S+) to (\S+)/, sub {
    my $uid = $1;
    my $status = $2;

    my $check = 0;
    $check = 1 if (S->{'reconcileResults'} =~ /Review state for $uid was successfully updated \($status\)/s);

    ok($check, "Log contains a record for $uid being updated to $status");
}
