#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given q/the WAS test data is ready/, sub {

    $dbh->do(q{
        delete from contactinfo_test_update
        });

};

Then qr/the WAS (optional|required) update was called with (\S+) "(\S+)" for (\S+)/, sub {
    my $appType = $1;
    my $attribute = $2;
    my $value = $3;
    my $uid = $4;

    my $app = '';
    $app = 'StudentContactInformation' if ($appType eq 'optional');
    $app = 'StudentContactInformationRequired' if ($appType eq 'required');

    my $column  = '';
    $column = 'contactinfo_reviewRequested' if ($attribute eq 'reviewRequested');
    $column = 'contactinfo_complete' if ($attribute eq 'complete');

    my($exists) = $dbh->selectrow_array(qq{
        select count(*)
            from contactinfo_test_update
            where contactinfo_uid = lower(?)
                and contactinfo_app = ?
                and $column = '$value'
        }, undef, $uid, $app);

    ok($exists, "WAS attribute $attribute was set to $value for $uid for app $app");

};

Then qr/the WAS (optional|required) update was not called for (\S+)/, sub {
    my $appType = $1;
    my $uid = $2;

    my $app = '';
    $app = 'StudentContactInformation' if ($appType eq 'optional');
    $app = 'StudentContactInformationRequired' if ($appType eq 'required');

    my($exists) = $dbh->selectrow_array(qq{
        select count(*)
            from contactinfo_test_update
            where contactinfo_uid = lower(?)
                and contactinfo_app = ?
        }, undef, $uid, $app);

    ok(!$exists, "WAS optional update was not called for $uid");

};

Then qr/the WAS (optional|required) update was called with completed today for (\S+)/, sub {
    my $appType = $1;
    my $uid = $2;

    my $app = '';
    $app = 'StudentContactInformation' if ($appType eq 'optional');
    $app = 'StudentContactInformationRequired' if ($appType eq 'required');

    my($exists) = $dbh->selectrow_array(qq{
        select count(*)
            from contactinfo_test_update
            where contactinfo_uid = lower(?)
                and contactinfo_app = ?
                and contactinfo_completed_date = to_char(sysdate, 'YYYYMMDD')
        }, undef, $uid, $app);

    ok($exists, "WAS optional data was marked complete on current date for $uid");

};

