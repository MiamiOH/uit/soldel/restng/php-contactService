#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given q/the test data is ready/, sub {

    my $idData = loadDataFromFile('ids');
    my $relationData = loadDataFromFile('relation');
    my $contactData = loadDataFromFile('contact');
    my $profileData = loadDataFromFile('profile');
    my $idenData = loadDataFromFile('spriden');
    my $configData = loadDataFromFile('config');
    my $termData = loadDataFromFile('terms');
    my $studentData = loadDataFromFile('students');
    my $registrationData = loadDataFromFile('registrations');
    my $sectionAttrData = loadDataFromFile('sectionAttr');
    my $housingData = loadDataFromFile('housing');
    my $offCampusData = loadDataFromFile('offCampus');
    my $studentAttributes = loadDataFromFile('studentAttribute');
    my $registrationVal = loadDataFromFile('registrationValidation');

    foreach my $table (qw(szbuniq STVRELT SPREMRG spriden stvterm sgbstdn sfrstcr ssrattr slrrasg sgrsatt stvrsts)) {
        $bannerDbh->do(qq{
                delete from $table
            });
    }

    foreach my $table (qw(person_contact_activity)) {
        $profileDbh->do(qq{
                delete from $table
            });
    }

    $bannerDbh->do(q{
            alter session set nls_date_format='YYYY-MM-DD'
        });

    foreach my $term (@{$termData}) {
        my $startDate = $term->{'startDate'} =~ /sysdate/i ? $term->{'startDate'} : "'" . $term->{'startDate'} . "'";
        my $endDate = $term->{'endDate'} =~ /sysdate/i ? $term->{'endDate'} : "'" . $term->{'endDate'} . "'";

        $bannerDbh->do(qq{
            insert into stvterm (stvterm_code, stvterm_desc, stvterm_start_date, stvterm_end_date,
                    stvterm_activity_date)
                values (?, ?, $startDate, $endDate, sysdate)
            }, undef, $term->{'termCode'}, $term->{'description'});

    }

    $bannerDbh->do(q{
            alter session set nls_date_format='DD-MON-YY'
        });

    foreach my $record (@{$idData}) {
        $bannerDbh->do(q{
            insert into szbuniq (szbuniq_pidm, szbuniq_banner_id, szbuniq_unique_id)
                values (?, ?, ?)
            }, undef, $record->{'pidm'}, $record->{'banner_id'}, uc $record->{'uid'});
    }

    foreach my $record (@{$relationData}) {
        $bannerDbh->do(q{
            insert into STVRELT (STVRELT_CODE,STVRELT_DESC,STVRELT_ACTIVITY_DATE,STVRELT_SEVIS_EQUIV,STVRELT_SURROGATE_ID,
            STVRELT_VERSION,STVRELT_USER_ID,STVRELT_DATA_ORIGIN,STVRELT_VPDI_CODE)
                values (?,?,?,?,?,?,?,?,?)
            }, undef, $record->{'RELATION_CODE'}, $record->{'RELATION_DESC'},$record->{'RELATION_ACTIVITY_DATE'},$record->{'RELATION_SEVIS_EQUIV'},
            $record->{'RELATION_SURROGATE_ID'},$record->{'RELATION_VERSION'},$record->{'RELATION_USER_ID'},$record->{'RELATION_DATA_ORIGIN'},
            $record->{'RELATION_VPDI_CODE'});


    }

    foreach my $record (@{$profileData}) {
        my $reviewDate = 'null';
         if($record->{'REVIEW_DATE'}){
            $reviewDate = $record->{'REVIEW_DATE'} =~ /sysdate/i ? $record->{'REVIEW_DATE'} : "'" . $record->{'REVIEW_DATE'} . "'";
         } 
         
        $profileDbh->do(qq{

            insert into person_contact_activity (status_id, PIDM, ADD_DATE, REVIEW_DATE, REVIEW_STATUS, review_type)
                values (person_contact_activity_seq.nextval, ?, ?, $reviewDate, ?, ?)
            }, undef, $record->{'PIDM'}, $record->{'ADD_DATE'}, $record->{'REVIEW_STATUS'},
                $record->{'TYPE'});
    }

    foreach my $record (@{$contactData}) {
        $bannerDbh->do(q{
            insert into SPREMRG (SPREMRG_PIDM,SPREMRG_PRIORITY,SPREMRG_LAST_NAME,SPREMRG_FIRST_NAME,SPREMRG_MI,
            SPREMRG_STREET_LINE1,SPREMRG_STREET_LINE2,SPREMRG_STREET_LINE3,SPREMRG_CITY,SPREMRG_STAT_CODE,SPREMRG_NATN_CODE,SPREMRG_ZIP,SPREMRG_PHONE_AREA,
            SPREMRG_PHONE_NUMBER,SPREMRG_PHONE_EXT,SPREMRG_RELT_CODE,SPREMRG_ACTIVITY_DATE,SPREMRG_ATYP_CODE,SPREMRG_DATA_ORIGIN,SPREMRG_USER_ID,SPREMRG_SURNAME_PREFIX,
            SPREMRG_CTRY_CODE_PHONE,SPREMRG_HOUSE_NUMBER,SPREMRG_STREET_LINE4,SPREMRG_SURROGATE_ID,SPREMRG_VERSION,SPREMRG_VPDI_CODE)
                values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
            }, undef, $record->{'CONTACT_PIDM'}, $record->{'CONTACT_PRIORITY'},$record->{'CONTACT_LAST_NAME'},$record->{'CONTACT_FIRST_NAME'},
            $record->{'CONTACT_MI'},$record->{'CONTACT_STREET_LINE1'},$record->{'CONTACT_STREET_LINE2'},$record->{'CONTACT_STREET_LINE3'},
            $record->{'CONTACT_CITY'}, $record->{'CONTACT_STAT_CODE'}, $record->{'CONTACT_NATN_CODE'}, $record->{'CONTACT_ZIP'}
            , $record->{'CONTACT_PHONE_AREA'}, $record->{'CONTACT_PHONE_NUMBER'}, $record->{'CONTACT_PHONE_EXT'}, $record->{'CONTACT_RELT_CODE'}
            , $record->{'CONTACT_ACTIVITY_DATE'}, $record->{'CONTACT_ATYP_CODE'}, $record->{'CONTACT_DATA_ORIGIN'}, $record->{'CONTACT_USER_ID'}
            , $record->{'CONTACT_SURNAME_PREFIX'}, $record->{'CONTACT_CTRY_CODE_PHONE'}, $record->{'CONTACT_HOUSE_NUMBER'}, $record->{'CONTACT_STREET_LINE4'}
            , $record->{'CONTACT_SURROGATE_ID'}, $record->{'CONTACT_VERSION'}, $record->{'CONTACT_VPDI_CODE'});
    }

    #This table is used by Banner API GB_EMERGENCY_CONTACT.P_CREATE() to validate the PIDM value.
    foreach my $record (@{$idenData}) {
        $bannerDbh->do(q{
            insert into spriden (spriden_pidm, spriden_id, spriden_last_name, spriden_first_name)
                values (?, ?, ?, ?)
            }, undef, $record->{'SPRIDEN_PIDM'}, $record->{'SPRIDEN_ID'}, $record->{'SPRIDEN_LAST_NAME'},
             $record->{'SPRIDEN_FIRST_NAME'});
    }

    foreach my $record (@{$studentData}) {
        $bannerDbh->do(q{
            insert into sgbstdn (sgbstdn_pidm, sgbstdn_term_code_eff, sgbstdn_stst_code, sgbstdn_styp_code,
                sgbstdn_camp_code, sgbstdn_levl_code, sgbstdn_degc_code_1, sgbstdn_majr_code_1, sgbstdn_activity_date, 
                sgbstdn_coll_code_1, sgbstdn_program_1)
                values (?, ?, ?, ?, ?, ?, ?, ?, sysdate, ?, ?)
            }, undef, $record->{'pidm'}, $record->{'term'}, $record->{'status'}, $record->{'type'},
                $record->{'campus'}, $record->{'level'}, $record->{'degree'}, $record->{'major'}, $record->{'college'},
                $record->{'program'});
    }

    foreach my $record (@{$registrationData}) {
        $bannerDbh->do(q{
            insert into sfrstcr (sfrstcr_pidm, sfrstcr_term_code, sfrstcr_crn, sfrstcr_reg_seq,
                sfrstcr_add_date, sfrstcr_activity_date, sfrstcr_levl_code, sfrstcr_camp_code,
                sfrstcr_rsts_code, sfrstcr_credit_hr)
                values (?, ?, ?, ?, sysdate, sysdate, ?, ?, ?, ?)
            }, undef, $record->{'pidm'}, $record->{'term'}, $record->{'crn'}, $record->{'sequence'},
                $record->{'level'}, $record->{'campus'}, $record->{'rstsCode'}, $record->{'credits'});
    }

    foreach my $record (@{$sectionAttrData}) {
        $bannerDbh->do(q{
            insert into ssrattr (ssrattr_term_code, ssrattr_crn, ssrattr_attr_code, ssrattr_activity_date)
                values (?, ?, ?, sysdate)
            }, undef, $record->{'term'}, $record->{'crn'}, $record->{'code'});
    }

    foreach my $record (@{$housingData}) {
        $bannerDbh->do(q{
            insert into slrrasg (slrrasg_pidm, slrrasg_term_code, slrrasg_bldg_code, slrrasg_room_number,
                    slrrasg_rrcd_code, slrrasg_begin_date, slrrasg_end_Date, slrrasg_total_days,
                    slrrasg_total_months, slrrasg_total_terms, slrrasg_ascd_code, slrrasg_ascd_date,
                    slrrasg_onl_or_bat, slrrasg_activity_date, slrrasg_ar_ind, slrrasg_assess_needed)
                values (?, ?, ?, ?, ?, sysdate + 7, sysdate + 110, 110, 3.5, 1, ?, sysdate, 'B', sysdate,
                    'N', 'N')
            }, undef, $record->{'pidm'}, $record->{'term'}, $record->{'building'}, $record->{'room'},
                $record->{'rrcdCode'}, $record->{'ascdCode'});
    }
    
    foreach my $record (@{$studentAttributes}) {
        $bannerDbh->do(q{
            insert into SGRSATT (SGRSATT_PIDM, SGRSATT_TERM_CODE_EFF, SGRSATT_ATTS_CODE, SGRSATT_ACTIVITY_DATE,
                SGRSATT_STSP_KEY_SEQUENCE, SGRSATT_SURROGATE_ID, SGRSATT_VERSION)
                values (?, ?, ?, ?, ?, ?, ?)
            }, undef, $record->{'PIDM'}, $record->{'TERM_CODE_EFF'}, $record->{'ATTS_CODE'}, $record->{'ACTIVITY_DATE'},
                $record->{'STSP_KEY_SEQUENCE'}, $record->{'SURROGATE_ID'}, $record->{'VERSION'});
    }

    foreach my $record (@{$registrationVal}) {
        $bannerDbh->do(q{
            insert into STVRSTS (STVRSTS_CODE,
                                 STVRSTS_DESC,
                                 STVRSTS_ENTERABLE_IND,
                                 STVRSTS_INCL_SECT_ENRL,
                                 STVRSTS_INCL_ASSESS,
                                 STVRSTS_AUTO_GRADE,
                                 STVRSTS_GRADABLE_IND,
                                 STVRSTS_ACTIVITY_DATE,
                                 STVRSTS_WAIT_IND,
                                 STVRSTS_SYSTEM_REQ_IND,
                                 STVRSTS_VOICE_TYPE,
                                 STVRSTS_SB_PRINT_IND,
                                 STVRSTS_WITHDRAW_IND,
                                 STVRSTS_WEB_IND,
                                 STVRSTS_SWAP,
                                 STVRSTS_EXTENSION_IND,
                                 STVRSTS_ATTEMPT_HR_IND,
                                 STVRSTS_INCL_TMST_IND,
                                 STVRSTS_ACTION_DESC,
                                 STVRSTS_SURROGATE_ID,
                                 STVRSTS_VERSION,
                                 STVRSTS_USER_ID,
                                 STVRSTS_DATA_ORIGIN,
                                 STVRSTS_VPDI_CODE)
                values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            }, undef, $record->{'CODE'}, $record->{'DESC'}, $record->{'ENTERABLE_IND'}, $record->{'INCL_SECT_ENRL'}, $record->{'INCL_ASSESS'}, $record->{'AUTO_GRADE'},
            $record->{'GRADABLE_IND'}, $record->{'ACTIVITY_DATE'}, $record->{'WAIT_IND'}, $record->{'SYSTEM_REQ_IND'}, $record->{'VOICE_TYPE'}, $record->{'SB_PRINT_IND'},
            $record->{'WITHDRAW_IND'}, $record->{'WEB_IND'}, $record->{'SWAP'}, $record->{'EXTENSION_IND'}, $record->{'ATTEMPT_HR_IND'}, $record->{'INCL_TMST_IND'},
            $record->{'ACTION_DESC'}, $record->{'SURROGATE_ID'}, $record->{'VERSION'}, $record->{'USER_ID'}, $record->{'DATA_ORIGIN'}, $record->{'VPDI_CODE'});
    }

    foreach my $cmItem (@{$configData}) {
        $dbh->do(q{
            delete from cm_config
            where config_application = ? and config_key = ?
            }, undef, $cmItem->{'application'}, $cmItem->{'key'});

        $dbh->do(q{
            insert into cm_config (config_application, config_key,
                config_data_type, config_data_structure, config_category,
                config_desc, config_value, activity_date, activity_user)
            values (?, ?, ?, ?, ?, ?, ?, sysdate, 'doej')
            }, undef, $cmItem->{'application'}, $cmItem->{'key'}, $cmItem->{'data_type'},
                $cmItem->{'data_structure'}, $cmItem->{'category'}, $cmItem->{'desc'},
                $cmItem->{'value'});
    }

    # Ensure our static token is present
        $dbh->do(q{
            delete from ws_authentication_token
            where token = ?
            }, undef, 'w9cJTKsjhumFoFXzQ5fzw9XQBc');

        $dbh->do(q{
            insert into ws_authentication_token (token, username, issued_time, expiration_time, credential_source)
            values (?, ?, sysdate, sysdate + 700, 'local')
            }, undef, 'w9cJTKsjhumFoFXzQ5fzw9XQBc', 'souther');

        foreach my $table (qw(offcampus_status)) {
            $dbh->do(qq{
                 delete from $table
            });
        }

        foreach my $record (@{$offCampusData}) {
            $dbh->do(q{
                insert into offcampus_status (pidm, term, response)
                    values (?, ?, ?)
                }, undef, $record->{'pidm'}, $record->{'term'}, uc $record->{'response'});
        }

};

sub loadDataFromFile {
    my $fileName = shift;

    my $data = [];

    open FILE, "../sql/sampleData/$fileName.txt" or die "Couldn't open ../sql/sampleData/$fileName.txt: $!";

    my $names = <FILE>;
    chomp $names;

    my @fieldNames = split("\t", $names);
    while (<FILE>) {
        chomp;
        my @values = split("\t");

        my $record = {};
        for (my $i = 0; $i < scalar(@fieldNames); $i++) {
            $record->{$fieldNames[$i]} = $values[$i];
        }
        push(@{$data}, $record);
    }

    close FILE;

    return $data;
}