#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

Given qr/an activity record for (\S+) with status "(\S+)" (.*)/, sub {
	my $uid = $1;
	my $value = $2;
	my $spec = $3;
	my $status = $3 eq 'does exist' ? 1 : 0;

	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from safmgr.person_contact_activity inner join szbuniq
			    on szbuniq_pidm = safmgr.person_contact_activity.pidm
			where szbuniq_unique_id = upper(?)
			    and review_status = ?
		}, undef, $uid, $value);

	ok($realStatus == $status, "An activity record for $uid with status $value $spec");

};

Given qr/a (\S+) activity record for (\S+) (does exist|does not exist)/, sub {
	my $status = $1;
	my $uid = $2;
	my $spec = $3;
	my $check = $3 eq 'does exist' ? 1 : 0;

	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from safmgr.person_contact_activity inner join szbuniq
			    on szbuniq_pidm = safmgr.person_contact_activity.pidm
			where szbuniq_unique_id = upper(?)
			    and review_status = ?
		}, undef, $uid, $status);

	ok($realStatus == $check, "An $status activity record for $uid $spec ($realStatus == $check)");

};

# Flip the default requested record to required
Given qr/a required activity record for (\S+) is added/, sub {
	my $uid = $1;
	my $spec = $2;

    $bannerDbh->do(q{
        update safmgr.person_contact_activity set review_status = ?
            where pidm = (select szbuniq_pidm from szbuniq
                    where szbuniq_unique_id = upper(?))
                and review_status = ?
    }, undef, 'required', $uid, 'requested');

	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from safmgr.person_contact_activity inner join szbuniq
			    on szbuniq_pidm = safmgr.person_contact_activity.pidm
			where szbuniq_unique_id = upper(?)
			    and review_status = ?
		}, undef, $uid, 'required');

	ok($realStatus == 1, "An required activity record for $uid does exist");

};

# Flip the default requested record to complete or cancelled
Given qr/(\S+) (completed|cancelled) the review yesterday/, sub {
	my $uid = $1;
	my $spec = $2;

	my $status = $spec eq 'completed' ? 'complete' : 'cancelled';

    $bannerDbh->do(q{
        update safmgr.person_contact_activity set review_status = ?, review_date = sysdate -1
            where pidm = (select szbuniq_pidm from szbuniq
                    where szbuniq_unique_id = upper(?))
                and review_status = ?
    }, undef, $status, $uid, 'requested');

	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from safmgr.person_contact_activity inner join szbuniq
			    on szbuniq_pidm = safmgr.person_contact_activity.pidm
			where szbuniq_unique_id = upper(?)
			    and review_status = ?
			    and trunc(review_date) = trunc(sysdate - 1)
		}, undef, $uid, $status);

	ok($realStatus == 1, "An $spec activity record for $uid does exist");

};

Given qr/an emergency contact record for (\S+) with pidm "(\S+)" (.*)/, sub {
	my $uid = $1;
	my $value = $2;
	my $spec = $3;
	my $status = $3 eq 'does exist' ? 1 : 0;

	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from saturn.spremrg inner join szbuniq
			    on szbuniq_pidm = saturn.spremrg.spremrg_pidm
			where szbuniq_unique_id = upper(?)
			    and spremrg_pidm = ?
		}, undef, $uid, $value);

	ok($realStatus == $status, "A emergency contact record for $uid with pidm $value $spec");

};

Given qr/there are (\d+) outstanding (\S+) reviews/, sub {
    my $expectedCount = $1;
    my $status = $2;

	my($realCount) = $bannerDbh->selectrow_array(q{
		select count(*)
			from safmgr.person_contact_activity
			where review_status = ?
			    and review_date is null
		}, undef, $status);

	ok($realCount == $expectedCount, "Expected $expectedCount $status records, but found $realCount");
};

Given qr/the term (\S+) (starts|ends) in (\S+) days/, sub {
    my $termCode = $1;
    my $target = $2;
    my $days = $3;

    my $column = $target eq 'starts' ? 'stvterm_start_date' : 'stvterm_end_date';

    $bannerDbh->do(qq{
        update stvterm set $column = sysdate +$days
            where stvterm_code = ?
        }, undef, $termCode);

};

Given qr/(\S+) has cancelled enrollment for term (\S+)/, sub {
	my $uid = $1;
	my $term = $2;

	$bannerDbh->do(q{
		update sfrstcr set sfrstcr_rsts_code = 'DW'
			where sfrstcr_pidm = (
			    select szbuniq_pidm from szbuniq where szbuniq_unique_id = upper(?))
			    and sfrstcr_term_code = ?
		}, undef, $uid, $term);

	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from sfrstcr
			where sfrstcr_pidm = (
                select szbuniq_pidm from szbuniq where szbuniq_unique_id = upper(?))
                and sfrstcr_term_code = ?
                and sfrstcr_rsts_code != 'DW'
		}, undef, $uid, $term);

	ok($realStatus == 0, "Student $uid has cancelled enrollment for $term");

};

Given qr/(\S+) has enrolled for term (\S+)/, sub {
	my $uid = $1;
	my $term = $2;

    $bannerDbh->do(q{
        insert into sgbstdn (sgbstdn_pidm, sgbstdn_term_code_eff, sgbstdn_stst_code, sgbstdn_styp_code,
            sgbstdn_camp_code, sgbstdn_levl_code, sgbstdn_degc_code_1, sgbstdn_activity_date)
            values ((select szbuniq_pidm from szbuniq where szbuniq_unique_id = upper(?)), ?, ?, ?, ?, ?, ?, sysdate)
        }, undef, $uid, $term, 'AS', 'N', 'O', 'UG', '114');

    my($pidm) =  $bannerDbh->selectrow_array(q{
        select szbuniq_pidm from szbuniq where szbuniq_unique_id = upper(?)}, undef, $uid
        );
        
    $bannerDbh->do(q{
        delete from safmgr.person_contact_activity where pidm=?}, undef, $pidm
    );
    
	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from sgbstdn
			where sgbstdn_pidm = (
                select szbuniq_pidm from szbuniq where szbuniq_unique_id = upper(?))
                and sgbstdn_term_code_eff = ?
                and sgbstdn_stst_code = 'AS'
		}, undef, $uid, $term);

	ok($realStatus == 1, "Student $uid has been enrolled for $term");

    $bannerDbh->do(q{
        insert into sfrstcr (sfrstcr_pidm, sfrstcr_term_code, sfrstcr_crn, sfrstcr_reg_seq,
            sfrstcr_add_date, sfrstcr_activity_date, sfrstcr_levl_code, sfrstcr_camp_code,
            sfrstcr_rsts_code, sfrstcr_credit_hr)
            values ((select szbuniq_pidm from szbuniq where szbuniq_unique_id = upper(?)),
                ?, ?, ?, sysdate, sysdate, ?, ?, ?, ?)
        }, undef, $uid, $term, '12345', 1, 'UG', 'O', 'RW', 3);
};

When qr/the request contains (.+)/, sub {
    my $key = $1;

    ok(defined($requestData->{$key}), "Request data for '$key' found");

    S->{'objectType'} = 'collection';
    S->{'object'} = $requestData->{$key};

};

Then qr/an activity record (added|reviewed) today for (\S+) with status "(\S+)" (.*)/, sub {
	my $action = $1;
	my $uid = $2;
	my $value = $3;
	my $spec = $4;
	my $status = $4 eq 'does exist' ? 1 : 0;

    my $column = $action eq 'added' ? 'add_date' : 'review_date';

	my($realStatus) = $bannerDbh->selectrow_array(qq{
		select count(*)
			from safmgr.person_contact_activity inner join szbuniq
			    on szbuniq_pidm = safmgr.person_contact_activity.pidm
			where szbuniq_unique_id = upper(?)
			    and review_status = ?
			    and trunc($column) = trunc(sysdate)
		}, undef, $uid, $value);

	ok($realStatus == $status, "An activity record for $uid with status $value $spec ($realStatus == $status)");

};

Then qr/there is an outstanding review for (\S+)/, sub {
    my $uid = $1;

	my($realCount) = $bannerDbh->selectrow_array(q{
		select count(*)
			from safmgr.person_contact_activity inner join szbuniq
                on szbuniq_pidm = safmgr.person_contact_activity.pidm
            where szbuniq_unique_id = upper(?)
                and review_date is null
                and review_status in ('requested', 'required')
		}, undef, $uid);

	ok($realCount == 1, "There is an outstanding review for $uid ($realCount)");
};

Then qr/there is an outstanding (requested|required) review for (\S+)/, sub {
    my $status = $1;
    my $uid = $2;

	my($realCount) = $bannerDbh->selectrow_array(q{
		select count(*)
			from safmgr.person_contact_activity inner join szbuniq
                on szbuniq_pidm = safmgr.person_contact_activity.pidm
            where szbuniq_unique_id = upper(?)
                and review_date is null
                and review_status = ?
		}, undef, $uid, $status);

	ok($realCount == 1, "There is an outstanding $status review for $uid ($realCount)");
};

Then qr/a completed activity record for (\S+) with a review date of today (does exist|does not exist)/, sub {
	my $uid = $1;
	my $spec = $2;
	my $status = $2 eq 'does exist' ? 1 : 0;

	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from safmgr.person_contact_activity inner join szbuniq
			    on szbuniq_pidm = safmgr.person_contact_activity.pidm
			where szbuniq_unique_id = upper(?)
			    and review_status = ?
			    and review_date = trunc(sysdate)
		}, undef, $uid, 'complete');

	ok($realStatus == $status, "A completed activity record for $uid $spec");

};

Then qr/an emergency contact record for (\S+) with pidm "(\S+)" (.*)/, sub {
	my $uid = $1;
	my $value = $2;
	my $spec = $3;
	my $status = $3 eq 'does exist' ? 1 : 0;

	my($realStatus) = $bannerDbh->selectrow_array(q{
		select count(*)
			from saturn.spremrg inner join szbuniq
			    on szbuniq_pidm = saturn.spremrg.spremrg_pidm
			where szbuniq_unique_id = upper(?)
			    and spremrg_pidm = ?
		}, undef, $uid, $value);

	ok($realStatus == $status, "A emergency contact record for $uid with pidm $value $spec");

};

