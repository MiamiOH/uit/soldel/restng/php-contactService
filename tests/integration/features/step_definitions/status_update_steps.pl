#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use LWP::UserAgent;
use JSON;
use Data::Dumper;

use lib 'lib';
use StepConfig;


Given qr/a collection of contact records to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                      'relation' => 'Mother',
                      'relationCode' => 'M',
                      'pidm' => '567890',
                      'priority' => '1',
                      'firstName' => 'Janet',
                      'lastName' => 'Bryant',
                      'middleName' => '',
                      'phoneNumber' => '+15132345678',
                      'phoneNumberNational' => '(513)234-5678',
                      'phoneNumberInternational' => '+1(513)-234-5678',
             },
        ];
};

Given qr/a collection of contact records missing person to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                      'pidm' => '567890',
                      'priority' => '2',
                      'firstName' => 'Steve',
                      'lastName' => 'Steve',
                      'middleName' => '',
                      'phoneNumber' => '+15132345678',
                      'phoneNumberNational' => '(513)234-5678',
                      'phoneNumberInternational' => '+1(513)-234-5678',
             },
        ];
};

Given qr/a collection of contact records with incorrect details to create in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                      'pidm' => '567890',
                      'priority' => '5',
                      'firstName' => 'Steve',
                      'lastName' => 'Steve',
                      'middleName' => '',
                      'phoneNumber' => '+1513!aswfgs2345678',
                      'phoneNumberNational' => '(513)234-5678',
                      'phoneNumberInternational' => '+1(513)-234-5678',
             },
        ];
};

Given qr/a collection of contact records to update in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                    'relation' => 'Father',
                    'relationCode' => 'F',
                    'pidm' => '234567',
                    'priority' => '1',
                    'firstName' => 'John',
                    'lastName' => 'Snow',
                    'middleName' => '',
                    'phoneNumber' => '+15132345678',
             },
        ];

};

Given qr/a collection of contact records to delete in the payload/, sub {
S->{'objectType'} = 'collection';
S->{'object'} = [
            {
                    'relation' => 'Father',
                    'relationCode' => 'F',
                    'pidm' => '987654',
                    'priority' => '1',
                    'firstName' => 'Doug',
                    'lastName' => 'Troy',
                    'middleName' => '',
                    'phoneNumber' => '+15132345678',
             },
        ];

};
