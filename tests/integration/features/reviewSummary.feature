Feature: Get the current number of people for contact review
  As an interested entity
  I want to get the count of collection of people who are required to complete the review and people who have completed the review
  In order to know the participation data for the contact app

  Background:
    Given the test data is ready

  Scenario: Require authentication to get the review summary list
    Given a REST client
    When I make a GET request for /person/contact/reviewSummary/v1?type=student
    Then the HTTP status code is 401

  Scenario: Requesting Collection for review summary list for students
    Given a REST client
    And a token for the ADMIN user
    When I make a GET request for /person/contact/reviewSummary/v1?type=student
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject has an element numberOfRequestedOrRequired equal to "3"
    And the subject has an element numberOfCompleted equal to "1"
    And the subject has an element percentOfRequestedOrRequired equal to "75"
    And the subject has an element percentOfCompleted equal to "25"



