Feature: Get the current status of people for contact review
  As an interested entity
  I want to get the contact review status of a person or collection of people
  In order to ensure the correct people are flagged for notification

  Background:
    Given the test data is ready

  Scenario: Require authentication for the reviewStatus resource
    Given a REST client
    When I make a GET request for /person/contact/reviewStatus/v1
    Then the HTTP status code is 401

  Scenario: Require the type option be a supported value (i.e. student)
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/reviewStatus/v1?type=bob
    Then the HTTP status code is 400

  Scenario: Requesting student type should not include employees
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/reviewStatus/v1?type=student
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And all entries have a type equal to "student"

  Scenario: Collection contains correct models
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/reviewStatus/v1?type=student
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And I get the first entry with pidm equal to "987654" from the subject
    And the subject has an element reviewStatus equal to "requested"
    And the subject has an element type equal to "student"

  Scenario: Requesting requested returns only status = "requested"
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When I make a GET request for /person/contact/reviewStatus/v1?status=requested
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And all entries have a reviewStatus equal to "requested"

