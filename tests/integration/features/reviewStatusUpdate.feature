Feature: Update the review status of a user
  As a developer using the Review Status API
  I want to update the review status of a user
  In order to add or update a person's status

  Background:
    Given the test data is ready
    And the WAS test data is ready

  Scenario: Require authentication for the reviewStatus resource
    Given a REST client
    When I make a PUT request for /person/contact/reviewStatus/v1/doej
    Then the HTTP status code is 401

  Scenario: Create a new status record for a user with no current status
    Given a REST client
    And a token for the CONTACT_WS_USER user
    And an activity record for doej with status "requested" does not exist
    When I have a status with the pidm "123456"
    And I have a status with the uniqueId "doej"
    And I have a status with the reviewStatus "requested"
    And I have a status with the type "student"
    And I make a PUT request for /person/contact/reviewStatus/v1/doej
    Then the HTTP status code is 200
    And an activity record added today for doej with status "requested" does exist
    And the WAS optional update was called with reviewRequested "1" for doej
    And the WAS optional update was called with complete "0" for doej
    And the WAS required update was called with reviewRequested "0" for doej
    And the WAS required update was called with complete "0" for doej

  Scenario: Update an existing status record from requested to complete
    Given a REST client
    And a token for the CONTACT_WS_USER user
    And a requested activity record for souther does exist
    When I have a status with the pidm "876253"
    And I have a status with the uniqueId "souther"
    And I have a status with the reviewStatus "complete"
    And I have a status with the type "student"
    And I make a PUT request for /person/contact/reviewStatus/v1/souther
    Then the HTTP status code is 200
    And an activity record added today for souther with status "requested" does not exist
    And a completed activity record for souther with a review date of today does exist
    And the WAS optional update was called with complete "1" for souther
    And the WAS required update was called with complete "1" for souther

  Scenario: Update an existing status record from requested to required
    Given a REST client
    And a token for the CONTACT_WS_USER user
    And a requested activity record for souther does exist
    When I have a status with the pidm "876253"
    And I have a status with the uniqueId "souther"
    And I have a status with the reviewStatus "required"
    And I have a status with the type "student"
    And I make a PUT request for /person/contact/reviewStatus/v1/souther
    Then the HTTP status code is 200
    And an activity record added today for souther with status "requested" does not exist
    And there is an outstanding required review for souther
    And the WAS optional update was called with reviewRequested "0" for souther
    And the WAS optional update was called with complete "0" for souther
    And the WAS required update was called with reviewRequested "1" for souther
    And the WAS required update was called with complete "0" for souther

  Scenario: Update an existing status record from required to complete
    Given a REST client
    And a token for the CONTACT_WS_USER user
    And a required activity record for souther is added
    When I have a status with the pidm "876253"
    And I have a status with the uniqueId "souther"
    And I have a status with the reviewStatus "complete"
    And I have a status with the type "student"
    And I make a PUT request for /person/contact/reviewStatus/v1/souther
    Then the HTTP status code is 200
    And a completed activity record for souther with a review date of today does exist
    And the WAS optional update was called with reviewRequested "0" for souther
    And the WAS optional update was called with complete "1" for souther
    And the WAS required update was called with reviewRequested "0" for souther
    And the WAS required update was called with complete "1" for souther

  Scenario: Update a collection of status entries
    Given a REST client
    And a token for the CONTACT_WS_USER user
    When the request contains status update models
    And I make a PUT request for /person/contact/reviewStatus/v1
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And all entries have a code equal to "200"

  Scenario: A user can update their own status
    Given a REST client
    And a token for the DOEJ user
    And an activity record for doej with status "complete" does not exist
    When I have a status with the pidm "123456"
    And I have a status with the uniqueId "doej"
    And I have a status with the reviewStatus "complete"
    And I have a status with the type "student"
    And I make a PUT request for /person/contact/reviewStatus/v1/doej
    Then the HTTP status code is 200
    And an activity record added today for doej with status "complete" does exist
    And the WAS optional update was called with complete "1" for doej
    And the WAS required update was called with complete "1" for doej

  Scenario: A user is not authorized to update another user's status
    Given a REST client
    And a token for the SMITHD user
    And an activity record for doej with status "complete" does not exist
    When I have a status with the pidm "123456"
    And I have a status with the uniqueId "doej"
    And I have a status with the reviewStatus "complete"
    And I have a status with the type "student"
    And I make a PUT request for /person/contact/reviewStatus/v1/doej
    Then the HTTP status code is 401

  Scenario: Update SELF must match PIDM in model to SELF id
    Given a REST client
    And a token for the DOEJ user
    And an activity record for doej with status "complete" does not exist
    When I have a status with the pidm "123455"
    And I have a status with the uniqueId "doej"
    And I have a status with the reviewStatus "complete"
    And I have a status with the type "student"
    And I make a PUT request for /person/contact/reviewStatus/v1/doej
    Then the HTTP status code is 400
