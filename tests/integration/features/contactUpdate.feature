Feature: Update emergency contact record of a user
  As a developer using the contact API
  I want to update the emergency contact record of a user
  In order to make sure it works

  Background:
    Given the test data is ready

  Scenario: Require authentication for the contact resource
    Given a REST client
    When I make a PUT request for /person/contact/v1/howardt
    Then the HTTP status code is 401

  Scenario: Update a new emergency contact record for a user
    Given a REST client
    And a token for the CONTACT_WS_USER user
    And a collection of contact records to update in the payload
    When I give the HTTP Content-type header with the value "application/json"
    And I make a PUT request for /person/contact/v1/howardt?type=emergency
    Then the HTTP status code is 200
    And the response can be parsed
    And I get the data element from the response object
    And the subject contains 1 entry
    And I get entry 1 from the subject
    And the subject has an element status equal to "200"
    And that an emergency contact record for howardt with pidm "234567" does exist

