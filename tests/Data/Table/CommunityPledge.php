<?php


namespace MiamiOH\RestngContactService\Tests\Data\Table;


use MiamiOH\RESTng\Testing\Data\Table;

class CommunityPledge extends Table
{

    public function create(): void
    {
        $this->drop();
        $this->database->exec('
            create table if not exists community_pledge (
                id integer primary key AUTOINCREMENT,
                uniqueid VARCHAR2(8) not null,
                type VARCHAR2(200) not null,
                response VARCHAR2(200) not null,
                response_time DATE not null)
            ');
    }

    public function drop(): void
    {
        $this->database->exec('drop table if exists community_pledge');

    }

    public function populate(): void
    {
        foreach ($this->testRecords() as $pledgeData) {
            $insert = sprintf(
                "insert into community_pledge(id, uniqueid, type, response, response_time) values ('%s', '%s', '%s', '%s', '%s')",
                ...$pledgeData
            );

            $this->database->exec($insert);
        }
    }

    private function testRecords(): array
    {
        return [
            [11111, 'doej', 'oxf-student', 'agree', '2020-07-22 18:22:12'],
            [11141, 'doej3', 'oxf-student', 'agree', '2020-07-22 18:22:12'],
            [11121, 'doej3', 'oxf-student', 'disagree', '2020-07-23 18:22:12'],
            [11411, 'doej3', 'oxf-student', 'agree', '2020-07-24 18:22:12'],
        ];
    }
}
