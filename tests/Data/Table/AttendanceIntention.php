<?php


namespace MiamiOH\RestngContactService\Tests\Data\Table;


use MiamiOH\RESTng\Testing\Data\Table;

class AttendanceIntention extends Table
{

    public function create(): void
    {
        $this->drop();
        $this->database->exec('
            create table if not exists attendance_intention (
                id integer primary key AUTOINCREMENT,
                uniqueid text not null,
                choice text not null,
                delayed_start_date text,
                response_time text not null
            )
        ');
    }

    public function drop(): void
    {
        $this->database->exec('drop table if exists attendance_intention');

    }

    public function populate(): void
    {
        foreach ($this->testRecords() as $intentions) {
            $insert = sprintf(
                    "insert into attendance_intention(id, uniqueid, choice, delayed_start_date, response_time) values ('%s', '%s', '%s', '%s', '%s')",
                 ...$intentions
            );

            $this->database->exec($insert);
        }
    }

    private function testRecords(): array
    {
        return [
            [11111,'doej','delayedStart','2020-09-07','2020-07-22 18:22:12'],
            [11141,'doej3','delayedStart','2020-09-07','2020-07-22 18:22:12'],
            [11121,'doej3','delayedStart','2020-09-07','2020-07-23 18:22:12'],
            [11411,'doej3','delayedStart','2020-09-07','2020-07-24 18:22:12'],
        ];
    }
}
