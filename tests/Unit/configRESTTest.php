<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ConfigRESTTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $configREST;

    private $api;
    private $configuration;

    private $calledApplicationName = '';
    private $calledCategoryName = '';
    private $mockConfigurationItems = [];

    protected function setUp():void
    {

        $this->calledApplicationName = '';
        $this->calledCategoryName = '';
        $this->mockConfigurationItems = [];

        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->configuration = $this->getMockBuilder('\MiamiOH\RESTng\Util\Configuration')
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $this->configuration->method('getConfiguration')
            ->with($this->callback(array($this, 'getConfigurationWithApplication')),
                $this->callback(array($this, 'getConfigurationWithCategory')))
            ->will($this->returnCallback(array($this, 'getConfigurationMock')));

        $this->configREST = new \MiamiOH\RestngContactService\Services\ConfigREST();
        $this->configREST->setLogger();
        $this->configREST->setApp($this->api);
        $this->configREST->setConfiguration($this->configuration);

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Missing required configuration category
     */
    public function testGetConfigurationWithoutCategory()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam'))
            ->getMock();

        $this->configREST->setRequest($request);

        $resp = $this->configREST->getConfiguration();

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Invalid configuration category
     */
    public function testGetConfigurationWithInvalidCategory()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['category' => 'bob']);

        $this->configREST->setRequest($request);

        $resp = $this->configREST->getConfiguration();

    }

    public function testGetConfigurationWithCategory()
    {

        $this->mockConfigurationItems = [
            'configItem1' => 'Config Item 1 value',
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['category' => 'uiString']);

        $this->configREST->setRequest($request);

        $resp = $this->configREST->getConfiguration();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(1, count($payload));

        $this->assertEquals('PersonContactInfo', $this->calledApplicationName);
        $this->assertEquals('Contact Info Review', $this->calledCategoryName);

    }

    public function getConfigurationWithApplication($subject)
    {
        $this->calledApplicationName = $subject;

        return true;
    }

    public function getConfigurationWithCategory($subject)
    {
        $this->calledCategoryName = $subject;

        return true;
    }

    public function getConfigurationMock()
    {
        return $this->mockConfigurationItems;
    }

}
