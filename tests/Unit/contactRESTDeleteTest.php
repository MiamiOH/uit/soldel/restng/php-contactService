<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ContactRESTDeleteTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $contactREST;

    private $api;
    private $contact;
    private $bannerId;

    private $contactData = [];
    private $mockModel = [];


    protected function setUp():void
    {

        $this->contactData = [];

        $this->mockModel = [];


        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->contact = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Contact')
            ->setMethods(array('delete'))
            ->getMock();

        $this->contact->method('delete')
            ->with($this->callback(array($this, 'deleteWithData')))
            ->will($this->returnCallback(array($this, 'deleteMock')));

        $this->contactREST = new \MiamiOH\RestngContactService\Services\ContactREST();

        $this->contactREST->setApp($this->api);
        $this->contactREST->setContact($this->contact);
        $this->contactREST->setBannerUtil($bannerUtil);
        $this->contactREST->setLogger();
    }

    public function testDeleteContactEmergency()
    {

        $this->mockModel = [
            [

                'relation' => 'F',
                'pidm' => '567890',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'phoneNumber' => '+15135551111',
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('jonesl');

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'emergency']);

        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->deleteContactCollection();

        $payload = $resp->getPayload();


        $this->assertEquals(App::API_OK, $resp->getStatus());
    }

    public function testDeleteContactEmergencyByPIDM()
    {

        $this->mockModel = [
            [

                'relation' => 'F',
                'pidm' => '567890',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'phoneNumber' => '+15135551111',
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('567890');

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'emergency']);

        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->deleteContactCollection();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function testDeleteContactMissingPerson()
    {
        $this->mockModel = [
            [
                'relation' => 'K',
                'pidm' => '567890',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'phoneNumber' => '+15135551111',
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('jonesl');

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'missingPerson', 'purgeInvalidContacts' => 'enabled']);

        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->deleteContactCollection();

        $payload = $resp->getPayload();


        $this->assertEquals(App::API_OK, $resp->getStatus());
        //$this->assertEquals($payload['data'][0]['pidm'], $this->contactListPidm, 'Response has pidm matching');
        //$this->assertEquals($payload['data'][0]['code'], App::API_OK, 'Response has code App::API_OK');
        $this->assertEquals(App::API_OK, $payload[0]['status'],
            'Response has code App::API_OK');

    }

    public function testDeleteBadRequest()
    {
        $this->mockModel = [
            [
                'relation' => 'F',
                'pidm' => '234556',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'phoneNumber' => '+15135551111',
            ],
        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('jonesl');

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'missingPerson']);

        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->deleteContactCollection();

        $payload = $resp->getPayload();

        $this->assertEquals($payload[0]['status'],
            App::API_BADREQUEST,
            'Response has code App::API_BADREQUEST');


    }

    public function deleteContactPidmMock($subject)
    {
        $this->contactListPidm = $subject;

        return true;
    }

    public function deleteContactDataMock()
    {
        return $this->contactData;
    }

    public function deleteContactResponseMock()
    {
        $payload = [
            'data' => [
                [
                    'pidm' => '567890',
                    'code' => 200,
                    'message' => 'DELETE Success'
                ]
            ],
            'status' => 200,
            'error' => 'false'
        ];

        return $payload;
    }

    public function deleteWithData()
    {
        return true;
    }

    public function deleteMock()
    {
        return $this->mockModel;
    }
}
