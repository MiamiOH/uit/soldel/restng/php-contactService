<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

class ReviewStatusUpdateTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $reviewStatus;

    private $dbh;
    private $was;

    private $queryNumber = 0;
    private $queries = [];
    private $queryMockResponses = [];

    private $updateWASUid = '';
    private $updateWASType = '';
    private $updateWASOptions = [];
    private $updateWASResponse = '';

    private $profile;

    protected function setUp():void
    {

        $this->queryNumber = 0;
        $this->queries = [];
        $this->queryMockResponses = [];

        $this->updateWASUid = '';
        $this->updateWASType = '';
        $this->updateWASOptions = [];
        $this->updateWASResponse = '';


        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstcolumn', 'perform', 'queryfirstrow_assoc'))
            ->getMock();

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);


        $db->method('getHandle')->willReturn($this->dbh);

        $this->was = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\WAS')
            ->setMethods(array('updateWAS'))
            ->getMock();

        $this->profile = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Profile')
            ->setMethods(array('getLDAPAttributes'))
            ->getMock();

        $this->reviewStatus = new \MiamiOH\RestngContactService\Services\ReviewStatus();

        $this->reviewStatus->setDatabase($db);
        $this->reviewStatus->setWas($this->was);
        $this->reviewStatus->setProfile($this->profile);
    }

    public function testGetCurrentStatus()
    {

        $record = [
            'pidm' => 123456,
            'uniqueid' => 'doej',
            'review_status' => 'requested',
            'add_date' => '2016-06-06',
            'review_date' => '',
            'review_type' => 'student',
            'status_id' => 1,
        ];

        $expectedModel = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'reviewStatus' => 'requested',
            'addDate' => '2016-06-06',
            'reviewDate' => '',
            'type' => 'student',
            'statusId' => 1,
        ];

        /*
        * Expected query order
        *  1 queryfirstcolumn to see if status exists
        *  2 queryfirstrow_assoc fetch record for user
        */

        $this->queryMockResponses[] = 1;
        $this->queryMockResponses[] = $record;

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $result = $this->reviewStatus->getCurrentStatus(123456, $record['review_type']);

        $this->assertEquals($expectedModel, $result);

    }

    public function testGetCurrentStatusNotFound()
    {

        $this->queryMockResponses[] = 0;

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $result = $this->reviewStatus->getCurrentStatus(123456, 'student');

        $this->assertNull($result);

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage MiamiOH\RestngContactService\Services\ReviewStatus::setStatus()
     *     requires valid reviewStatus (requested, required, complete, cancelled
     */
    public function testSetStatusRequiresValidStatus()
    {
        $model = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'reviewStatus' => 'bob',
            'type' => 'student'
        ];

        $this->reviewStatus->setStatus($model);

    }

    public function testSetStatusRequestedNoCurrentStatus()
    {

        $model = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'reviewStatus' => 'requested',
            'type' => 'student'
        ];

        /*
         * Expected query order
         *  1 queryfirstcolumn to see if status exists
         *  2 perform insert of new record for pidm
         */

        $this->queryMockResponses[] = 0;
        $this->queryMockResponses[] = true;

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $this->was->expects($this->once())->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithType')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->updateWASResponse = 'ok';

        $this->profile->expects($this->once())->method('getLDAPAttributes')
            ->willReturn([
                'primaryAffiliationCode' => 'stu',
                'primaryAffiliationDesc' => 'student',
                'campusCode' => 'O',
                'campus' => 'Oxford',
                'givenName' => 'Jane',
                'classes' => []
            ]);

        $result = $this->reviewStatus->setStatus($model);

        $this->assertTrue($result);

        $this->assertEquals(123456, $this->queries[0]['params'],
            'Provided pidm is used to find a current status');

        $this->assertTrue(in_array(123456, $this->queries[1]['params']),
            'Provided pidm is used when adding new status record');
        $this->assertTrue(in_array('requested', $this->queries[1]['params']),
            'Proper complete code is used when adding new status record');
        $this->assertTrue(stripos($this->queries[1]['query'], 'insert') !== false,
            'Insert is called to add new status record');

        $this->assertTrue($this->updateWASOptions['reviewRequested'],
            'Update WAS to required sets reviewRequested true');
        $this->assertFalse($this->updateWASOptions['complete'],
            'Update WAS to required clears complete status');

    }

    public function testSetStatusCompleteNoCurrentStatus()
    {

        $model = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'reviewStatus' => 'complete',
            'type' => 'student'
        ];

        /*
         * Expected query order
         *  1 queryfirstcolumn to see if status exists
         *  2 perform insert of new record for pidm
         */

        $this->queryMockResponses[] = 0;
        $this->queryMockResponses[] = true;

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $this->was->expects($this->once())->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithType')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->updateWASResponse = 'ok';

        $this->profile->expects($this->once())->method('getLDAPAttributes')
            ->willReturn([
                'primaryAffiliationCode' => 'stu',
                'primaryAffiliationDesc' => 'student',
                'campusCode' => 'O',
                'campus' => 'Oxford',
                'givenName' => 'Jane',
                'classes' => []
            ]);

        $result = $this->reviewStatus->setStatus($model);

        $this->assertTrue($result);

        $this->assertEquals(123456, $this->queries[0]['params'],
            'Provided pidm is used to find a current status');
        $this->assertTrue(in_array(123456, $this->queries[1]['params']),
            'Provided pidm is used when adding new status record');
        $this->assertTrue(in_array('complete', $this->queries[1]['params']),
            'Proper complete code is used when adding new status record');
        $this->assertTrue(in_array(date('Y-m-d'), $this->queries[1]['params']),
            'Review date is today when adding new complete status record');
        $this->assertTrue(stripos($this->queries[1]['query'], 'insert') !== false,
            'Insert is called to add new status record');

        $this->assertEquals('doej', $this->updateWASUid,
            'Update WAS with correct uniqueid');
        $this->assertEquals('student', $this->updateWASType,
            'Update WAS with correct type');
        $this->assertTrue($this->updateWASOptions['complete'],
            'Update WAS with complete true');

    }

    public function testSetStatusCompleteWithCurrentStatus()
    {

        $record = [
            'pidm' => 123456,
            'uniqueid' => 'doej',
            'review_status' => 'requested',
            'add_date' => '2016-06-06',
            'review_date' => '',
            'review_type' => 'student',
            'status_id' => 1,
        ];

        $model = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'reviewStatus' => 'complete',
            'type' => 'student'
        ];

        /*
         * Expected query order
         *  1 queryfirstcolumn to see if status exists
         *  2 queryfirstrow_assoc fetch record for user
         *  3 perform to update the current status record
         */

        $this->queryMockResponses[] = 1;
        $this->queryMockResponses[] = $record;
        $this->queryMockResponses[] = true;

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->expects($this->once())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $this->was->expects($this->once())->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithType')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->updateWASResponse = 'ok';

        $result = $this->reviewStatus->setStatus($model);

        $this->assertTrue($result);

        $this->assertEquals(123456, $this->queries[0]['params'],
            'Provided pidm is used to find a current status');

        $this->assertEquals(123456, $this->queries[1]['params'],
            'Provided pidm is used to fetch a current status');

        $this->assertTrue(in_array(1, $this->queries[2]['params']),
            'Provided statusId is used when updating a requested record');
        $this->assertTrue(in_array(date('Y-m-d'), $this->queries[2]['params']),
            'Review date is today is used when updating requested record without providing a date');
        $this->assertTrue(in_array('complete', $this->queries[2]['params']),
            'Proper complete code is used when updating requested record');
        $this->assertTrue(stripos($this->queries[2]['query'], 'update') !== false,
            'Update is called to update a requested record');

        $this->assertEquals('doej', $this->updateWASUid,
            'Update WAS with correct uniqueid');
        $this->assertEquals('student', $this->updateWASType,
            'Update WAS with correct type');
        $this->assertTrue($this->updateWASOptions['complete'],
            'Update was with complete true');

    }

    public function testSetStatusRequestedWithCurrentStatusRequested()
    {

        $record = [
            'pidm' => 123456,
            'uniqueid' => 'doej',
            'review_status' => 'requested',
            'add_date' => '2016-06-06',
            'review_date' => '',
            'review_type' => 'student',
            'status_id' => 1,
        ];

        $model = [
            'pidm' => 123456,
            'uniqueId' => 'doej',
            'reviewStatus' => 'requested',
            'type' => 'student'
        ];

        /*
         * Expected query order
         *  1 queryfirstcolumn to see if status exists
         *  2 queryfirstrow_assoc fetch record for user
         */

        $this->queryMockResponses[] = 1;
        $this->queryMockResponses[] = $record;

        $this->dbh->expects($this->once())->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->expects($this->never())->method('perform')
            ->with($this->callback(array($this, 'performWithQuery')),
                $this->callback(array($this, 'performWithParams')))
            ->will($this->returnCallback(array($this, 'performMock')));

        $this->was->expects($this->never())->method('updateWAS')
            ->with($this->callback(array($this, 'updateWASWithUid')),
                $this->callback(array($this, 'updateWASWithType')),
                $this->callback(array($this, 'updateWASWithOptions')))
            ->will($this->returnCallback(array($this, 'updateWASMock')));

        $this->updateWASResponse = 'ok';

        $result = $this->reviewStatus->setStatus($model);

        $this->assertTrue($result);

        $this->assertEquals(123456, $this->queries[0]['params'],
            'Provided pidm is used to find a current status');

        $this->assertEquals(123456, $this->queries[1]['params'],
            'Provided pidm is used to fetch a current status');

    }

    public function queryfirstcolumnWithQuery($subject)
    {
        $this->queries[$this->queryNumber] = [
            'query' => $subject,
            'params' => [],
        ];

        return true;
    }

    public function queryfirstcolumnWithParams($subject)
    {
        $this->queries[$this->queryNumber]['params'] = $subject;

        return true;
    }

    public function queryfirstcolumnMock()
    {
        $queryNumber = $this->queryNumber;
        $this->queryNumber++;

        return $this->queryMockResponses[$queryNumber];
    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        $this->queries[$this->queryNumber] = [
            'query' => $subject,
            'params' => [],
        ];

        return true;
    }

    public function queryfirstrow_assocWithParams($subject)
    {
        $this->queries[$this->queryNumber]['params'] = $subject;

        return true;
    }

    public function queryfirstrow_assocMock()
    {
        $queryNumber = $this->queryNumber;
        $this->queryNumber++;

        return $this->queryMockResponses[$queryNumber];
    }

    public function performWithQuery($subject)
    {
        $this->queries[$this->queryNumber] = [
            'query' => $subject,
            'params' => [],
        ];

        return true;
    }

    public function performWithParams($subject)
    {
        $this->queries[$this->queryNumber]['params'] = $subject;

        return true;
    }

    public function performMock()
    {
        $queryNumber = $this->queryNumber;
        $this->queryNumber++;

        return $this->queryMockResponses[$queryNumber];
    }

    public function updateWASWithUid($subject)
    {
        $this->updateWASUid = $subject;

        return true;
    }

    public function updateWASWithType($subject)
    {
        $this->updateWASType = $subject;

        return true;
    }

    public function updateWASWithOptions($subject)
    {
        $this->updateWASOptions = $subject;

        return true;
    }

    public function updateWASMock()
    {
        return $this->updateWASResponse;
    }

}
