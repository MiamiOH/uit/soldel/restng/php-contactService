<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Legacy\DB\STH;
use MiamiOH\RestngContactService\Exceptions\ReAddContactException;

class ContactDeleteTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $contact;

    private $dbh;

    private $records = [];
    private $queryString = '';

    protected function setUp():void
    {

        $this->bindPlaceHolder = '';
        $this->boundValues = array();

        $this->apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('getUsername'))
            ->getMock();

        $this->phoneUtil = $this->getMockBuilder('\libphonenumber\PhoneNumberUtilMock')
            ->setMethods(array('parse', 'isValidNumber', 'format'))
            ->getMock();

        $this->phoneHelper = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\PhoneHelper')
            ->setMethods(array('getInstance', 'nationalCode', 'internationalCode'))
            ->getMock();

        $this->phoneHelper->method('getInstance')
            ->willReturn($this->phoneUtil);

        $this->phoneHelper->method('nationalCode')
            ->willReturn(1);

        $this->phoneHelper->method('internationalCode')
            ->willReturn(2);

        $this->rowID = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\RowIDMock')
            ->setMethods(array('free'))
            ->getMock();

        $this->dbh = $this->createMock(DBH::class);

        $this->sth = $this->getMockBuilder(STH::class)
            ->setMethods(array(
                'execute',
                'bind_by_name',
                'new_descriptor',
                'fetchrow_assoc'
            ))
            ->getMock();

        $this->dbh->method('prepare')->willReturn($this->sth);

        $this->sth->method('execute')->willReturn(true);

        $this->sth->method('bind_by_name')->willReturn(true);

        $this->dbh->method('queryfirstcolumn')->will($this->onConsecutiveCalls(0,
            1));

        $this->sth->method('new_descriptor')->willReturn($this->rowID);

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);

        $db->method('getHandle')->willReturn($this->dbh);

        $this->contact = new \MiamiOH\RestngContactService\Services\Contact();

        $this->contact->setDatabase($db);
        $this->contact->setApiUser($this->apiUser);
        $this->contact->setPhoneHelper($this->phoneHelper);
        $this->contact->setLogger();
    }

    public function testDeleteContactSuccess()
    {
        $this->records = [
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '1',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'Bubba',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'F',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '2',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'John',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'G',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->phoneUtil->method('isValidNumber')->willReturn('1');

        $model = [
            'relation' => 'Father',
            'relationCode' => 'F',
            'pidm' => '110518',
            'priority' => '1',
            'firstName' => 'Bubba',
            'lastName' => 'Smith',
            'middleName' => '',
            'phoneNumber' => '+15131234567',
        ];

        $this->assertEquals(true, $this->contact->delete($model));

    }

    public function testDeleteContactFailedAndRolledBack()
    {
        $this->records = [
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '1',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'Bubba',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'F',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '2',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'John',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'G',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->phoneUtil->method('isValidNumber')->willReturn('0');

        $model = [
            'relation' => 'Father',
            'relationCode' => 'F',
            'pidm' => '110518',
            'priority' => '1',
            'firstName' => 'Bubba',
            'lastName' => 'Smith',
            'middleName' => '',
            'phoneNumber' => '+15131234567',
        ];

        $this->expectException(ReAddContactException::class);

        $this->contact->delete($model);

    }

    public function testDeleteContactPurgesInvalidContacts()
    {
        $this->records = [
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '1',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'Bubba',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'F',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '2',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'John',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'G',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->phoneUtil->method('isValidNumber')->willReturn('0');

        $model = [
            'relation' => 'Father',
            'relationCode' => 'F',
            'pidm' => '110518',
            'priority' => '1',
            'firstName' => 'Bubba',
            'lastName' => 'Smith',
            'middleName' => '',
            'phoneNumber' => '+15131234567',
        ];

        $this->assertEquals(true, $this->contact->delete($model, true));

    }

    public function prepareWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function executeArrayMock()
    {
        return $this->records;
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }
}
