<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ContactRESTTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $contactREST;

    private $api;
    private $contact;
    private $request;

    private $contactData = [];

    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];
    private $mockReadResponse = [];
    private $readPidm = '';
    private $filterContactType = '';
    private $filterContactRelationCode = '';

    protected function setUp():void
    {

        $this->contactData = [];

        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];
        $this->mockReadResponse = [];
        $this->readPidm = '';
        $this->filterContactType = '';
        $this->filterContactRelationCode = '';

        $this->api = $this->createMock(App::class);

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($bannerId);

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->callback(array($this, 'getResourceParamWith')))
            ->will($this->returnCallback(array($this, 'getResourceParamMock')));

        $this->contact = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Contact')
            ->setMethods(array('read', 'filterContactType', 'filterRelationType'))
            ->getMock();

        $this->contact->method('filterContactType')
            ->with($this->callback(array($this, 'filterContactTypeWith')))
            ->will($this->returnSelf());

        $this->contact->method('filterRelationType')
            ->with($this->callback(array($this, 'filterRelationTypeWith')))
            ->will($this->returnSelf());


        $this->contactREST = new \MiamiOH\RestngContactService\Services\ContactREST();

        $this->contactREST->setApp($this->api);
        $this->contactREST->setContact($this->contact);
        $this->contactREST->setBannerUtil($bannerUtil);
        $this->contactREST->setLogger();
    }

    public function testGetContactREST()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'relation' => 'Father',
            'relationCode' => 'F',
            'priority' => 1,
            'firstName' => 'Bubba',
            'lastName' => 'Smith',
            'middleName' => '',
            'phoneNumber' => '+15131234567',
        ];

        $this->contact->method('read')
            ->with($this->callback(array($this, 'readWith')))
            ->will($this->returnCallback(array($this, 'readMock')));


        $this->contactREST->setRequest($this->request);

        $resp = $this->contactREST->getContact();

        $payload = $resp->getPayload();

        $this->assertEquals($this->mockReadResponse, $payload);

    }

    public function testGetContactRESTTypeMissingPerson()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'relation' => 'Father',
            'relationCode' => 'F',
            'priority' => 1,
            'firstName' => 'Bubba',
            'lastName' => 'Smith',
            'middleName' => '',
            'phoneNumber' => '+15131234567',
        ];

        $this->contact->method('read')
            ->with($this->callback(array($this, 'readWith')))
            ->will($this->returnCallback(array($this, 'readMock')));

        $this->request->method('getOptions')
            ->willReturn(['type' => 'missingPerson']);

        $this->contactREST->setRequest($this->request);

        $response = $this->contactREST->getContact();

        $payload = $response->getPayload();

        $this->assertEquals($this->mockReadResponse, $payload);
        $this->assertEquals('missingPerson', $this->filterContactType);
    }

    public function testGetContactRESTMissingPersonList()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'relation' => 'Missing Person',
            'relationCode' => 'K',
            'priority' => 1,
            'firstName' => 'Bubba',
            'lastName' => 'Smith',
            'middleName' => '',
            'phoneNumber' => '+15131234567',
        ];

        $this->contact->method('read')
            ->with($this->callback(array($this, 'readWith')))
            ->will($this->returnCallback(array($this, 'readMissingPersonMock')));

        $this->contactREST->setRequest($this->request);

        $response = $this->contactREST->getMissingPersonContactList();

        $payload = $response->getPayload();

        $this->assertEquals($this->mockReadResponse, $payload);
        $this->assertEquals('emergency', $this->filterContactType);

    }

    public function testGetContactRESTRelation()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockReadResponse = [
            'pidm' => 123456,
            'relation' => 'Father',
            'relationCode' => 'F',
            'priority' => 1,
            'firstName' => 'Bubba',
            'lastName' => 'Smith',
            'middleName' => '',
            'phoneNumber' => '+15131234567',
        ];

        $this->contact->method('read')
            ->with($this->callback(array($this, 'readWith')))
            ->will($this->returnCallback(array($this, 'readMock')));

        $this->request->method('getOptions')
            ->willReturn(['relationCode' => 'F']);

        $this->contactREST->setRequest($this->request);

        $response = $this->contactREST->getContact();

        $payload = $response->getPayload();

        $this->assertEquals($this->mockReadResponse, $payload);
        $this->assertEquals('F', $this->filterContactRelationCode);

    }

    public function getResourceParamWith($subject)
    {
        $this->requestResourceParam = $subject;

        return true;
    }

    public function getResourceParamMock()
    {
        if (isset($this->requestResourceParamMocks[$this->requestResourceParam])) {
            return $this->requestResourceParamMocks[$this->requestResourceParam];
        }

        return null;
    }


    public function readWith($subject)
    {
        $this->readPidm = $subject;

        return true;
    }

    public function readMock()
    {
        return $this->mockReadResponse;
    }

    public function readMissingPersonMock()
    {
        if ($this->filterContactType === 'missingPerson') {
            return [];
        } else {
            return $this->mockReadResponse;
        }
    }

    public function filterContactTypeWith($subject)
    {
        $this->filterContactType = $subject;

        return true;
    }

    public function filterRelationTypeWith($subject)
    {
        $this->filterContactRelationCode = $subject;

        return true;
    }

    private function validateContactModel($contact)
    {

        $expectedKeys = [
            'relation',
            'relationCode',
            'pidm',
            'priority',
            'firstName',
            'lastName',
            'middleName',
            'streetAddressLine1',
            'streetAddressLine2',
            'city',
            'state',
            'postalCode',
            'phoneNumber',
            'addressType',
        ];

        $this->assertEquals(count($expectedKeys), count(array_keys($contact)),
            'Expected key count for contact model');
        foreach ($expectedKeys as $key) {
            $this->assertTrue(array_key_exists($key, $contact),
                'Expected key exists: ' . $key);
        }
    }
}
