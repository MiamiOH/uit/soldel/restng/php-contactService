<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ContactRESTPutTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $contactREST;

    private $api;
    private $contact;
    private $bannerId;

    private $contactData = [];
    private $mockModel = [];


    protected function setUp():void
    {

        $this->contactData = [];
        $this->mockModel = [];

        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();


        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->contact = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Contact')
            ->setMethods(array('update'))
            ->getMock();


        $this->contact->method('update')
            ->with($this->callback(array($this, 'updateWithData')))
            ->will($this->returnCallback(array($this, 'updateMock')));


        $this->contactREST = new \MiamiOH\RestngContactService\Services\ContactREST();

        $this->contactREST->setApp($this->api);
        $this->contactREST->setContact($this->contact);
        $this->contactREST->setBannerUtil($bannerUtil);
        $this->contactREST->setLogger();
    }

    public function testUpdateContactEmergency()
    {
        $this->mockModel = [
            [

                'relation' => 'F',
                'pidm' => '567890',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'areaCode' => '513',
                'phoneNumber' => '5551111',
            ]

        ];

        $this->bannerId->method('getPidm')->willReturn('567890');
        $this->request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);
        $this->contactREST->setRequest($this->request);

        $resp = $this->contactREST->updateContactCollection();


        $payload = $resp->getPayload();


        $this->assertEquals(App::API_OK, $resp->getStatus());
        // $this->assertEquals($payload['data'][0]['code'], App::API_OK, 'Response has code App::API_OK');
    }

    public function testUpdateContactEmergencyByPIDM()
    {

        $this->mockModel = [
            [

                'relation' => 'F',
                'pidm' => '567890',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'areaCode' => '513',
                'phoneNumber' => '5551111',
            ]

        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('567890');

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'emergency']);

        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->updateContactCollection();


        $payload = $resp->getPayload();


        $this->assertEquals(App::API_OK, $resp->getStatus());
    }

    public function testUpdateContactMissingPerson()
    {
        $this->mockModel = [
            [
                'relation' => 'K',
                'pidm' => '567890',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'areaCode' => '513',
                'phoneNumber' => '5551111',
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('jonesl');

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'missingPerson']);

        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->updateContactCollection();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals($payload[0]['status'], App::API_OK,
            'Response has code App::API_OK');
    }

    public function testUpdateContactBadRequest()
    {
        $this->mockModel = [
            [
                'relation' => 'F',
                'pidm' => '234556',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'areaCode' => '513',
                'phoneNumber' => '5551111',
            ],
        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('jonesl');

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'missingPerson']);

        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->updateContactCollection();

        $payload = $resp->getPayload();

        $this->assertEquals($payload[0]['status'],
            App::API_BADREQUEST,
            'Response has code App::API_BADREQUEST');

    }

    public function updateContactPidmMock($subject)
    {
        $this->contactListPidm = $subject;

        return true;
    }

    public function updateContactTypeMock($subject)
    {
        $this->contactListType = $subject;

        return true;
    }

    public function updateContactDataMock()
    {
        return $this->contactData;
    }

    public function updateContactResponseMock()
    {
        $payload = [
            'data' => [
                [
                    'pidm' => '567890',
                    'code' => 200,
                    'message' => 'PUT Success'
                ]
            ],
            'status' => 200,
            'error' => 'false'
        ];

        return $payload;
    }

    public function updateWithData()
    {
        return true;
    }

    public function updateMock()
    {
        return $this->mockModel;
    }
}
