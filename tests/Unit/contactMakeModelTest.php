<?php
/*
-----------------------------------------------------------
FILE NAME: phoneGetTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the Phone Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/12/2015       SCHMIDEE
Description:  Initial Program

 */

namespace MiamiOH\RestngContactService\Tests\Unit;

class ContactMakeModelTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $contact;

    private $phoneHelper;
    private $phoneUtil;

    protected function setUp():void
    {

        $this->phoneUtil = $this->getMockBuilder('\libphonenumber\PhoneNumberUtilMock')
            ->setMethods(array('parse', 'isValidNumber', 'format'))
            ->getMock();

        $this->phoneHelper = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\PhoneHelper')
            ->setMethods(array('getInstance', 'nationalCode', 'internationalCode'))
            ->getMock();

        $this->phoneHelper->method('getInstance')
            ->willReturn($this->phoneUtil);

        $this->phoneHelper->method('nationalCode')
            ->willReturn(1);

        $this->phoneHelper->method('internationalCode')
            ->willReturn(2);

        $this->contact = new \MiamiOH\RestngContactService\Services\Contact();

        $this->contact->setPhoneHelper($this->phoneHelper);
        $this->contact->setLogger();
    }

    public function testPhoneMakeModelUS()
    {
        $record = [
            'spremrg_pidm' => '110518',
            'spremrg_priority' => '1',
            'spremrg_last_name' => 'Smith',
            'spremrg_first_name' => 'Bubba',
            'spremrg_mi' => '',
            'spremrg_phone_area' => '513',
            'spremrg_phone_number' => '1234567',
            'spremrg_phone_ext' => '',
            'spremrg_relt_code' => 'F',
            'spremrg_atyp_code' => 'MA',
            'stvrelt_desc' => '',
        ];

        $this->phoneUtil->method('parse')->will($this->returnSelf());
        $this->phoneUtil->method('isValidNumber')->willReturn(true);
        $this->phoneUtil->method('format')->will($this->onConsecutiveCalls(
            '(513) 123-4567', '+1 513-123-4567'
        ));

        $model = $this->contact->makeModelFromRecord($record);

        $this->assertEquals($record['spremrg_pidm'], $model['pidm']);
        $this->assertEquals($record['stvrelt_desc'], $model['relation']);
        $this->assertEquals($record['spremrg_relt_code'], $model['relationCode']);
        $this->assertEquals($record['spremrg_priority'], $model['priority']);
        $this->assertEquals($record['spremrg_first_name'], $model['firstName']);
        $this->assertEquals($record['spremrg_last_name'], $model['lastName']);
        $this->assertEquals($record['spremrg_mi'], $model['middleName']);

        $this->assertEquals('+1' . $record['spremrg_phone_area'] . $record['spremrg_phone_number'],
            $model['phoneNumber']);
        $this->assertEquals('(513) 123-4567', $model['phoneNumberNational']);
        $this->assertEquals('+1 513-123-4567', $model['phoneNumberInternational']);

    }

    public function testPhoneMakeModelInternational()
    {
        $record = [
            'spremrg_pidm' => '110518',
            'spremrg_priority' => '1',
            'spremrg_last_name' => 'Smith',
            'spremrg_first_name' => 'Bubba',
            'spremrg_mi' => '',
            'spremrg_phone_area' => '513',
            'spremrg_phone_number' => '12345',
            'spremrg_phone_ext' => '',
            'spremrg_relt_code' => 'F',
            'spremrg_atyp_code' => 'MA',
            'stvrelt_desc' => '',
        ];

        $this->phoneUtil->method('parse')->will($this->returnSelf());
        $this->phoneUtil->method('isValidNumber')->willReturn(true);
        $this->phoneUtil->method('format')->will($this->onConsecutiveCalls(
            '(513) 123-4567', '+1 513-123-4567'
        ));

        $model = $this->contact->makeModelFromRecord($record);

        $this->assertEquals($record['spremrg_pidm'], $model['pidm']);
        $this->assertEquals($record['stvrelt_desc'], $model['relation']);
        $this->assertEquals($record['spremrg_relt_code'], $model['relationCode']);
        $this->assertEquals($record['spremrg_priority'], $model['priority']);
        $this->assertEquals($record['spremrg_first_name'], $model['firstName']);
        $this->assertEquals($record['spremrg_last_name'], $model['lastName']);
        $this->assertEquals($record['spremrg_mi'], $model['middleName']);

        $this->assertEquals('+' . $record['spremrg_phone_area'] . $record['spremrg_phone_number'],
            $model['phoneNumber']);

    }

    public function testPhoneMakeModelBadPhone()
    {
        $record = [
            'spremrg_pidm' => '110518',
            'spremrg_priority' => '1',
            'spremrg_last_name' => 'Smith',
            'spremrg_first_name' => 'Bubba',
            'spremrg_mi' => '',
            'spremrg_phone_area' => '5',
            'spremrg_phone_number' => '12345',
            'spremrg_phone_ext' => '',
            'spremrg_relt_code' => 'F',
            'spremrg_atyp_code' => 'MA',
            'stvrelt_desc' => '',
        ];

        $this->phoneUtil->method('parse')
            ->will($this->throwException(new \libphonenumber\NumberParseException(0,
                'Bad number')));

        $model = $this->contact->makeModelFromRecord($record);

        $this->assertEquals($record['spremrg_pidm'], $model['pidm']);
        $this->assertEquals($record['stvrelt_desc'], $model['relation']);
        $this->assertEquals($record['spremrg_relt_code'], $model['relationCode']);
        $this->assertEquals($record['spremrg_priority'], $model['priority']);
        $this->assertEquals($record['spremrg_first_name'], $model['firstName']);
        $this->assertEquals($record['spremrg_last_name'], $model['lastName']);
        $this->assertEquals($record['spremrg_mi'], $model['middleName']);

        $this->assertEquals('+' . $record['spremrg_phone_area'] . $record['spremrg_phone_number'],
            $model['phoneNumber']);

        $this->assertEquals('+512345', $model['phoneNumberNational']);
        $this->assertEquals('+512345', $model['phoneNumberInternational']);
    }

}
