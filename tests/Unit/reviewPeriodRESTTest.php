<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ReviewPeriodRESTTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $reviewPeriodREST;

    private $api;
    private $reviewPeriod;

    private $rpListData = [];
    private $rpListType = '';

    protected function setUp():void
    {

        $this->rpListData = [];
        $this->rpListType = '';

        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->reviewPeriod = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\ReviewPeriod')
            ->setMethods(array('getReviewPeriod'))
            ->getMock();

        $this->reviewPeriod->method('getReviewPeriod')
            ->with($this->callback(array($this, 'getReviewPeriodType')))
            ->will($this->returnCallback(array($this, 'getReviewPeriodMock')));

        $this->reviewPeriodREST = new \MiamiOH\RestngContactService\Services\ReviewPeriodREST();

        $this->reviewPeriodREST->setApp($this->api);
        $this->reviewPeriodREST->setReviewPeriod($this->reviewPeriod);
        $this->reviewPeriodREST->setLogger();
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Missing required type option
     */
    public function testGetReviewPeriodRequiresType()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn([]);

        $this->reviewPeriodREST->setRequest($request);

        $resp = $this->reviewPeriodREST->getCurrentReviewPeriod();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Invalid type for reviewPeriod
     */
    public function testGetReviewPeriodRequiresValidType()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'bob']);

        $this->reviewPeriodREST->setRequest($request);

        $resp = $this->reviewPeriodREST->getCurrentReviewPeriod();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function testGetReviewPeriodStudent()
    {

        $this->rpListData = [
            [
                'pidm' => 123456,
                'uniqueId' => 'doej',
                'reviewStatus' => 'p',
                'type' => 'student'
            ],
            [
                'pidm' => 987654,
                'uniqueId' => 'smithd',
                'reviewStatus' => 'p',
                'type' => 'student'
            ],
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'student']);

        $this->reviewPeriodREST->setRequest($request);

        $resp = $this->reviewPeriodREST->getCurrentReviewPeriod();

        $this->assertEquals('student', $this->rpListType,
            'Review required list called with expected type');

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(2, count($payload));

    }

    public function testGetReviewPeriodEmployee()
    {

        $this->rpListData = [
            [
                'pidm' => 123456,
                'uniqueId' => 'doej',
                'reviewStatus' => 'p',
                'type' => 'employee'
            ],
            [
                'pidm' => 987654,
                'uniqueId' => 'smithd',
                'reviewStatus' => 'p',
                'type' => 'employee'
            ],
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'employee']);

        $this->reviewPeriodREST->setRequest($request);

        $resp = $this->reviewPeriodREST->getCurrentReviewPeriod();

        $this->assertEquals('employee', $this->rpListType,
            'Review required list called with expected type');

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(2, count($payload));

    }

    public function getReviewPeriodType($subject)
    {
        $this->rpListType = $subject;

        return true;
    }

    public function getReviewPeriodMock()
    {
        return $this->rpListData;
    }
}
