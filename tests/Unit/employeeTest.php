<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Util\Configuration;


class EmployeeTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $employee;

    private $dbh;

    private $record = [];
    private $queryString = '';
    private $queryParams = [];

    private $configuration;

    protected function setUp():void
    {

        $this->record = [];
        $this->queryString = '';
        $this->queryParams = [];

        $this->dbh = $this->createMock(DBH::class);

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);

        $db->method('getHandle')->willReturn($this->dbh);

        $this->employee = new \MiamiOH\RestngContactService\Services\Employee();

        $this->configuration = $this->createMock(Configuration::class);
        $this->configuration->method('getConfiguration')->willReturn([
            'newEmployeeCutOffDate' => '0901'
        ]);

        $this->employee->setDatabase($db);
        $this->employee->setLogger();
        $this->employee->setConfiguration($this->configuration);
    }

    public function testGetEmployeesForReview()
    {
        $this->record = [
            [
                'pebempl_pidm' => '110518',
                'szbuniq_unique_id' => 'DOEJ',
                'review_status' => 'requested',
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->employee->setReviewState('requested');
        $this->employee->setTermCode('202110');

        $records = $this->employee->getEmployeesForReview();

        $this->assertTrue(is_array($records));

        $this->assertTrue(in_array('requested', $this->queryParams));
        $this->assertTrue(in_array(date('Y').'0901', $this->queryParams));

        $this->assertEquals('110518', $records[0]['pidm']);
        $this->assertEquals('doej', $records[0]['uniqueId']);
        $this->assertEquals('requested', $records[0]['reviewStatus']);
    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryfirstrow_assocWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryfirstrow_assocMock()
    {
        return $this->record;
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->record;
    }
}
