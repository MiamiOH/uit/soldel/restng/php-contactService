<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RestngContactService\Services\Pledge;
use MiamiOH\RestngContactService\Tests\BaseTestCase;

class PledgeTest extends BaseTestCase
{

    /**
     * @var \MiamiOH\RestngContactService\Services\Pledge
     */
    private $pledge;

    public function setUp(): void
    {
        parent::setUp();
        $this->pledge = new Pledge();
        $this->pledge->setApp($this->api);
        $this->pledge->setDatabase($this->db);


    }

    public function testCanGetPledgeData()
    {
        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];
        $queryReturn = [
            [
                'id' => '11115',
                'uniqueid' => 'doej',
                'type' => 'oxf-student',
                'response' => 'Agree',
                'response_time' => '2020-07-29 14:21:15'
            ]
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->willReturn($queryReturn);

        $this->pledge->setRequest($this->request);

        $response = $this->pledge->getPledgeByUniqueId();

        $this->assertInstanceOf(Response::class, $response);

        $this->assertEquals(APP::API_OK, $response->getStatus());

        $pledgeResponses = $response->getPayload();
        $this->assertCount(1, $pledgeResponses);
        $pledgeInfo = $pledgeResponses[0];
        $this->assertTrue(is_array($pledgeInfo));
        $this->assertEquals('doej', $pledgeInfo['uniqueId']);
        $this->assertEquals('oxf-student', $pledgeInfo['type']);
        $this->assertEquals('Agree', $pledgeInfo['response']);
        $this->assertEquals('2020-07-29 14:21:15', $pledgeInfo['responseTime']);
    }

    public function testNoPledgeResponseAvailable()
    {
        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->willReturn([]);
        $this->pledge->setRequest($this->request);

        $response = $this->pledge->getPledgeByUniqueId();

        $pledgeResponses = $response->getPayload();
        $this->assertCount(0, $pledgeResponses);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(APP::API_OK, $response->getStatus());
    }

    public function testCanGetMultiplePledgeDataResults()
    {
        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];
        $queryReturn = [
            [
                'id' => '11115',
                'uniqueid' => 'doej2',
                'type' => 'oxf-student',
                'response' => 'Agree',
                'response_time' => '2020-07-29 14:21:15'
            ],
            [
                'id' => '11135',
                'uniqueid' => 'doej2',
                'type' => 'oxf-student',
                'response' => 'Agree',
                'response_time' => '2020-08-01 16:21:15'
            ]
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->willReturn($queryReturn);

        $this->pledge->setRequest($this->request);

        $response = $this->pledge->getPledgeByUniqueId();

        $this->assertInstanceOf(Response::class, $response);

        $this->assertEquals(APP::API_OK, $response->getStatus());

        $pledgeResponses = $response->getPayload();
        $this->assertCount(2, $pledgeResponses);
        $pledgeInfo = $pledgeResponses[0];
        $this->assertTrue(is_array($pledgeInfo));
        $this->assertEquals('doej2', $pledgeInfo['uniqueId']);
        $this->assertEquals('oxf-student', $pledgeInfo['type']);
        $this->assertEquals('Agree', $pledgeInfo['response']);
        $this->assertEquals('2020-07-29 14:21:15', $pledgeInfo['responseTime']);
    }

    public function testCanCreateAPledgeRecord()
    {
        $queryReturn = [
            'id' => '11115',
            'uniqueid' => 'doejC',
            'type' => 'oxf-student',
            'response' => 'Agree',
            'response_time' => '2020-07-11 18:22:12'
        ];

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->willReturn($queryReturn);

        $this->request->expects($this->once())->method('getData')->willReturn(
            [
                'id' => '11111',
                'uniqueId' => 'doejC',
                'type' => 'oxf-student',
                'response' => 'Agree',
                'responseTime' => '2020-07-11 18:22:12'
            ]
        );

        $this->pledge->setRequest($this->request);

        $response = $this->pledge->createPledgeResponse();
        $this->assertInstanceOf(Response::class, $response);
        $pledgeResult = $response->getPayload();
        $this->assertEquals('doejC', $pledgeResult['uniqueId']);
    }


}
