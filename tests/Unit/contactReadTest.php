<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;

class ContactReadTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $contact;

    private $dbh;
    private $phoneHelper;
    private $phoneUtil;

    private $records = [];
    private $queryString = '';
    private $queryParams = [];

    protected function setUp():void
    {

        $this->records = [];
        $this->queryString = '';
        $this->queryParams = [];

        $this->phoneUtil = $this->getMockBuilder('\libphonenumber\PhoneNumberUtilMock')
            ->setMethods(array('parse', 'isValidNumber', 'format'))
            ->getMock();

        $this->phoneHelper = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\PhoneHelper')
            ->setMethods(array('getInstance', 'nationalCode', 'internationalCode'))
            ->getMock();

        $this->phoneHelper->method('getInstance')
            ->willReturn($this->phoneUtil);

        $this->phoneHelper->method('nationalCode')
            ->willReturn(1);

        $this->phoneHelper->method('internationalCode')
            ->willReturn(2);

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->createMock(DatabaseFactory::class);

        $db->method('getHandle')->willReturn($this->dbh);

        $this->contact = new \MiamiOH\RestngContactService\Services\Contact();

        $this->contact->setDatabase($db);
        $this->contact->setPhoneHelper($this->phoneHelper);
        $this->contact->setLogger();
    }

    public function testContactRead()
    {
        $pidm = ['110518'];

        $this->records = [
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '1',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'Bubba',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'F',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '2',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'John',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'G',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->contact->read($pidm);

        $this->assertTrue(in_array($pidm, $this->queryParams));
        $this->assertTrue(stripos($this->queryString, 'FROM SPREMRG') !== false,
            'Query contains from SPREMRG');

        $this->assertEquals(2, count($models));

        $record = $this->records[0];
        $model = $models[0];

        $this->assertEquals($record['spremrg_pidm'], $model['pidm']);

    }

    public function testContactReadByRelationType()
    {

        $pidm = '110518';
        $types = ['F'];

        $this->records = [
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '1',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'Bubba',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'F',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->contact->filterRelationType($types)->read($pidm);

        foreach ($types as $type) {
            $this->assertTrue(in_array($type, $this->queryParams));
        }
        $this->assertTrue(stripos($this->queryString,
                'spremrg_relt_code in') !== false,
            'Query contains spremrg_relt_code in condition');

    }

    public function testContactReadByContactTypeMissingPerson()
    {

        $pidm = '110518';
        $type = 'missingPerson';

        $this->records = [
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '1',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'Bubba',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'F',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->contact->filterContactType($type)->read($pidm);

        $this->assertTrue(stripos($this->queryString,
                'spremrg_relt_code =') !== false,
            'Query includes spremrg_relt_code comparison');
        $this->assertTrue(in_array('K', $this->queryParams,
            'Query params contain type code'));

    }

    public function testContactReadByContactTypeEmergency()
    {

        $pidm = '110518';
        $type = 'emergency';

        $this->records = [
            [
                'spremrg_pidm' => '110518',
                'spremrg_priority' => '1',
                'spremrg_last_name' => 'Smith',
                'spremrg_first_name' => 'Bubba',
                'spremrg_mi' => '',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '1234567',
                'spremrg_phone_ext' => '',
                'spremrg_relt_code' => 'F',
                'spremrg_street_line1' => '123 Any Street',
                'spremrg_street_line2' => '',
                'spremrg_street_line3' => '',
                'spremrg_city' => 'Springfield',
                'spremrg_stat_code' => 'OH',
                'spremrg_natn_code' => '',
                'spremrg_zip' => '45000',
                'spremrg_atyp_code' => 'MA',
                'stvrelt_desc' => '',
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $models = $this->contact->filterContactType($type)->read($pidm);

        $this->assertTrue(stripos($this->queryString,
                'spremrg_relt_code !=') !== false,
            'Query includes spremrg_relt_code comparison');
        $this->assertTrue(in_array('K', $this->queryParams,
            'Query params contain type code'));

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Filtering by contact type and relation type cannot
     *     be used together
     */
    public function testContactReadByContactAndRelationType()
    {

        $pidm = '110518';
        $models = $this->contact->filterContactType('emergency')->filterRelationType('F')->read($pidm);

        $this->assertTrue(stripos($this->queryString,
                'spremrg_relt_code !=') !== false,
            'Query includes spremrg_relt_code comparison');
        $this->assertTrue(in_array('K', $this->queryParams,
            'Query params contain type code'));

    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }
}
