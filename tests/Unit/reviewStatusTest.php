<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

class ReviewStatusTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $reviewStatus;

    private $dbh;
    private $sth;

    private $records = [];
    private $recordIndex = 0;
    private $queryString = '';
    private $queryParams = [];

    protected function setUp():void
    {

        $this->records = [];
        $this->recordIndex = 0;
        $this->queryString = '';
        $this->queryParams = [];

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('execute', 'fetchrow_assoc'))
            ->getMock();

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('prepare'))
            ->getMock();

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);


        $db->method('getHandle')->willReturn($this->dbh);

        $this->reviewStatus = new \MiamiOH\RestngContactService\Services\ReviewStatus();

        $this->reviewStatus->setDatabase($db);
    }

    public function testGetReviewStatusListStudent()
    {

        $this->records = [
            [
                'status_id' => 1,
                'pidm' => 123,
                'uniqueid' => 'doej',
                'review_type' => 'student',
                'review_status' => 'requested',
                'review_date' => '',
                'add_date' => '2016-06-06'
            ]
        ];

        $expectedStatus = 'requested';

        $this->dbh->method('prepare')
            ->with($this->callback(array($this, 'prepareWithQuery')))
            ->willReturn($this->sth);

        $this->sth->method('execute')
            ->with($this->callback(array($this, 'executeWithParams')))
            ->willReturn(true);

        $this->sth->method('fetchrow_assoc')
            ->will($this->returnCallback(array($this, 'fetchrow_assocMock')));

        $list = $this->reviewStatus->getReviewStatusList(['type' => 'student'], 0, 0,
            0);

        $this->assertTrue(stripos($this->queryString,
                'from safmgr.person_contact_activity') !== false,
            'Query from person_contact_activity');
        $this->assertTrue(stripos($this->queryString, 'review_type = ?') !== false
            && in_array('student', $this->queryParams),
            'Query review status for student only');

        $this->assertEquals(count($this->records), count($list),
            'Review status list has expected number of records');
        $this->assertEquals(7, count($list[0]),
            'Returned model has correct number of attributes');
        $this->assertTrue(array_key_exists('statusId', $list[0]));
        $this->assertTrue(array_key_exists('pidm', $list[0]));
        $this->assertTrue(array_key_exists('uniqueId', $list[0]));
        $this->assertTrue(array_key_exists('type', $list[0]));
        $this->assertTrue(array_key_exists('reviewStatus', $list[0]));
        $this->assertTrue(array_key_exists('reviewDate', $list[0]));
        $this->assertTrue(array_key_exists('addDate', $list[0]));

        $this->assertEquals($this->records[0]['status_id'], $list[0]['statusId']);
        $this->assertEquals($this->records[0]['pidm'], $list[0]['pidm']);
        $this->assertEquals($this->records[0]['uniqueid'], $list[0]['uniqueId']);
        $this->assertEquals($this->records[0]['review_type'], $list[0]['type']);
        $this->assertEquals($expectedStatus, $list[0]['reviewStatus']);
        $this->assertEquals($this->records[0]['review_date'],
            $list[0]['reviewDate']);
        $this->assertEquals($this->records[0]['add_date'], $list[0]['addDate']);

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Invalid type "bob" for getReviewStatusList
     */
    public function testGetReviewStatusListRequiresValidType()
    {

        $list = $this->reviewStatus->getReviewStatusList(['type' => 'bob'], 0, 0, 0);

    }

    public function testGetReviewStatusListRequested()
    {

        $this->records = [
            [
                'status_id' => 1,
                'pidm' => 123,
                'uniqueid' => 'doej',
                'review_type' => 'student',
                'review_status' => 'requested',
                'review_date' => '',
                'add_date' => '2016-06-06'
            ]
        ];
        $this->dbh->method('prepare')
            ->with($this->callback(array($this, 'prepareWithQuery')))
            ->willReturn($this->sth);

        $this->sth->method('execute')
            ->with($this->callback(array($this, 'executeWithParams')))
            ->willReturn(true);

        $this->sth->method('fetchrow_assoc')
            ->will($this->returnCallback(array($this, 'fetchrow_assocMock')));

        $list = $this->reviewStatus->getReviewStatusList(['status' => ['requested']],
            0, 0, 0);

        $this->assertTrue(stripos($this->queryString,
                'from safmgr.person_contact_activity') !== false,
            'Query from person_contact_activity');
        $this->assertTrue(stripos($this->queryString, 'review_status in') !== false
            && in_array('requested', $this->queryParams),
            'Query review status for requested only');

        $this->assertEquals(count($this->records), count($list),
            'Review status list has expected number of records');
        $this->assertEquals(7, count($list[0]),
            'Returned model has correct number of attributes');

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Invalid type "bob" for getReviewStatusList
     */
    public function testGetReviewStatusListStudentValidType()
    {

        $this->reviewStatus->getReviewStatusList(['type' => 'bob'], 0, 0, 0);

    }

    public function prepareWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function executeWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function fetchrow_assocMock()
    {
        if ($this->recordIndex >= count($this->records)) {
            return false;
        }

        $index = $this->recordIndex;
        $this->recordIndex++;

        return $this->records[$index];
    }
}
