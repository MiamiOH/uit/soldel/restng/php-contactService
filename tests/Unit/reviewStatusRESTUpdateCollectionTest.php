<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ReviewStatusRESTUpdateCollectionTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $reviewStatusREST;

    private $api;
    private $reviewStatus;

    private $setStatusResponse = [];
    private $setStatusModel = [];

    protected function setUp():void
    {

        $this->setStatusResponse = [];
        $this->setStatusModel = [];

        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->reviewStatus = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\ReviewStatus')
            ->setMethods(array('setStatus'))
            ->getMock();

        $this->reviewStatusREST = new \MiamiOH\RestngContactService\Services\ReviewStatusREST();

        $this->reviewStatusREST->setApp($this->api);
        $this->reviewStatusREST->setReviewStatus($this->reviewStatus);
        $this->reviewStatusREST->setLogger();
    }

    public function testGetReviewStatusListStudentUpdate()
    {

        $this->setStatusResponse = [
            true,
            true,
        ];

        $updateModels = [
            [
                'uniqueId' => 'doej',
                'pidm' => 123456,
                'reviewRequired' => true,
                'reviewStatus' => 'requested',
                'type' => 'student'
            ],
            [
                'uniqueId' => 'southrd',
                'pidm' => 987654,
                'reviewRequired' => true,
                'reviewStatus' => 'complete',
                'type' => 'student'
            ],
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')
            ->willReturn($updateModels);

        $this->reviewStatus->method('setStatus')
            ->with($this->callback(array($this, 'setStatusWithModel')))
            ->will($this->returnCallback(array($this, 'setStatusMock')));

        $this->reviewStatusREST->setRequest($request);

        $resp = $this->reviewStatusREST->updateReviewStatusCollection();

        $this->assertEquals(App::API_OK, $resp->getStatus());

        $payload = $resp->getPayload();

        $this->assertEquals(count($updateModels), count($payload),
            'Correct number of records updated');

    }

    public function setStatusWithModel($subject)
    {
        $this->setStatusModel[] = $subject;

        return true;
    }

    public function setStatusMock()
    {
        return array_shift($this->setStatusResponse);
    }

}
