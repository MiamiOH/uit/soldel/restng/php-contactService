<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

class ContactCreateTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $contact;

    private $dbh;
    private $sth;
    private $rowID;

    private $bindPlaceHolder = '';
    private $boundValues = [];
    private $queryNumber = 0;
    private $queries = [];
    private $queryMockResponses = [];
    private $records = [];
    private $phoneHelper;
    private $phoneUtil;
    private $mockApiUsername = '';

    protected function setUp():void
    {

        $this->bindPlaceHolder = '';
        $this->boundValues = [];
        $this->queryNumber = 0;
        $this->queries = [];
        $this->queryMockResponses = [];
        $this->records = [];

        $this->apiUser = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('getUsername'))
            ->getMock();

        $this->phoneUtil = $this->getMockBuilder('\libphonenumber\PhoneNumberUtilMock')
            ->setMethods(array('parse', 'isValidNumber', 'format'))
            ->getMock();

        $this->phoneHelper = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\PhoneHelper')
            ->setMethods(array('getInstance', 'nationalCode', 'internationalCode'))
            ->getMock();

        $this->phoneHelper->method('getInstance')
            ->willReturn($this->phoneUtil);

        $this->phoneHelper->method('nationalCode')
            ->willReturn(1);

        $this->phoneHelper->method('internationalCode')
            ->willReturn(2);

        $this->rowID = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\RowIDMock')
            ->setMethods(array('free'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array(
                'execute',
                'bind_by_name',
                'new_descriptor',
                'fetchrow_assoc'
            ))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->with($this->callback(array($this, 'bind_by_nameWithName')),
                $this->callback(array($this, 'bind_by_nameWithValue')))
            ->willReturn(true);

        $this->sth->method('fetchrow_assoc')
            ->will($this->returnCallback(array($this, 'fetchrow_assocMock')));

        $this->sth->method('new_descriptor')
            ->willReturn($this->rowID);

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('prepare', 'queryfirstcolumn', 'queryall_array'))
            ->getMock();

        $this->dbh->method('prepare')
            ->with($this->callback(array($this, 'prepareWithQuery')))
            ->willReturn($this->sth);;

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);


        $db->method('getHandle')->willReturn($this->dbh);

        $this->contact = new \MiamiOH\RestngContactService\Services\Contact();

        $this->contact->setDatabase($db);
        $this->contact->setApiUser($this->apiUser);
        $this->contact->setPhoneHelper($this->phoneHelper);
        $this->contact->setLogger();
    }

    public function testContactCreate()
    {

        $this->queryMockResponses[] = 1;

        $this->queryMockResponses[] = 'F';

        $model = [
            'relation' => 'Father',
            'relationCode' => 'F',
            'pidm' => '567890',
            'priority' => '1',
            'firstName' => 'John',
            'lastName' => 'Doe',
            'middleName' => 'M',
            'phoneNumber' => '+15134567890',
        ];

        $this->records = [
            [
                'stvrelt_desc' => 'Father',
                'spremrg_relt_code' => 'F',
                'spremrg_pidm' => '567890',
                'spremrg_priority' => '1',
                'spremrg_first_name' => 'John',
                'spremrg_last_name' => 'Doe',
                'spremrg_middle_name' => 'M',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '4567890',

            ]
        ];
        $parsedPhoneNumber = [
            'countryCode:protected' => 1,
            'nationalNumber:protected' => 5134567890,
            'extenstion:protected' => null,
            'italianLeadingZero:protected' => null,
            'rawInput:protected' => +15134567890,
            'countryCodeSource:protected' => 0,
            'preferredDomesticCarrierCode:protected' => null,
            'hasNumberOfLeadingZeros:protected' => null,
            'numberOfLeadingZeros:protected' => 1,
        ];

        $this->dbh->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->phoneUtil->method('parse')
            ->with('+15134567890')
            ->willReturn($parsedPhoneNumber);

        $this->phoneUtil->method('isValidNumber')
            ->with($parsedPhoneNumber)
            ->willReturn('1');

        $this->phoneUtil->method('format')
            ->with($parsedPhoneNumber)
            ->willReturn('(513) 456-7890');

        $result = $this->contact->create($model);


        $this->assertEquals('567890', $this->boundValues[':P_PIDM'],
            'PIDM is bound to 567890');
        $this->assertEquals('F', $this->boundValues[':P_RELT_CODE'],
            'Relation is bound to F');
        $this->assertEquals('M', $this->boundValues[':P_MID_NAME'],
            'Middle name is bound to M');
        $this->assertEquals('2', $this->boundValues[':P_PRIORITY'],
            'Priority has been incremented');
        $this->assertEquals('513', $this->boundValues[':P_PHONE_AREA'],
            'Area code is correct');
        $this->assertEquals('4567890', $this->boundValues[':P_PHONE_NUMBER'],
            'Phone number is correct');

    }

    public function testContactCreateTypeMissingPerson()
    {

        $this->queryMockResponses[] = 1;

        $this->queryMockResponses[] = 1;

        $model = [
            'relation' => '',
            'relationCode' => '',
            'pidm' => '567890',
            'priority' => '1',
            'firstName' => 'John',
            'lastName' => 'Doe',
            'middleName' => 'M',
            'phoneNumber' => '+15134567890',
        ];

        $this->records = [
            [
                'stvrelt_desc' => 'Missing Person Contact',
                'spremrg_relt_code' => 'F',
                'spremrg_pidm' => '567890',
                'spremrg_priority' => '1',
                'spremrg_first_name' => 'John',
                'spremrg_last_name' => 'Doe',
                'spremrg_middle_name' => 'M',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '4567890',

            ]
        ];
        $parsedPhoneNumber = [
            'countryCode:protected' => 1,
            'nationalNumber:protected' => 5134567890,
            'extenstion:protected' => null,
            'italianLeadingZero:protected' => null,
            'rawInput:protected' => +15134567890,
            'countryCodeSource:protected' => 0,
            'preferredDomesticCarrierCode:protected' => null,
            'hasNumberOfLeadingZeros:protected' => null,
            'numberOfLeadingZeros:protected' => 1,
        ];

        $this->dbh->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->phoneUtil->method('parse')
            ->with('+15134567890')
            ->willReturn($parsedPhoneNumber);

        $this->phoneUtil->method('isValidNumber')
            ->with($parsedPhoneNumber)
            ->willReturn('1');

        $this->phoneUtil->method('format')
            ->with($parsedPhoneNumber)
            ->willReturn('(513) 456-7890');

        $this->contact->create($model, 'missingPerson');

        $this->assertEquals('567890', $this->boundValues[':P_PIDM'],
            'PIDM is bound to 567890');
        $this->assertEquals('K', $this->boundValues[':P_RELT_CODE'],
            'Relation is bound to K');

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Invalid relation code F for missingPerson type
     */
    public function testContactCreateTypeCodeMismatch()
    {

        $this->queryMockResponses[] = 1;

        $this->queryMockResponses[] = 'F';

        $model = [
            'relation' => '',
            'relationCode' => 'F',
            'pidm' => '567890',
            'priority' => '1',
            'firstName' => 'John',
            'lastName' => 'Doe',
            'middleName' => 'M',
            'phoneNumber' => '+15135551111',
        ];

        $this->contact->create($model, 'missingPerson');

    }

    /**
     * @expectedException Exception
     */
    public function testContactCreateFailedWitMaxPriorityReached()
    {

        $this->queryMockResponses[] = 10;

        $this->queryMockResponses[] = 'F';

        $model = [
            'relation' => 'Father',
            'relationCode' => 'F',
            'pidm' => '567890',
            'priority' => '1',
            'firstName' => 'John',
            'lastName' => 'Doe',
            'middleName' => 'M',
            'phoneNumber' => '+15134567890',
        ];

        $this->records = [
            [
                'stvrelt_desc' => 'Father',
                'spremrg_relt_code' => 'F',
                'spremrg_pidm' => '567890',
                'spremrg_priority' => '1',
                'spremrg_first_name' => 'John',
                'spremrg_last_name' => 'Doe',
                'spremrg_middle_name' => 'M',
                'spremrg_phone_area' => '513',
                'spremrg_phone_number' => '4567890',

            ]
        ];
        $parsedPhoneNumber = [
            'countryCode:protected' => 1,
            'nationalNumber:protected' => 5134567890,
            'extenstion:protected' => null,
            'italianLeadingZero:protected' => null,
            'rawInput:protected' => +15134567890,
            'countryCodeSource:protected' => 0,
            'preferredDomesticCarrierCode:protected' => null,
            'hasNumberOfLeadingZeros:protected' => null,
            'numberOfLeadingZeros:protected' => 1,
        ];

        $this->dbh->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->phoneUtil->method('parse')
            ->with('+15134567890')
            ->willReturn($parsedPhoneNumber);

        $this->phoneUtil->method('isValidNumber')
            ->with($parsedPhoneNumber)
            ->willReturn('1');

        $this->phoneUtil->method('format')
            ->with($parsedPhoneNumber)
            ->willReturn('(513) 456-7890');

        $result = $this->contact->create($model);
    }

    public function bind_by_nameWithName($subject)
    {
        $this->bindPlaceHolder = $subject;

        return true;
    }

    public function bind_by_nameWithValue($subject)
    {
        $this->boundValues[$this->bindPlaceHolder] = $subject;
        $this->bindPlaceHolder = '';

        return true;
    }

    public function prepareWithQuery($subject)
    {
        $this->queries[$this->queryNumber] = [
            'query' => $subject,
            'params' => [],
        ];

        return true;
    }

    public function executeArrayMock()
    {
        return $this->queryMockResponses[$this->queryNumber++];
    }

    public function queryfirstcolumnWithQuery($subject)
    {
        $this->queries[$this->queryNumber] = [
            'query' => $subject,
            'params' => [],
        ];

        return true;
    }

    public function queryfirstcolumnWithParams($subject)
    {
        $this->queries[$this->queryNumber]['params'] = $subject;

        return true;
    }

    public function queryfirstcolumnMock()
    {
        return $this->queryMockResponses[$this->queryNumber++];
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }

    public function fetchrow_assocMock()
    {
        return $this->records[0];
    }

    public function getUsernameMock()
    {
        return $this->mockApiUsername;
    }
}
