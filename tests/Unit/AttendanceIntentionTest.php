<?php

namespace MiamiOH\RestngContactService\tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RestngContactService\Services\AttendanceIntention;
use MiamiOH\RestngContactService\Tests\BaseTestCase;

class AttendanceIntentionTest extends BaseTestCase
{
    private $intention;

    public function setUp(): void
    {
        parent::setUp();
        $this->intention = new AttendanceIntention();
        $this->intention->setApp($this->api);
        $this->intention->setDatabase($this->db);
    }

    public function testCanGetAnIntention()
    {
        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];
        $queryRecords = [
            [
                'id' => '11111',
                'uniqueid' => 'doej',
                'choice' => 'delayedStart',
                'intention_type' => 'oxf-stu',
                'delayed_start_date' => '2020-09-07',
                'response_time' => '2020-07-22 18:22:12',
                'additional_choice_info' => 'Yes'
            ]
        ];
        $this->dbh->expects($this->once())->method('queryall_array')
            ->willReturn($queryRecords);
        $this->intention->setRequest($this->request);
        $response = $this->intention->getIntention();

        $intentionResponse = $response->getPayload();
        $this->assertCount(1, $intentionResponse);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(APP::API_OK, $response->getStatus());
    }

    public function testCanGetAnIntentionWithNoAdditionalChoiceText()
    {
        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];
        $queryRecords = [
            [
                'id' => '11111',
                'uniqueid' => 'doej',
                'choice' => 'delayedStart',
                'intention_type' => 'oxf-stu',
                'delayed_start_date' => '2020-09-07',
                'response_time' => '2020-07-22 18:22:12',
                'additional_choice_info' => null
            ]
        ];
        $this->dbh->expects($this->once())->method('queryall_array')
            ->willReturn($queryRecords);
        $this->intention->setRequest($this->request);
        $response = $this->intention->getIntention();

        $intentionResponse = $response->getPayload();
        $this->assertCount(1, $intentionResponse);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(APP::API_OK, $response->getStatus());
        $this->assertArrayHasKey('additionalChoiceInfo', $intentionResponse[0]);

    }

    public function testCanReturnEmptyArrayOfIntentions()
    {
        $this->requestResourceParamMocks = [
            'muid' => 'doej2',
        ];
        $queryRecords = [];

        $this->intention->setRequest($this->request);
        $this->dbh->expects($this->once())->method('queryall_array')
            ->willReturn($queryRecords);

        $response = $this->intention->getIntention();

        $intentionResponse = $response->getPayload();
        $this->assertCount(0, $intentionResponse);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(APP::API_OK, $response->getStatus());
    }

    public function testCanDeleteAnIntention()
    {
        $this->request->expects($this->once())->method('getResourceParam')->willReturn('doej');
        $this->dbh->expects($this->once())->method('perform');
        $this->intention->setRequest($this->request);
        $response = $this->intention->deleteIntention();
        $this->assertEquals(APP::API_OK, $response->getStatus());
    }

    public function testCanReturnMultipleIntentions()
    {

        $queryRecords = [
            [
                'id' => '11111',
                'uniqueid' => 'doej',
                'choice' => 'delayedStart',
                'intention_type' => 'oxf-stu',
                'delayed_start_date' => '2020-09-07',
                'response_time' => '2020-07-22 18:22:12',
                'additional_choice_info' => 'Yes'
            ],
            [
                'id' => '11111',
                'uniqueid' => 'doej',
                'choice' => 'delayedStart',
                'intention_type' => 'oxf-stu',
                'delayed_start_date' => '2020-09-07',
                'response_time' => '2020-07-22 18:22:12',
                'additional_choice_info' => 'Yes'
            ]

        ];

        $this->intention->setRequest($this->request);
        $this->dbh->expects($this->once())->method('queryall_array')
            ->willReturn($queryRecords);

        $response = $this->intention->getIntention();

        $intentionResponse = $response->getPayload();
        $this->assertCount(2, $intentionResponse);
        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(APP::API_OK, $response->getStatus());
    }

    public function testCanCreateASingleIntentionRecord()
    {
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $request->expects($this->once())->method('getData')->willReturn(
            [
                'id' => '11111',
                'uniqueId' => 'doejC',
                'choice' => 'delayedStart',
                'intentionType' => 'oxf-stu',
                'delayedStartDate' => '2020-08-11',
                'responseTime' => '2020-07-11 18:22:12',
                'additionalChoiceInfo' => 'Yes'
            ]
        );

        $this->intention->setRequest($request);
        $response = $this->intention->createAttendanceIntention();
        $intentionResponse = $response->getPayload();
        $this->assertArrayHasKey('uniqueId', $intentionResponse);
        $this->assertArrayHasKey('choice', $intentionResponse);
        $this->assertArrayHasKey('delayedStartDate', $intentionResponse);
        $this->assertArrayHasKey('responseTime', $intentionResponse);
        $this->assertArrayHasKey('intentionType', $intentionResponse);
        $this->assertArrayHasKey('additionalChoiceInfo', $intentionResponse);
    }
}
