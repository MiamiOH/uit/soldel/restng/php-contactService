<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ContactRESTPostTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $contactREST;

    private $api;
    private $contact;
    private $bannerId;

    private $contactData = [];
    private $mockModel = [];
    private $createModel = [];
    private $createType = '';

    protected function setUp():void
    {

        $this->contactData = [];
        $this->mockModel = [];
        $this->createModel = [];
        $this->createType = '';

        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($this->bannerId);

        $this->contact = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Contact')
            ->setMethods(array('create'))
            ->getMock();

        $this->contact->method('create')
            ->with($this->callback(array($this, 'createWithData')),
                $this->callback(array($this, 'createWithType')))
            ->will($this->returnCallback(array($this, 'createMockModel')));

        $this->contactREST = new \MiamiOH\RestngContactService\Services\ContactREST();

        $this->contactREST->setApp($this->api);
        $this->contactREST->setContact($this->contact);
        $this->contactREST->setBannerUtil($bannerUtil);
        $this->contactREST->setLogger();
    }

    public function testAddContact()
    {
        $this->mockModel = [
            [
                'relationCode' => 'F',
                'pidm' => '567890',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'areaCode' => '513',
                'phoneNumber' => '5551111',
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('jonesl');

        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->createContactCollection();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals($payload[0]['status'], App::API_CREATED,
            'Response has code App::API_CREATED');

        $this->assertEquals('emergency', $this->createType);
    }

    public function testAddContactByPIDM()
    {
        $this->mockModel = [
            [

                'relationCode' => 'F',
                'pidm' => '567890',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'phoneNumber' => '+15135551111',
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('567890');


        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->createContactCollection();


        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertEquals($payload[0]['status'], App::API_CREATED,
            'Response has code App::API_CREATED');

        $this->assertEquals('emergency', $this->createType);

    }

    public function testAddContactByRelationCodeEmergency()
    {
        $this->mockModel = [
            [
                'relationCode' => 'F',
                'pidm' => '567890',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'phoneNumber' => '+15135551111',
            ]

        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('jonesl');

        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->createContactCollection();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        // $this->assertEquals('567890', $payload[0]['contact']['pidm'], 'Response has pidm matching');
        $this->assertEquals(App::API_CREATED, $payload[0]['status'],
            'Response has code App::API_CREATED');

        $this->assertEquals('emergency', $this->createType);

    }

    public function testAddContactByOptionMissingPerson()
    {
        $this->mockModel = [
            [
                'relationCode' => 'K',
                'pidm' => '567890',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'phoneNumber' => '+15135551111',
            ]
        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('jonesl');

        $request->expects($this->once())->method('getData')
            ->willReturn($this->mockModel);

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'missingPerson']);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->createContactCollection();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        // $this->assertEquals('567890', $payload[0]['contact']['pidm'], 'Response has pidm matching');
        $this->assertEquals(App::API_CREATED, $payload[0]['status'],
            'Response has code App::API_CREATED');

        $this->assertEquals('missingPerson', $this->createType);

    }

    public function testAddContactBadRequest()
    {
        $this->contactData = [
            [
                'relationCode' => 'F',
                'pidm' => '234556',
                'priority' => '1',
                'firstName' => 'John',
                'lastName' => 'Doe',
                'middleName' => 'M',
                'phoneNumber' => '+15135551111',
            ],
        ];

        $this->bannerId->method('getPidm')->willReturn('567890');

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('jonesl');

        $request->expects($this->once())->method('getData')
            ->willReturn($this->contactData);

        $this->contactREST->setRequest($request);

        $resp = $this->contactREST->createContactCollection();
        $payload = $resp->getPayload();

        $this->assertEquals(App::API_BADREQUEST,
            $payload[0]['status']);

    }

    public function createWithData($subject)
    {
        $this->createModel = $subject;

        return true;
    }

    public function createWithType($subject)
    {
        $this->createType = $subject;

        return true;
    }

    public function createMockModel()
    {
        return $this->mockModel;
    }
}
