<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class RelationRESTTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $relationREST;

    private $api;
    private $relation;

    private $relationData = [];

    private $filterType = '';
    private $formatType = '';

    protected function setUp():void
    {

        $this->relationData = [];
        $this->filterType = '';
        $this->formatType = '';

        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->relation = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Relation')
            ->setMethods(array('getList', 'filterType', 'formatType'))
            ->getMock();

        $this->relation->method('getList')
            ->will($this->returnCallback(array($this, 'getListMock')));

        $this->relation->method('filterType')
            ->with($this->callback(array($this, 'filterTypeWith')))
            ->will($this->returnSelf());

        $this->relation->method('formatType')
            ->with($this->callback(array($this, 'formatTypeWith')))
            ->will($this->returnSelf());

        $this->relationREST = new \MiamiOH\RestngContactService\Services\RelationREST();

        $this->relationREST->setApp($this->api);
        $this->relationREST->setRelation($this->relation);
        $this->relationREST->setLogger();
    }

    public function testGetRelationList()
    {

        $this->relationData = [
            [
                'relationCode' => 'fa',
                'description' => 'father'
            ],
            [
                'relationCode' => 'mo',
                'description' => 'mother'
            ],
            [
                'relationCode' => 'gu',
                'description' => 'guardian'
            ],
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

//        $request->method('getOptions')
//            ->willReturn([ 'type' => 'ec' ]);

        $this->relationREST->setRequest($request);

        $resp = $this->relationREST->getRelationList();

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(3, count($payload));

        $this->assertTrue(array_key_exists('relationCode', $payload[0]));
        $this->assertEquals('father', $payload[0]['description']);

    }

    public function testGetRelationListType()
    {

        $this->relationData = [
            [
                'relationCode' => 'fa',
                'description' => 'father'
            ],
            [
                'relationCode' => 'mo',
                'description' => 'mother'
            ],
            [
                'relationCode' => 'gu',
                'description' => 'guardian'
            ],
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['relationType' => 'ec']);

        $this->relationREST->setRequest($request);

        $resp = $this->relationREST->getRelationList();

        $payload = $resp->getPayload();

        $this->assertEquals('ec', $this->filterType);

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(3, count($payload));

        $this->assertTrue(array_key_exists('relationCode', $payload[0]));
        $this->assertEquals('father', $payload[0]['description']);

    }

    public function getListMock()
    {
        return $this->relationData;
    }

    public function filterTypeWith($subject)
    {
        $this->filterType = $subject;

        return true;
    }

    public function formatTypeWith($subject)
    {
        $this->formatType = $subject;

        return true;
    }

}
