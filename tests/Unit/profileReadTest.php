<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\Util\Configuration;
use PHPUnit\Framework\MockObject\MockObject;

class ProfileReadTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $profile;

    private $dbh;
    private $db;
    private $sth;
    private $ldap;
    private $ldapFactory;
    private $entry;
    private $term;
    private $student;
    private $employee;
    /** @var Configuration|MockObject  */
    private $configuration;

    private $record = [];
    private $queryString = '';
    private $preapreQueryString = '';
    private $queryParams = [];
    private $bindPlaceHolder = '';
    private $boundValues = [];
    private $response = 0;

    protected function setUp():void
    {
        $this->record = [];
        $this->queryString = '';
        $this->preapreQueryString = '';
        $this->queryParams = [];
        $this->bindPlaceHolder = '';
        $this->boundValues = [];
        $this->response = 0;

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstrow_assoc'))
            ->getMock();

        $this->ldap = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\LDAP')
            ->setMethods(array('next_entry', 'search'))
            ->getMock();

        $this->ldapFactory = $this->getMockBuilder('\MiamiOH\RESTng\Connector\LDAPFactory')
            ->setMethods(array('getHandle'))
            ->getMock();

        $this->ldapFactory->method('getHandle')->willReturn($this->ldap);

        $this->entry = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\LDAP\Entry')
            ->disableOriginalConstructor()
            ->setMethods(array('attribute_value','attribute_values'))
            ->getMock();

        $this->db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);


        $this->db->method('getHandle')->willReturn($this->dbh);

        $this->term = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Term')
            ->setMethods(array('getCurrentReviewTermForStudents', 'getCurrentReviewTermForEmployees'))
            ->getMock();

        $this->student = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Student')
            ->setMethods(array('getStudentData'))
            ->getMock();

        $this->employee = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Employee')
            ->setMethods(array('isEmployee'))
            ->getMock();

        $this->configuration = $this->createMock(Configuration::class);
        $this->configuration->method('getConfiguration')->willReturn([
            'daysBeforeTermStartRequested' => 7,
            'daysBeforeTermStartRequestedEmployees' => 14,
        ]);

        $this->profile = new \MiamiOH\RestngContactService\Services\Profile();

        $this->profile->setDatabase($this->db);
        $this->profile->setTerm($this->term);
        $this->profile->setStudent($this->student);
        $this->profile->setEmployee($this->employee);
        $this->profile->setLdapFactory($this->ldapFactory);
        $this->profile->setLogger();
        $this->profile->setConfiguration($this->configuration);
    }

    public function testProfileRead()
    {
        $this->employee
            ->method('isEmployee')
            ->willReturn(false);

        $this->makeProfile();

        $pidm = '110518';
        $uid = 'DOEJ';

        $this->record = [
            'szbuniq_pidm' => '110518',
            'szbuniq_banner_id' => '+00110518',
            'szbuniq_unique_id' => 'DOEJ',
            'spriden_first_name' => 'John',
            'spriden_last_name' => 'Doe',
            'review_status' => 'requested',
            'add_date' => '2015-08-01',
            'review_date' => null,
            'review_type' => 'student',
            'excluded_actions' => '',
            'sgrsatt_atts_code' => 'COMM',
        ];

        $studentData = [
            'pidm' => '110518',
            'offCampusStudent' => true,
            'international' => false,
            'studentLevelCode' => 'UG',
            'studentTypeCode' => 'N',
            'studentCampusCode' => 'O',
        ];

        $this->response = 1;

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $this->student->expects($this->once())->method('getStudentData')
            ->willReturn($studentData);

        $this->ldap->expects($this->once())->method('search')
            ->willReturn(1);

        $this->ldap->expects($this->once())->method('next_entry')
            ->willReturn($this->entry);

        $this->entry->expects($this->at(0))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliationcode')
            ->willReturn('und');

        $this->entry->expects($this->at(1))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliation')
            ->willReturn('student');

        $this->entry->expects($this->at(2))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocationCode')
            ->willReturn('oxf');

        $this->entry->expects($this->at(3))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocation')
            ->willReturn('oxford');

        $this->entry->expects($this->at(4))
            ->method('attribute_value')
            ->with('givenName')
            ->willReturn('John');

        $this->entry
            ->method('attribute_values')
            ->with('muohioeduCurrentCourseCRN')
            ->willReturn([]);

        $this->term->expects($this->once())->method('getCurrentReviewTermForStudents')
            ->willReturn(['termCode' => '201710', 'description' => 'Fall 2016', 'termStartDate' => '2016-08-22']);

        $model = $this->profile->read($pidm, $uid);

        $this->assertTrue(in_array($pidm, $this->queryParams));

        $this->assertEquals('110518', $model['pidm']);
        $this->assertEquals('+00110518', $model['bannerId']);
        $this->assertEquals('doej', $model['uniqueId']);
        $this->assertEquals('John', $model['firstName']);
        $this->assertEquals('Doe', $model['lastName']);
        $this->assertEquals('2015-08-01', $model['addDate']);
        $this->assertEquals('', $model['reviewDate']);
        $this->assertEquals('requested', $model['reviewStatus']);
        $this->assertEquals('student', $model['reviewType']);
        $this->assertEquals([], $model['excludedActions']);
        $this->assertEquals('201710', $model['reviewTermCode']);
        $this->assertEquals('Fall 2016', $model['reviewTermDescription']);
        //$this->assertTrue($model['hasResidenceHallAssignment']);
        $this->assertEquals('und', $model['primaryAffiliationCode']);
        $this->assertEquals('student', $model['primaryAffiliationDescription']);
        $this->assertEquals('student', $model['primaryAffiliationType']);
        $this->assertEquals(true, $model['offCampusStudent']);
        $this->assertEquals('oxf', $model['campusCode']);
        $this->assertEquals(true, $model['commuter']);
        $this->assertEquals(false, $model['international']);
    }

    public function testProfileReadForRegionalStudent()
    {
        $this->employee
            ->method('isEmployee')
            ->willReturn(false);

        $this->makeProfile();

        $pidm = '110518';
        $uid = 'DOEJ';

        $this->record = [
            'szbuniq_pidm' => '110518',
            'szbuniq_banner_id' => '+00110518',
            'szbuniq_unique_id' => 'DOEJ',
            'spriden_first_name' => 'John',
            'spriden_last_name' => 'Doe',
            'review_status' => 'requested',
            'add_date' => '2015-08-01',
            'review_date' => null,
            'review_type' => 'student',
            'sgbstdn_camp_code' => 'H',
            'sgrsatt_atts_code' => 'SYES'
        ];

        $studentData = [
            'pidm' => '110518',
            'offCampusStudent' => true,
            'international' => false,
            'studentLevelCode' => 'UG',
            'studentTypeCode' => 'N',
            'studentCampusCode' => 'O',
        ];

        $this->response = 1;

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $this->student->expects($this->once())->method('getStudentData')
            ->willReturn($studentData);

        $this->ldap->expects($this->once())->method('search')
            ->willReturn(1);

        $this->ldap->expects($this->once())->method('next_entry')
            ->willReturn($this->entry);

        $this->entry->expects($this->at(0))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliationcode')
            ->willReturn('und');

        $this->entry->expects($this->at(1))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliation')
            ->willReturn('student');

        $this->entry->expects($this->at(2))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocationCode')
            ->willReturn('ham');

        $this->entry->expects($this->at(3))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocation')
            ->willReturn('hamilton');

        $this->entry->expects($this->at(4))
            ->method('attribute_value')
            ->with('givenName')
            ->willReturn('Johny');

        $this->term->expects($this->once())->method('getCurrentReviewTermForStudents')
            ->willReturn(['termCode' => '201710', 'description' => 'Fall 2016', 'termStartDate' => '2016-08-22']);

        $model = $this->profile->read($pidm, $uid);

        $this->assertTrue(in_array($pidm, $this->queryParams));

        $this->assertEquals('110518', $model['pidm']);
        $this->assertEquals('+00110518', $model['bannerId']);
        $this->assertEquals('doej', $model['uniqueId']);
        $this->assertEquals('Johny', $model['firstName']);
        $this->assertEquals('Doe', $model['lastName']);
        $this->assertEquals('2015-08-01', $model['addDate']);
        $this->assertEquals('', $model['reviewDate']);
        $this->assertEquals('requested', $model['reviewStatus']);
        $this->assertEquals('student', $model['reviewType']);
        $this->assertEquals('201710', $model['reviewTermCode']);
        $this->assertEquals('Fall 2016', $model['reviewTermDescription']);
        //$this->assertTrue($model['hasResidenceHallAssignment']);
        $this->assertEquals('und', $model['primaryAffiliationCode']);
        $this->assertEquals('student', $model['primaryAffiliationDescription']);
        $this->assertEquals('student', $model['primaryAffiliationType']);
        $this->assertEquals(true, $model['offCampusStudent']);
        $this->assertEquals('ham', $model['campusCode']);
        $this->assertEquals(false, $model['commuter']);
    }

    public function testProfileReadForOtherCampusStudent()
    {
        $this->employee
            ->method('isEmployee')
            ->willReturn(false);

        $this->makeProfile();

        $pidm = '110518';
        $uid = 'DOEJ';

        $this->record = [
            'szbuniq_pidm' => '110518',
            'szbuniq_banner_id' => '+00110518',
            'szbuniq_unique_id' => 'DOEJ',
            'spriden_first_name' => 'John',
            'spriden_last_name' => 'Doe',
            'review_status' => 'requested',
            'add_date' => '2015-08-01',
            'review_date' => null,
            'review_type' => 'student',
            'sgrsatt_atts_code' => 'SYES'
        ];

        $studentData = [
            'pidm' => '110518',
            'offCampusStudent' => true,
            'international' => false,
            'studentLevelCode' => 'UG',
            'studentTypeCode' => 'N',
            'studentCampusCode' => 'O',
        ];

        $this->response = 1;

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $this->student->expects($this->once())->method('getStudentData')
            ->willReturn($studentData);

        $this->ldap->expects($this->once())->method('search')
            ->willReturn(1);

        $this->ldap->expects($this->once())->method('next_entry')
            ->willReturn($this->entry);

        $this->entry->expects($this->at(0))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliationcode')
            ->willReturn('und');

        $this->entry->expects($this->at(1))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliation')
            ->willReturn('student');

        $this->entry->expects($this->at(2))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocationCode')
            ->willReturn('lex');

        $this->entry->expects($this->at(3))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocation')
            ->willReturn('lexington');

        $this->entry->expects($this->at(4))
            ->method('attribute_value')
            ->with('givenName')
            ->willReturn('John');

        $this->term->expects($this->once())->method('getCurrentReviewTermForStudents')
            ->willReturn(['termCode' => '201710', 'description' => 'Fall 2016', 'termStartDate' => '2016-08-22']);

        $model = $this->profile->read($pidm, $uid);

        $this->assertTrue(in_array($pidm, $this->queryParams));

        $this->assertEquals('110518', $model['pidm']);
        $this->assertEquals('+00110518', $model['bannerId']);
        $this->assertEquals('doej', $model['uniqueId']);
        $this->assertEquals('John', $model['firstName']);
        $this->assertEquals('Doe', $model['lastName']);
        $this->assertEquals('2015-08-01', $model['addDate']);
        $this->assertEquals('', $model['reviewDate']);
        $this->assertEquals('requested', $model['reviewStatus']);
        $this->assertEquals('student', $model['reviewType']);
        $this->assertEquals('201710', $model['reviewTermCode']);
        $this->assertEquals('Fall 2016', $model['reviewTermDescription']);
        //$this->assertTrue($model['hasResidenceHallAssignment']);
        $this->assertEquals('und', $model['primaryAffiliationCode']);
        $this->assertEquals('student', $model['primaryAffiliationDescription']);
        $this->assertEquals('student', $model['primaryAffiliationType']);
        $this->assertEquals(true, $model['offCampusStudent']);
        $this->assertEquals('lex', $model['campusCode']);
        $this->assertEquals(false, $model['commuter']);
    }

    public function testProfileReadForEmployee()
    {
        $this->employee
            ->method('isEmployee')
            ->willReturn(true);

        $this->makeProfile();

        $pidm = '110518';
        $uid = 'DOEJ';

        $this->record = [
            'szbuniq_pidm' => '110518',
            'szbuniq_banner_id' => '+00110518',
            'szbuniq_unique_id' => 'DOEJ',
            'spriden_first_name' => 'John',
            'spriden_last_name' => 'Doe',
            'review_status' => 'requested',
            'add_date' => '2015-08-01',
            'review_date' => null,
            'review_type' => 'employee',
            'sgrsatt_atts_code' => null
        ];

        $this->response = 1;

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $this->ldap->expects($this->once())->method('search')
            ->willReturn(1);

        $this->ldap->expects($this->once())->method('next_entry')
            ->willReturn($this->entry);

        $this->entry->expects($this->at(0))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliationcode')
            ->willReturn('sta');

        $this->entry->expects($this->at(1))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliation')
            ->willReturn('employee');

        $this->entry->expects($this->at(2))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocationCode')
            ->willReturn('oxf');

        $this->entry->expects($this->at(3))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocation')
            ->willReturn('oxford');

        $this->entry->expects($this->at(4))
            ->method('attribute_value')
            ->with('givenName')
            ->willReturn('John');

        $this->term->expects($this->once())->method('getCurrentReviewTermForEmployees')
            ->willReturn(['termCode' => '201710', 'description' => 'Fall 2016', 'termStartDate' => '2016-08-22']);

        $model = $this->profile->read($pidm, $uid);

        $this->assertTrue(in_array($pidm, $this->queryParams));

        $this->assertEquals('110518', $model['pidm']);
        $this->assertEquals('+00110518', $model['bannerId']);
        $this->assertEquals('doej', $model['uniqueId']);
        $this->assertEquals('John', $model['firstName']);
        $this->assertEquals('Doe', $model['lastName']);
        $this->assertEquals('2015-08-01', $model['addDate']);
        $this->assertEquals('', $model['reviewDate']);
        $this->assertEquals('requested', $model['reviewStatus']);
        $this->assertEquals('employee', $model['reviewType']);
        $this->assertEquals('201710', $model['reviewTermCode']);
        $this->assertEquals('Fall 2016', $model['reviewTermDescription']);
        //$this->assertTrue($model['hasResidenceHallAssignment']);
        $this->assertEquals('sta', $model['primaryAffiliationCode']);
        $this->assertEquals('employee', $model['primaryAffiliationDescription']);
        $this->assertEquals('employee', $model['primaryAffiliationType']);
        $this->assertEquals('oxf', $model['campusCode']);
    }

    public function testProfileReadForInternationalStudent()
    {
        $this->makeProfile();

        $pidm = '110518';
        $uid = 'DOEJ';

        $this->record = [
            'szbuniq_pidm' => '110518',
            'szbuniq_banner_id' => '+00110518',
            'szbuniq_unique_id' => 'DOEJ',
            'spriden_first_name' => 'John',
            'spriden_last_name' => 'Doe',
            'review_status' => 'requested',
            'add_date' => '2015-08-01',
            'review_date' => null,
            'review_type' => 'student',
            'sgbstdn_camp_code' => 'H',
            'sgrsatt_atts_code' => 'SYES'
        ];

        $studentData = [
            'pidm' => '110518',
            'offCampusStudent' => true,
            'international' => true,
            'studentLevelCode' => 'UG',
            'studentTypeCode' => 'N',
            'studentCampusCode' => 'O',
        ];

        $this->response = 1;

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $this->student->expects($this->once())->method('getStudentData')
            ->willReturn($studentData);

        $this->ldap->expects($this->once())->method('search')
            ->willReturn(1);

        $this->ldap->expects($this->once())->method('next_entry')
            ->willReturn($this->entry);

        $this->entry->expects($this->at(0))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliationcode')
            ->willReturn('und');

        $this->entry->expects($this->at(1))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliation')
            ->willReturn('student');

        $this->entry->expects($this->at(2))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocationCode')
            ->willReturn('ham');

        $this->entry->expects($this->at(3))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocation')
            ->willReturn('hamilton');

        $this->entry->expects($this->at(4))
            ->method('attribute_value')
            ->with('givenName')
            ->willReturn('Johny');

        $this->term->expects($this->once())->method('getCurrentReviewTermForStudents')
            ->willReturn(['termCode' => '201710', 'description' => 'Fall 2016', 'termStartDate' => '2016-08-22']);

        $model = $this->profile->read($pidm, $uid);

        $this->assertTrue(in_array($pidm, $this->queryParams));

        $this->assertEquals('110518', $model['pidm']);
        $this->assertEquals('+00110518', $model['bannerId']);
        $this->assertEquals('doej', $model['uniqueId']);
        $this->assertEquals('Johny', $model['firstName']);
        $this->assertEquals('Doe', $model['lastName']);
        $this->assertEquals('2015-08-01', $model['addDate']);
        $this->assertEquals('', $model['reviewDate']);
        $this->assertEquals('requested', $model['reviewStatus']);
        $this->assertEquals('student', $model['reviewType']);
        $this->assertEquals('201710', $model['reviewTermCode']);
        $this->assertEquals('Fall 2016', $model['reviewTermDescription']);
        //$this->assertTrue($model['hasResidenceHallAssignment']);
        $this->assertEquals('und', $model['primaryAffiliationCode']);
        $this->assertEquals('student', $model['primaryAffiliationDescription']);
        $this->assertEquals('student', $model['primaryAffiliationType']);
        $this->assertEquals(true, $model['offCampusStudent']);
        $this->assertEquals('ham', $model['campusCode']);
        $this->assertEquals(false, $model['commuter']);
        $this->assertEquals(true, $model['international']);
        $this->assertEquals('UG', $model['studentLevelCode']);
        $this->assertEquals('N', $model['studentTypeCode']);
        $this->assertEquals('O', $model['studentCampusCode']);
    }

    /**
     * @param array $excludedActions
     * @param array $expectedRequiredActions
     *
     * @dataProvider profileReviewActions
     */
    public function testProfileWithReviewActions(string $pledgeEnabled, array $excludedActions, array $expectedRequiredActions)
    {
        $this->makeProfile(['pledgeMenu.showMenuItem' => $pledgeEnabled]);

        $pidm = '110518';
        $uid = 'DOEJ';

        $this->record = [
            'szbuniq_pidm' => '110518',
            'szbuniq_banner_id' => '+00110518',
            'szbuniq_unique_id' => 'DOEJ',
            'spriden_first_name' => 'John',
            'spriden_last_name' => 'Doe',
            'review_status' => 'requested',
            'add_date' => '2015-08-01',
            'review_date' => null,
            'review_type' => 'student',
            'excluded_actions' => implode(',', $excludedActions),
            'sgrsatt_atts_code' => 'COMM',
        ];

        $studentData = [
            'pidm' => '110518',
            'offCampusStudent' => true,
            'international' => false,
            'studentLevelCode' => 'UG',
            'studentTypeCode' => 'N',
            'studentCampusCode' => 'O',
        ];

        $this->response = 1;

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $this->student->expects($this->once())->method('getStudentData')
            ->willReturn($studentData);

        $this->ldap->expects($this->once())->method('search')
            ->willReturn(1);

        $this->ldap->expects($this->once())->method('next_entry')
            ->willReturn($this->entry);

        $this->entry->expects($this->at(0))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliationcode')
            ->willReturn('und');

        $this->entry->expects($this->at(1))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliation')
            ->willReturn('student');

        $this->entry->expects($this->at(2))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocationCode')
            ->willReturn('oxf');

        $this->entry->expects($this->at(3))
            ->method('attribute_value')
            ->with('muohioeduPrimaryLocation')
            ->willReturn('oxford');

        $this->entry->expects($this->at(4))
            ->method('attribute_value')
            ->with('givenName')
            ->willReturn('John');

        $this->entry
            ->method('attribute_values')
            ->with('muohioeduCurrentCourseCRN')
            ->willReturn([]);

        $model = $this->profile->read($pidm, $uid);

        $this->assertEquals($expectedRequiredActions, $model['requiredActions']);
        $this->assertEquals($excludedActions, $model['excludedActions']);
    }

    public function profileReviewActions(): array
    {
        return [
            'all required' => [
                'true',
                [],
                ['emergencyContacts', 'phoneNumber', 'missingPersonContacts', 'schoolAddress', 'pledgeInfo'],
            ],
            'all required, pledge disabled' => [
                'false',
                [],
                ['emergencyContacts', 'phoneNumber', 'missingPersonContacts', 'schoolAddress'],
            ],
            'one excluded' => [
                'true',
                ['missingPersonContacts'],
                ['emergencyContacts', 'phoneNumber', 'missingPersonContacts', 'schoolAddress', 'pledgeInfo'],
            ],
        ];
    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryfirstrow_assocWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryfirstrow_assocMock()
    {
        return $this->record;
    }

    public function bind_by_nameWithName($subject)
    {
        $this->bindPlaceHolder = $subject;

        return true;
    }

    public function bind_by_nameWithValue($subject)
    {
        $this->boundValues[$this->bindPlaceHolder] = $subject;
        $this->bindPlaceHolder = '';

        return true;
    }

    public function prepareWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function executeArrayMock()
    {
        return $this->response;
    }

    protected function makeProfile(array $overrides = []): void
    {
        $config = array_merge([
            'daysBeforeTermStartRequested' => 7,
            'pledgeMenu.showMenuItem' => 'true',
            'daysBeforeTermStartRequestedEmployees' => 14,
        ], $overrides);

        $this->configuration = $this->createMock(Configuration::class);
        $this->configuration->method('getConfiguration')->willReturn($config);

        $this->profile = new \MiamiOH\RestngContactService\Services\Profile();

        $this->profile->setDatabase($this->db);
        $this->profile->setTerm($this->term);
        $this->profile->setStudent($this->student);
        $this->profile->setEmployee($this->employee);
        $this->profile->setLdapFactory($this->ldapFactory);
        $this->profile->setLogger();
        $this->profile->setConfiguration($this->configuration);
    }
}
