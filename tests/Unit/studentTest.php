<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\Legacy\DB\DBH;

class StudentTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $student;

    private $dbh;

    private $record = [];
    private $queryString = '';
    private $queryParams = [];

    protected function setUp():void
    {

        $this->record = [];
        $this->queryString = '';
        $this->queryParams = [];

        $this->dbh = $this->createMock(DBH::class);

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);


        $db->method('getHandle')->willReturn($this->dbh);

        $this->student = new \MiamiOH\RestngContactService\Services\Student();

        $this->student->setDatabase($db);
        $this->student->setLogger();
    }

    public function testGetStudentData()
    {
        $pidm = '110518';

        $this->record = [
            'szbuniq_pidm' => '110518',
            'off_campus_student' => 'Y',
            'international' => 'N',
            'student_level_code' => 'UG',
            'student_type_code' => 'N',
            'student_campus_code' => 'O'
        ];

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->with($this->callback(array($this, 'queryfirstrow_assocWithQuery')),
                $this->callback(array($this, 'queryfirstrow_assocWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstrow_assocMock')));

        $this->dbh->expects($this->once())->method('queryall_array')
            ->willReturn(
                [ ['crn' => '12344', 'course_name' => 'MTH151', 'online_only' => 1] ]
            );

        $this->student->setTermCode('201710');

        $model = $this->student->getStudentData($pidm);

        $this->assertEquals($pidm, $this->queryParams);
        $this->assertTrue(stripos($this->queryString,
                'baninst1.fz_oxfd_offcampus') !== false,
            'Query contains baninst1.fz_oxfd_offcampus');

        $this->assertEquals('110518', $model['pidm']);
        $this->assertEquals(true, $model['offCampusStudent']);
        $this->assertEquals(false, $model['international']);
        $this->assertEquals('UG', $model['studentLevelCode']);
        $this->assertEquals('N', $model['studentTypeCode']);
        $this->assertEquals('O', $model['studentCampusCode']);
        $this->assertEquals([ ['CRN' => '12344', 'courseName' => 'MTH151', 'onlineOnly' => 1] ], $model['classAttendance']);
    }

    public function testGetStudentsForReview()
    {
        $this->record = [
            [
                'sgbstdn_pidm' => '110518',
                'szbuniq_unique_id' => 'DOEJ',
                'review_status' => 'requested',
            ],
        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->setReviewState('requested');
        $this->student->setTermCode('201710');

        $records = $this->student->getStudentsForReview();

        $this->assertTrue(is_array($records));

        $this->assertTrue(stripos($this->queryString,
                "'requested' as review_status") !== false,
            'Query includes correct review state');
//        $this->assertTrue(stripos($this->queryString,
//                "sgbstdn_term_code_eff = '201710'") !== false,
//            'Query includes correct term code');

        $this->assertEquals('110518', $records[0]['pidm']);
        $this->assertEquals('doej', $records[0]['uniqueId']);
        $this->assertEquals('requested', $records[0]['reviewStatus']);
    }

    public function testGetStudentsForReviewNoActiveTerm()
    {

        $this->dbh->expects($this->never())->method('queryall_array');

        $this->student->setReviewState('');
        $this->student->setTermCode('');

        $records = $this->student->getStudentsForReview();

        $this->assertTrue(is_array($records));

    }

    public function queryfirstrow_assocWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryfirstrow_assocWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryfirstrow_assocMock()
    {
        return $this->record;
    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->record;
    }
}
