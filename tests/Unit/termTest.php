<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

class TermTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $term;

    private $dbh;
    private $configuration;

    private $mockConfigurationItems = [];

    protected function setUp():void
    {

        $this->mockConfigurationItems = [];

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryfirstrow_assoc'))
            ->getMock();

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);


        $db->method('getHandle')->willReturn($this->dbh);

        $this->configuration = $this->getMockBuilder('\MiamiOH\RESTng\Util\Configuration')
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $this->configuration->method('getConfiguration')
            ->with($this->callback(array($this, 'getConfigurationWithApplication')),
                $this->callback(array($this, 'getConfigurationWithCategory')))
            ->will($this->returnCallback(array($this, 'getConfigurationMock')));

        $this->term = new \MiamiOH\RestngContactService\Services\Term();

        $this->term->setDatabase($db);
        $this->term->setConfiguration($this->configuration);

    }

    public function testGetCurrentReviewTermForStudents()
    {

        $this->mockConfigurationItems = [
            'daysBeforeTermStartRequested' => '7',
            'daysAfterTermStartRequired' => '14',
            'daysBeforeTermEndToEndReview' => '7'
        ];

        $expectedTerm = [
            'stvterm_code' => '201710',
            'stvterm_desc' => 'Fall 2016',
            'stvterm_start_date' => '20160824',
        ];

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->willReturn($expectedTerm);

        $term = $this->term->getCurrentReviewTermForStudents();

        $this->assertEquals($expectedTerm['stvterm_code'], $term['termCode']);
        $this->assertEquals($expectedTerm['stvterm_start_date'],
            $term['termStartDate']);
    }

    public function testGetCurrentReviewTermForEmployees()
    {

        $this->mockConfigurationItems = [
            'daysBeforeTermStartRequestedEmployees' => '7',
            'daysAfterTermStartRequiredEmployees' => '14',
        ];

        $expectedTerm = [
            'stvterm_code' => '201710',
            'stvterm_desc' => 'Fall 2016',
            'stvterm_start_date' => '20160824',
        ];

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->willReturn($expectedTerm);

        $term = $this->term->getCurrentReviewTermForEmployees();

        $this->assertEquals($expectedTerm['stvterm_code'], $term['termCode']);
        $this->assertEquals($expectedTerm['stvterm_start_date'],
            $term['termStartDate']);
    }

    public function testGetCurrentReviewTermNoTerm()
    {

        $this->mockConfigurationItems = [
            'daysBeforeTermStartRequested' => '7',
            'daysAfterTermStartRequired' => '14',
        ];

        $expectedTerm = \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET;

        $this->dbh->expects($this->once())->method('queryfirstrow_assoc')
            ->willReturn($expectedTerm);

        $term = $this->term->getCurrentReviewTermForStudents();

        $this->assertEquals('', $term['termCode']);
        $this->assertEquals('', $term['termStartDate']);
    }

    public function getConfigurationWithApplication($subject)
    {
        $this->calledApplicationName = $subject;

        return true;
    }

    public function getConfigurationWithCategory($subject)
    {
        $this->calledCategoryName = $subject;

        return true;
    }

    public function getConfigurationMock()
    {
        return $this->mockConfigurationItems;
    }

}
