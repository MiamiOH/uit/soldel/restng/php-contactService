<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

class ProfileReadListTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $profile;

    private $dbh;
    private $sth;
    private $term;
    private $student;

    private $record = [];
    private $queryString = '';
    private $preapreQueryString = '';
    private $queryParams = [];
    private $bindPlaceHolder = '';
    private $boundValues = [];
    private $response = 0;

    protected function setUp():void
    {

        $this->record = [];
        $this->queryString = '';
        $this->queryParams = [];
        $this->bindPlaceHolder = '';
        $this->boundValues = [];
        $this->response = 0;
        $this->preapreQueryString = '';

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->ldap = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\LDAP')
            ->setMethods(array('next_entry', 'search'))
            ->getMock();

        $ldapFactory = $this->getMockBuilder('\MiamiOH\RESTng\Connector\LDAPFactory')
            ->setMethods(array('getHandle'))
            ->getMock();

        $ldapFactory->method('getHandle')->willReturn($this->ldap);

        $this->entry = $this->getMockBuilder('\MiamiOH\RESTng\Legacy\LDAP\Entry')
            ->disableOriginalConstructor()
            ->setMethods(array('attribute_value','attribute_values'))
            ->getMock();

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);

        $db->method('getHandle')->willReturn($this->dbh);

        $this->term = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Term')
            ->setMethods(array('getCurrentReviewTermForStudents', 'getCurrentReviewTermForEmployees'))
            ->getMock();

        $this->student = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Student')
            ->setMethods(array('getStudentData'))
            ->getMock();

        $this->profile = new \MiamiOH\RestngContactService\Services\Profile();

        $this->profile->setDatabase($db);
        $this->profile->setTerm($this->term);
        $this->profile->setStudent($this->student);
        $this->profile->setLdapFactory($ldapFactory);
        $this->profile->setLogger();
    }

    public function testProfileReadListStudent()
    {
        $pidms = ['110518', '123123'];

        $this->record = [
            [
                'szbuniq_pidm' => '110518',
                'szbuniq_banner_id' => '+00110518',
                'szbuniq_unique_id' => 'DOEJ',
                'spriden_first_name' => 'John',
                'spriden_last_name' => 'Doe',
                'review_status' => 'P',
                'add_date' => '2015-08-01',
                'review_date' => null,
                'review_type' => 'student',
                'sgrsatt_atts_code' => 'SEP'
            ],
            [
                'szbuniq_pidm' => '123123',
                'szbuniq_banner_id' => '+00123123',
                'szbuniq_unique_id' => 'SMITHT',
                'spriden_first_name' => 'John',
                'spriden_last_name' => 'Doe',
                'review_status' => 'P',
                'add_date' => '2015-08-01',
                'review_date' => null,
                'review_type' => 'student',
                'sgrsatt_atts_code' => 'SYES'
            ],
        ];

        $studentData = [
            [
                'pidm' => '110518',
                'offCampusStudent' => true,
                'international' => false,
                'studentLevelCode' => 'UG',
                'studentTypeCode' => 'N',
                'studentCampusCode' => 'O',
            ],
            [
                'pidm' => '123123',
                'offCampusStudent' => true,
                'international' => false,
                'studentLevelCode' => 'UG',
                'studentTypeCode' => 'N',
                'studentCampusCode' => 'O',
            ],
        ];

        $this->response = 1;

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->student->expects($this->exactly(2))->method('getStudentData')
            ->will($this->onConsecutiveCalls($studentData[0], $studentData[1]));

        $this->ldap->method('search')
            ->willReturn(1);

        $this->ldap->method('next_entry')
            ->willReturn($this->entry);

        $this->entry->expects($this->at(0))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliationcode')
            ->willReturn('und');

        $this->entry->expects($this->at(1))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliation')
            ->willReturn('Student');

        $this->entry
            ->method('attribute_values')
            ->with('muohioeduCurrentCourseCRN')
            ->willReturn([]);

        $this->term->expects($this->once())->method('getCurrentReviewTermForStudents')
            ->willReturn(['termCode' => '201710', 'description' => 'Fall 2016']);

        $models = $this->profile->readList($pidms, 'student');

        $this->assertTrue(in_array($pidms[0], $this->queryParams));

    }

    public function testProfileReadListEmployee()
    {
        $pidms = ['110518', '123123'];

        $this->record = [
            [
                'szbuniq_pidm' => '110518',
                'szbuniq_banner_id' => '+00110518',
                'szbuniq_unique_id' => 'DOEJ',
                'spriden_first_name' => 'John',
                'spriden_last_name' => 'Doe',
                'review_status' => 'P',
                'add_date' => '2015-08-01',
                'review_date' => null,
                'review_type' => 'employee',
                'sgrsatt_atts_code' => ''
            ],
            [
                'szbuniq_pidm' => '123123',
                'szbuniq_banner_id' => '+00123123',
                'szbuniq_unique_id' => 'SMITHT',
                'spriden_first_name' => 'John',
                'spriden_last_name' => 'Doe',
                'review_status' => 'P',
                'add_date' => '2015-08-01',
                'review_date' => null,
                'review_type' => 'employee',
                'sgrsatt_atts_code' => ''
            ],
        ];

        $this->response = 1;

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')),
                $this->callback(array($this, 'queryall_arrayWithParams')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));


        $this->ldap->method('search')
            ->willReturn(1);

        $this->ldap->method('next_entry')
            ->willReturn($this->entry);

        $this->entry->expects($this->at(0))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliationcode')
            ->willReturn('sta');

        $this->entry->expects($this->at(1))
            ->method('attribute_value')
            ->with('muohioeduprimaryaffiliation')
            ->willReturn('Employee');

        $this->entry
            ->method('attribute_values')
            ->with('muohioeduCurrentCourseCRN')
            ->willReturn([]);

        $this->term->expects($this->once())->method('getCurrentReviewTermForEmployees')
            ->willReturn(['termCode' => '201710', 'description' => 'Fall 2016']);

        $models = $this->profile->readList($pidms, 'employee');

        $this->assertTrue(in_array($pidms[0], $this->queryParams));

    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryall_arrayWithParams($subject)
    {
        $this->queryParams = $subject;

        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->record;
    }

    public function prepareWithQuery($subject)
    {
        $this->preapreQueryString = $subject;

        return true;
    }

    public function executeArrayMock()
    {
        return $this->response;
    }

    public function bind_by_nameWithName($subject)
    {
        $this->bindPlaceHolder = $subject;

        return true;
    }

    public function bind_by_nameWithValue($subject)
    {
        $this->boundValues[$this->bindPlaceHolder] = $subject;
        $this->bindPlaceHolder = '';

        return true;
    }
}
