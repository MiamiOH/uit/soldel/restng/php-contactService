<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use Exception;

class RelationTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $relation;

    private $dbh;

    private $records = [];
    private $queryString = '';

    protected function setUp():void
    {

        $this->records = [];
        $this->queryString = '';

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $this->dbh->expects($this->once())->method('queryall_array')
            ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
            ->will($this->returnCallback(array($this, 'queryall_arrayMock')));

        $this->dbh->error_string = '';

        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);


        $db->method('getHandle')->willReturn($this->dbh);

        $this->relation = new \MiamiOH\RestngContactService\Services\Relation();

        $this->relation->setDatabase($db);
        $this->relation->setLogger();
    }

    public function testGetRelationData()
    {

        $this->records = [

        ];

        $this->relation->getList();

        $this->assertTrue(strpos($this->queryString, 'WHERE') === false,
            'Query no param does not contain WHERE');
        $this->assertTrue(strpos($this->queryString, 'FROM STVRELT') !== false,
            'Query from stvrelt');

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage query failed
     */
    public function testGetRelationDataException()
    {

        $this->records = [

        ];

        $this->dbh->expects($this->once())->method('queryall_array')
            ->will($this->throwException(new Exception('query failed')));;

        $this->relation->getList();

    }

    public function queryall_arrayWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }

    public function queryall_arrayMock()
    {
        return $this->records;
    }
}
