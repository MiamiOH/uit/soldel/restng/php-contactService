<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

class WASTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $was;

    private $configObj;
    private $http;

    private $configData = [];
    private $httpRequestUrl = '';
    private $httpResponse = '';

    protected function setUp():void
    {

        // Test will use this data unless you call setConfig again
        $this->configData = [
            'wasClientUpdateUrl' => 'http://ws/contactInfo/updateWAS.php?extapp=PersonContactInfo&ss=bob',
            'wasClientRequiredUpdateUrl' => 'http://ws/contactInfo/updateWAS.php?extapp=PersonContactInfo&ss=bob',
            'wasClientUpdateUrlEmployee' => 'http://ws/contactInfo/updateWAS.php?extapp=EmployeeContactInfo&ss=bob',
            'wasClientRequiredUpdateUrlEmployee' => 'http://ws/contactInfo/updateWAS.php?extapp=EmployeeContactInfoRequired&ss=bob',
        ];

        $this->httpRequestUrl = '';
        $this->httpResponse = '';

        $this->configObj = $this->getMockBuilder('\MiamiOH\RESTng\Util\Configuration')
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $this->configObj->method('getConfiguration')
            ->will($this->returnCallback(array($this, 'getConfigMock')));

        $this->http = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\HTTP')
            ->setMethods(array('get'))
            ->getMock();

        $this->http->method('get')
            ->with($this->callback(array($this, 'getHttpWithUrl')))
            ->will($this->returnCallback(array($this, 'getHttpMock')));

        $this->was = new \MiamiOH\RestngContactService\Services\WAS();

        $this->was->setConfiguration($this->configObj);
        $this->was->setHttp($this->http);

    }

    public function testWASConfig()
    {
        $result = $this->was->updateWAS('doej','student');

        $this->assertNull($result, 'WAS update is null with no options');

    }

    public function testWASUpdateReviewRequested()
    {
        $this->httpResponse = 'ok';

        $result = $this->was->updateWAS('doej', 'student', ['reviewRequested' => true]);

        $this->assertEquals('ok', $result, 'WAS update returns expected result');

        $this->assertTrue(strpos($this->httpRequestUrl, 'uid=doej') !== false,
            'WAS update url contains correct uid param');
        $this->assertTrue(strpos($this->httpRequestUrl, 'reviewPending=1') !== false,
            'WAS update url contains correct reviewRequested param');
    }

    public function testWASUpdateComplete()
    {
        $this->httpResponse = 'ok';

        $result = $this->was->updateWAS('doej', 'student', ['complete' => true]);

        $this->assertEquals('ok', $result, 'WAS update returns expected result');

        $this->assertTrue(strpos($this->httpRequestUrl, 'uid=doej') !== false,
            'WAS update url contains correct uid param');
        $this->assertTrue(strpos($this->httpRequestUrl, 'complete=1') !== false,
            'WAS update url contains correct complete param');
    }

    public function getConfigMock()
    {
        return $this->configData;
    }

    public function getHttpWithUrl($subject)
    {
        $this->httpRequestUrl = $subject;

        return true;
    }

    public function getHttpMock()
    {
        return $this->httpResponse;
    }

}

