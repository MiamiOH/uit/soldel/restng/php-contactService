<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ProfileRESTGetListTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $profileREST;

    private $api;
    private $profile;
    private $request;

    private $mockReadResponse = [];

    protected function setUp():void
    {

        $this->mockReadResponse = [];

        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

        $this->profile = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Profile')
            ->setMethods(array('readList'))
            ->getMock();

        $this->profileREST = new \MiamiOH\RestngContactService\Services\ProfileREST();

        $this->profileREST->setApp($this->api);
        $this->profileREST->setProfile($this->profile);
        $this->profileREST->setLogger();
    }

    public function testProfileRESTGetList()
    {

        $this->mockReadResponse = [
            [
                'pidm' => 123456,
            ],
            [
                'pidm' => 654321,
            ],
        ];

        $this->profile->method('readList')
            ->with($this->callback(array($this, 'readListWith')))
            ->will($this->returnCallback(array($this, 'readListMock')));

        $this->request->method('getOptions')
            ->willReturn(['pidm' => [123456]]);

        $this->profileREST->setRequest($this->request);

        $resp = $this->profileREST->getProfileList();

        $payload = $resp->getPayload();

        $this->assertTrue(is_array($payload));

    }

    public function getResourceParamWith($subject)
    {
        $this->requestResourceParam = $subject;

        return true;
    }

    public function getResourceParamMock()
    {
        if (isset($this->requestResourceParamMocks[$this->requestResourceParam])) {
            return $this->requestResourceParamMocks[$this->requestResourceParam];
        }

        return null;
    }


    public function readListWith($subject)
    {
        $this->readPidm = $subject;

        return true;
    }

    public function readListMock()
    {
        return $this->mockReadResponse;
    }

}
