<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

class ContactPutTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $contact;

    private $dbh;
    private $sth;

    private $bindPlaceHolder = '';
    private $boundValues = [];
    private $queryNumber = 0;
    private $queries = [];
    private $queryMockResponses = [];

    protected function setUp():void
    {

        $this->bindPlaceHolder = '';
        $this->boundValues = [];
        $this->queryNumber = 0;
        $this->queries = [];
        $this->queryMockResponses = [];

        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('prepare', 'queryfirstcolumn'))
            ->getMock();

        $this->sth = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\STH')
            ->setMethods(array('execute', 'bind_by_name'))
            ->getMock();

        $this->sth->method('bind_by_name')
            ->with($this->callback(array($this, 'bind_by_nameWithName')),
                $this->callback(array($this, 'bind_by_nameWithValue')))
            ->willReturn(true);


        $this->dbh->expects($this->once())->method('prepare')
            ->with($this->callback(array($this, 'prepareWithQuery')))
            ->willReturn($this->sth);

        $this->dbh->method('queryfirstcolumn')
            ->with($this->callback(array($this, 'queryfirstcolumnWithQuery')),
                $this->callback(array($this, 'queryfirstcolumnWithParams')))
            ->will($this->returnCallback(array($this, 'queryfirstcolumnMock')));


        $db = $this->createMock(\MiamiOH\RESTng\Connector\DatabaseFactory::class);


        $db->method('getHandle')->willReturn($this->dbh);

        $this->contact = new \MiamiOH\RestngContactService\Services\Contact();

        $this->contact->setDatabase($db);
        $this->contact->setLogger();
    }

    public function testUpdateContactData()
    {


        $this->queryMockResponses[] = [
            [
                'stvrelt_code' => 'F',
            ]
        ];

        $this->bindPlaceHolder = '';
        $this->boundValues = array();

        $model = [
            'relation' => 'Father',
            'relationCode' => 'F',
            'pidm' => '567890',
            'priority' => '1',
            'firstName' => 'John',
            'lastName' => 'Doe',
            'middleName' => 'M',
            'areaCode' => '513',
            'phoneNumber' => '5551111',
        ];

        $this->contact->update($model);

        $this->assertTrue(stripos($this->queryString,
                'GB_EMERGENCY_CONTACT.P_UPDATE') !== false,
            'Query includes GB_EMERGENCY_CONTACT.P_UPDATE procedure call');
        $this->assertEquals('567890', $this->boundValues[':P_PIDM'],
            'PIDM is not bound to 567890');
        $this->assertEquals('F', $this->boundValues[':P_RELT_CODE'],
            'Relation is not bound to F');

    }

    public function bind_by_nameWithName($subject)
    {
        $this->bindPlaceHolder = $subject;

        return true;
    }

    public function bind_by_nameWithValue($subject)
    {
        $this->boundValues[$this->bindPlaceHolder] = $subject;
        $this->bindPlaceHolder = '';

        return true;
    }

    public function prepareWithQuery($subject)
    {
        $this->queryString = $subject;

        return true;
    }


    public function queryfirstcolumnWithQuery($subject)
    {
        $this->queries[$this->queryNumber] = [
            'query' => $subject,
            'params' => [],
        ];

        return true;
    }

    public function queryfirstcolumnWithParams($subject)
    {
        $this->queries[$this->queryNumber]['params'] = $subject;

        return true;
    }

    public function queryfirstcolumnMock()
    {
        return $this->queryMockResponses[$this->queryNumber++];
    }


}
