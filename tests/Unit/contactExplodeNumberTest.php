<?php
/*
-----------------------------------------------------------
FILE NAME: phoneGetTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Emily Schmidt

DESCRIPTION:  Unit Tests for Testing the GET Functionality of the Phone Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/12/2015       SCHMIDEE
Description:  Initial Program

 */

namespace MiamiOH\RestngContactService\Tests\Unit;

class contactExplodeNumberTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $contact;

    protected function setUp():void
    {

        $this->contact = new \MiamiOH\RestngContactService\Services\Contact();

    }

    public function testContactExplodeNumberUS()
    {
        $number = '+11234567890';

        $parts = $this->contact->explodePhoneNumber($number);

        $this->assertEquals('123', $parts['areaCode']);
        $this->assertEquals('4567890', $parts['number']);
        $this->assertEquals('', $parts['extension']);

    }

    public function testContactExplodeNumberUSExtension()
    {
        $number = '+11234567890 321';

        $parts = $this->contact->explodePhoneNumber($number);

        $this->assertEquals('123', $parts['areaCode']);
        $this->assertEquals('4567890', $parts['number']);
        $this->assertEquals('321', $parts['extension']);

    }

    public function testContactExplodeNumberInternational()
    {
        $number = '+2312345678321';

        $parts = $this->contact->explodePhoneNumber($number);

        $this->assertEquals('231', $parts['areaCode']);
        $this->assertEquals('2345678321', $parts['number']);
        $this->assertEquals('', $parts['extension']);

    }

}
