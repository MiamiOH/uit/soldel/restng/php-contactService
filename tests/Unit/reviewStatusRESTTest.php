<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ReviewStatusRESTTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $reviewStatusREST;

    private $api;
    private $reviewStatus;

    private $rpListData = [];
    private $rpListType = '';

    protected function setUp():void
    {

        $this->rpListData = [];
        $this->rpListType = '';

        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->reviewStatus = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\ReviewStatus')
            ->setMethods(array('getReviewStatusList'))
            ->getMock();

        $this->reviewStatus->method('getReviewStatusList')
            ->with($this->callback(array($this, 'getReviewStatusListType')))
            ->will($this->returnCallback(array($this, 'getReviewStatusListMock')));

        $this->reviewStatusREST = new \MiamiOH\RestngContactService\Services\ReviewStatusREST();
        $this->reviewStatusREST->setLogger();
        $this->reviewStatusREST->setApp($this->api);
        $this->reviewStatusREST->setReviewStatus($this->reviewStatus);
        $this->reviewStatusREST->setLogger();
    }

    public function testGetReviewStatusListStudent()
    {

        $this->rpListData = [
            [
                'pidm' => 123456,
                'uniqueId' => 'doej',
                'reviewStatus' => 'p',
                'type' => 'student'
            ],
            [
                'pidm' => 987654,
                'uniqueId' => 'smithd',
                'reviewStatus' => 'p',
                'type' => 'student'
            ],
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'student']);

        $this->reviewStatusREST->setRequest($request);

        $resp = $this->reviewStatusREST->getReviewStatusList();

        $this->assertEquals('student', $this->rpListType['type'],
            'Review status list called with expected type');

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(2, count($payload));

    }

    public function testGetReviewStatusListEmployee()
    {

        $this->rpListData = [
            [
                'pidm' => 123456,
                'uniqueId' => 'doej',
                'reviewStatus' => 'p',
                'type' => 'employee'
            ],
            [
                'pidm' => 987654,
                'uniqueId' => 'smithd',
                'reviewStatus' => 'p',
                'type' => 'employee'
            ],
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        $request->expects($this->once())->method('getOptions')
            ->willReturn(['type' => 'employee']);

        $this->reviewStatusREST->setRequest($request);

        $resp = $this->reviewStatusREST->getReviewStatusList();

        $this->assertEquals('employee', $this->rpListType['type'],
            'Review status list called with expected type');

        $payload = $resp->getPayload();

        $this->assertEquals(App::API_OK, $resp->getStatus());
        $this->assertTrue(is_array($payload));
        $this->assertEquals(2, count($payload));

    }

    public function getReviewStatusListType($subject)
    {
        $this->rpListType = $subject;

        return true;
    }

    public function getReviewStatusListMock()
    {
        return $this->rpListData;
    }
}
