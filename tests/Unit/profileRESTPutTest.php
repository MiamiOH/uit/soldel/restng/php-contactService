<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ProfileRESTPutTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $profileREST;

    private $api;
    private $profile;
    private $request;

    private $requestResourceParam = '';
    private $requestResourceParamMocks = [];
    private $mockUpdateResponse = '';

    protected function setUp():void
    {

        $this->requestResourceParam = '';
        $this->requestResourceParamMocks = [];
        $this->mockUpdateResponse = '';

        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $bannerUtil->method('getId')->willReturn($bannerId);

        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getData'))
            ->getMock();

        $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->callback(array($this, 'getResourceParamWith')))
            ->will($this->returnCallback(array($this, 'getResourceParamMock')));

        $this->profile = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Profile')
            ->setMethods(array('update'))
            ->getMock();

        $this->profileREST = new \MiamiOH\RestngContactService\Services\ProfileREST();

        $this->profileREST->setApp($this->api);
        $this->profileREST->setProfile($this->profile);
        $this->profileREST->setBannerUtil($bannerUtil);
        $this->profileREST->setLogger();
    }

    public function testProfileRESTPut()
    {

        $this->requestResourceParamMocks = [
            'muid' => 'doej',
        ];

        $this->mockUpdateResponse = true;

        $this->profile->method('update')
            ->with($this->callback(array($this, 'updateWith')))
            ->will($this->returnCallback(array($this, 'updateMock')));

        $this->profileREST->setRequest($this->request);

        $resp = $this->profileREST->updateProfileStatus();

        $payload = $resp->getPayload();

        $this->assertTrue(is_array($payload));

    }

    public function getResourceParamWith($subject)
    {
        $this->requestResourceParam = $subject;

        return true;
    }

    public function getResourceParamMock()
    {
        if (isset($this->requestResourceParamMocks[$this->requestResourceParam])) {
            return $this->requestResourceParamMocks[$this->requestResourceParam];
        }

        return null;
    }


    public function updateWith($subject)
    {
        $this->updatePidm = $subject;

        return true;
    }

    public function updateMock()
    {
        return $this->mockUpdateResponse;
    }

}
