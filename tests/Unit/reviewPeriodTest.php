<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

class ReviewPeriodTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $reviewPeriod;

    private $term;
    private $student;
    private $configObj;

    private $configData = [];

    protected function setUp():void
    {

        $this->configObj = $this->getMockBuilder('\MiamiOH\RESTng\Util\Configuration')
            ->setMethods(array('getConfiguration'))
            ->getMock();

        $this->configObj->method('getConfiguration')
            ->will($this->returnCallback(array($this, 'getConfigMock')));

        $this->term = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Term')
            ->setMethods(array('getCurrentReviewTermForStudents','getCurrentReviewTermForEmployees'))
            ->getMock();

        $this->student = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\Student')
            ->setMethods(array(
                'getStudentsForReview',
                'setTermCode',
                'setReviewState'
            ))
            ->getMock();

        $this->reviewPeriod = new \MiamiOH\RestngContactService\Services\ReviewPeriod();

        $this->reviewPeriod->setStudent($this->student);
    }

    public function testGetReviewPeriodStudent()
    {

        $reviewStudents = [
            [
                'pidm' => '123456',
            ],
            [
                'pidm' => '123457',
            ],
        ];

        $daysBeforeTerm = 7;
        $daysAfterTerm = 14;
        $termStartOffset = -1;

        $this->configData = [
            'daysBeforeTermStartRequested' => $daysBeforeTerm,
            'daysAfterTermStartRequired' => $daysAfterTerm,
        ];
        $this->reviewPeriod->setConfiguration($this->configObj);

        $this->term->expects($this->once())->method('getCurrentReviewTermForStudents')
            ->willReturn([
                'termCode' => '201710',
                'description' => 'Fall 2016',
                'termStartDate' => date('Y-m-d',
                    strtotime($termStartOffset . ' days')),
            ]);
        $this->reviewPeriod->setTerm($this->term);

        $this->student->expects($this->once())->method('setTermCode')
            ->with($this->equalTo('201710'))
            ->willReturn(true);

        $this->student->expects($this->once())->method('setReviewState')
            ->with($this->equalTo('requested'))
            ->willReturn(true);

        $this->student->expects($this->once())->method('getStudentsForReview')
            ->willReturn($reviewStudents);

        $period = $this->reviewPeriod->getReviewPeriod('student');

        $this->assertEquals('requested', $period['state']);
        $this->assertEquals('student', $period['type']);
        $this->assertEquals(date('Ymd',
            strtotime('-' . ($daysBeforeTerm - $termStartOffset) . ' days')),
            $period['requestedStartDate']);
        $this->assertEquals(date('Ymd',
            strtotime('+' . ($daysAfterTerm + $termStartOffset) . ' days')),
            $period['requiredStartDate']);
        $this->assertTrue(is_array($period['people']));
        $this->assertEquals(count($reviewStudents), count($period['people']));

    }

    public function testGetReviewPeriodStudentNoReview()
    {

        $this->configData = [
            'daysBeforeTermStartRequested' => '7',
            'daysAfterTermStartRequired' => '14',
        ];
        $this->reviewPeriod->setConfiguration($this->configObj);

        $this->term->expects($this->once())->method('getCurrentReviewTermForStudents')
            ->willReturn([
                'termCode' => '',
                'description' => '',
                'termStartDate' => '',
            ]);
        $this->reviewPeriod->setTerm($this->term);

        $this->student->expects($this->once())->method('setTermCode')
            ->with($this->equalTo(''))
            ->willReturn(true);

        $this->student->expects($this->once())->method('setReviewState')
            ->with($this->equalTo(''))
            ->willReturn(true);

        $this->student->expects($this->once())->method('getStudentsForReview')
            ->willReturn([]);

        $period = $this->reviewPeriod->getReviewPeriod('student');

        $this->assertEquals('', $period['state']);
        $this->assertEquals('student', $period['type']);
        $this->assertEquals('', $period['requestedStartDate']);
        $this->assertEquals('', $period['requiredStartDate']);
        $this->assertTrue(is_array($period['people']));
        $this->assertEquals(0, count($period['people']));

    }

    public function testGetReviewPeriodStudentRequired()
    {

        $reviewStudents = [
            [
                'pidm' => '123456',
            ],
            [
                'pidm' => '123457',
            ],
        ];

        $this->configData = [
            'daysBeforeTermStartRequested' => '7',
            'daysAfterTermStartRequired' => '14',
        ];
        $this->reviewPeriod->setConfiguration($this->configObj);

        $this->term->expects($this->once())->method('getCurrentReviewTermForStudents')
            ->willReturn([
                'termCode' => '201710',
                'description' => 'Fall 2016',
                'termStartDate' => date('Y-m-d', strtotime('-16 days')),
            ]);
        $this->reviewPeriod->setTerm($this->term);

        $this->student->expects($this->once())->method('setTermCode')
            ->with($this->equalTo('201710'))
            ->willReturn(true);

        $this->student->expects($this->once())->method('setReviewState')
            ->with($this->equalTo('required'))
            ->willReturn(true);

        $this->student->expects($this->once())->method('getStudentsForReview')
            ->willReturn($reviewStudents);

        $list = $this->reviewPeriod->getReviewPeriod('student');

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Unsupported review required list type bob
     */
    public function testGetReviewPeriodStudentValidType()
    {

        $this->reviewPeriod->getReviewPeriod('bob');

    }

    public function getConfigMock()
    {
        return $this->configData;
    }

}
