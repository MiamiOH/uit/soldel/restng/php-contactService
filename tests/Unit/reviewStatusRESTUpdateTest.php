<?php

namespace MiamiOH\RestngContactService\Tests\Unit;

use MiamiOH\RESTng\App;

class ReviewStatusRESTUpdateTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $reviewStatusREST;

    private $api;
    private $reviewStatus;
    private $bannerUtil;
    private $bannerId;

    private $getIdKey = '';
    private $getIdValue = '';
    private $setStatusResponse = null;
    private $setStatusModel = [];

    protected function setUp():void
    {

        $this->rpListData = [];
        $this->rpListType = '';

        $this->getIdKey = '';
        $this->getIdValue = '';
        $this->setStatusResponse = null;
        $this->setStatusModel = [];

        $this->api = $this->createMock(App::class);


        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        $this->reviewStatus = $this->getMockBuilder('\MiamiOH\RestngContactService\Services\ReviewStatus')
            ->setMethods(array('getReviewStatusList', 'setStatus'))
            ->getMock();

        $this->bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();

        $this->bannerUtil->method('getId')
            ->with($this->callback(array($this, 'getIdKey')),
                $this->callback(array($this, 'getIdValue')))
            ->will($this->returnCallback(array($this, 'getIdMock')));

        $this->bannerId = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getUniqueId', 'getPidm'))
            ->getMock();

        $this->reviewStatusREST = new \MiamiOH\RestngContactService\Services\ReviewStatusREST();

        $this->reviewStatusREST->setLogger();
        $this->reviewStatusREST->setApp($this->api);
        $this->reviewStatusREST->setReviewStatus($this->reviewStatus);
        $this->reviewStatusREST->setBannerUtil($this->bannerUtil);
        $this->reviewStatusREST->setLogger();
    }

    public function testGetReviewStatusListStudent()
    {
        $updateModel = [
            'pidm' => 123456,
            'reviewRequired' => true,
            'reviewStatus' => 'P',
            'type' => 'student'
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getResourceParamKey', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('doej');

        $request->expects($this->once())->method('getResourceParamKey')
            ->with($this->equalTo('muid'))->willReturn('uniqueId');

        $request->expects($this->once())->method('getData')
            ->willReturn($updateModel);

        $this->bannerId->expects($this->once())->method('getPidm')
            ->willReturn(123456);

        $this->reviewStatusREST->setRequest($request);

        $this->reviewStatus->method('setStatus')
            ->willReturn(true);

        $resp = $this->reviewStatusREST->updateReviewStatus();

        $this->assertEquals(App::API_OK, $resp->getStatus());

    }

    public function testGetReviewStatusListStudentNotFound()
    {

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getResourceParamKey'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('doej');

        $request->expects($this->once())->method('getResourceParamKey')
            ->with($this->equalTo('muid'))->willReturn('uniqueId');

        $this->bannerUtil->method('getId')
            ->will($this->throwException(new \MiamiOH\RESTng\Service\Extension\BannerIdNotFound("No matches for 'szbuniq_unique_id = DOEJ'")));
        $this->reviewStatusREST->setRequest($request);

        $resp = $this->reviewStatusREST->updateReviewStatus();

        $this->assertEquals(App::API_NOTFOUND, $resp->getStatus());

    }

    public function testReviewStatusStudentUpdate()
    {

        $this->setStatusResponse = true;

        $updateModel = [
            'uniqueId' => 'doej',
            'pidm' => 123456,
            'reviewRequired' => true,
            'reviewStatus' => 'P',
            'type' => 'student'
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getResourceParamKey', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('doej');

        $request->expects($this->once())->method('getResourceParamKey')
            ->with($this->equalTo('muid'))->willReturn('uniqueId');

        $request->expects($this->once())->method('getData')
            ->willReturn($updateModel);

        $this->bannerId->expects($this->once())->method('getPidm')
            ->willReturn(123456);

        $this->bannerId->expects($this->once())->method('getUniqueId')
            ->willReturn('doej');

        $this->reviewStatus->method('setStatus')
            ->with($this->callback(array($this, 'setStatusWithModel')))
            ->will($this->returnCallback(array($this, 'setStatusMock')));

        $this->reviewStatusREST->setRequest($request);

        $resp = $this->reviewStatusREST->updateReviewStatus();

        $this->assertEquals(App::API_OK, $resp->getStatus());

        $this->assertEquals($updateModel, $this->setStatusModel);

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage PIDM provided in URL does not match model pidm
     *     attribute
     */
    public function testReviewStatusStudentUpdateMismatchPidm()
    {

        $this->setStatusResponse = true;

        $updateModel = [
            'uniqueId' => 'doej',
            'pidm' => 123455,
            'reviewRequired' => true,
            'reviewStatus' => 'P',
            'type' => 'student'
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getResourceParamKey', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('doej');

        $request->expects($this->once())->method('getResourceParamKey')
            ->with($this->equalTo('muid'))->willReturn('uniqueId');

        $request->expects($this->once())->method('getData')
            ->willReturn($updateModel);

        $this->bannerId->expects($this->once())->method('getPidm')
            ->willReturn(123456);

        $this->bannerId->expects($this->once())->method('getUniqueId')
            ->willReturn('doej');

        $this->reviewStatus->method('setStatus')
            ->with($this->callback(array($this, 'setStatusWithModel')))
            ->will($this->returnCallback(array($this, 'setStatusMock')));

        $this->reviewStatusREST->setRequest($request);

        $resp = $this->reviewStatusREST->updateReviewStatus();

    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage UniqueID provided in URL does not match model
     *     uniqueId attribute
     */
    public function testReviewStatusStudentUpdateMismatchUid()
    {

        $this->setStatusResponse = true;

        $updateModel = [
            'uniqueId' => 'doec',
            'pidm' => 123456,
            'reviewRequired' => true,
            'reviewStatus' => 'P',
            'type' => 'student'
        ];

        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getResourceParamKey', 'getData'))
            ->getMock();

        $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('muid'))->willReturn('doej');

        $request->expects($this->once())->method('getResourceParamKey')
            ->with($this->equalTo('muid'))->willReturn('uniqueId');

        $request->expects($this->once())->method('getData')
            ->willReturn($updateModel);

        $this->bannerId->expects($this->once())->method('getPidm')
            ->willReturn(123456);

        $this->bannerId->expects($this->once())->method('getUniqueId')
            ->willReturn('doej');

        $this->reviewStatus->method('setStatus')
            ->with($this->callback(array($this, 'setStatusWithModel')))
            ->will($this->returnCallback(array($this, 'setStatusMock')));

        $this->reviewStatusREST->setRequest($request);

        $resp = $this->reviewStatusREST->updateReviewStatus();

    }

    public function getIdKey($subject)
    {
        $this->getIdKey = $subject;

        return true;
    }

    public function getIdValue($subject)
    {
        $this->getIdValue = $subject;

        return true;
    }

    public function getIdMock()
    {
        return $this->bannerId;
    }

    public function setStatusWithModel($subject)
    {
        $this->setStatusModel = $subject;

        return true;
    }

    public function setStatusMock()
    {
        return $this->setStatusResponse;
    }

}
