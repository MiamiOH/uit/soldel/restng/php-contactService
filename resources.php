<?php

return [
    'resources' => [
        'person' => [
            MiamiOH\RestngContactService\Resources\ConfigResourceProvider::class,
            MiamiOH\RestngContactService\Resources\ContactResourceProvider::class,
            MiamiOH\RestngContactService\Resources\PledgeResourceProvider::class,
            MiamiOH\RestngContactService\Resources\AttendanceIntentionResourceProvider::class,
            MiamiOH\RestngContactService\Resources\ProfileResourceProvider::class,
            MiamiOH\RestngContactService\Resources\RelationResourceProvider::class,
            MiamiOH\RestngContactService\Resources\ReviewPeriodResourceProvider::class,
            MiamiOH\RestngContactService\Resources\ReviewStatusResourceProvider::class,
            MiamiOH\RestngContactService\Resources\ReviewSummaryResourceProvider::class,
            MiamiOH\RestngContactService\Resources\Fall2020SummaryResourceProvider::class,
        ],
    ]
];
