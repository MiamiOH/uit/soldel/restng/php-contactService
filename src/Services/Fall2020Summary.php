<?php

namespace MiamiOH\RestngContactService\Services;

use Carbon\Carbon;

class Fall2020Summary
{

    private $datasource_name = 'MUWS_GEN_PROD';

    private $database;
    private $dbh;

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->database = $database;
        $this->dbh = $this->database->getHandle($this->datasource_name);
    }

    /**
     * @param $periodRecords
     * @param array $options
     *
     * @return array
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getSummaryDetails($periodRecords): array
    {
        $minActionDate = Carbon::parse($periodRecords['requestedStartDateFormatted']);

        $attendanceDetails = $this->attendanceDetails($minActionDate);
        $pledgeDetails = $this->pledgeDetails($minActionDate);

        return [
            'state' => $periodRecords['state'],
            'requestedStartDate' => $periodRecords['requestedStartDateFormatted'],
            'requiredStartDate' => $periodRecords ['requiredStartDateFormatted'],
            'attendanceDetails' => $attendanceDetails,
            'pledgeDetails' => $pledgeDetails,
        ];
    }

    /**
     * @param $minActionDate
     * @return mixed
     */
    private function attendanceDetails(Carbon $minActionDate): array
    {
        $queryString = '
            with intention as (
                select *
                from safmgr.attendance_intention a
                    where a.id = (select max(id) from safmgr.attendance_intention b where a.uniqueid = b.uniqueid and b.response_time >= to_date(?,\'YYYY-MM-DD\'))
            ),
            review as (
                select *
                    from safmgr.person_contact_activity
                    where add_date >= to_date(?, \'YYYY-MM-DD\')
            )
            select campus, choice as label, count(*) as value
                from review
                    inner join szbuniq on szbuniq_pidm = review.pidm
                    left outer join intention on upper(intention.uniqueid) = szbuniq_unique_id
                    where review_status != \'cancelled\'
                group by campus, choice
            ';

        return $this->convertKeys($this->dbh->queryall_array($queryString, [$minActionDate->format('Y-m-d'), $minActionDate->format('Y-m-d')]));
    }

    /**
     * @param $minActionDate
     * @return mixed
     */
    private function pledgeDetails($minActionDate)
    {
        $queryString = '
            with pledge_responses as (
                select * 
                    from safmgr.pledge a
                    where a.id = (select max(id) from safmgr.pledge b where a.uniqueid = b.uniqueid and b.response_time >= to_date(?,\'YYYY-MM-DD\'))
            ),
            review as (
                select *
                    from safmgr.person_contact_activity
                    where add_date >= to_date(?, \'YYYY-MM-DD\')
            )
            select campus, response as label, count(*) as value
                from review
                    inner join szbuniq on szbuniq_pidm = review.pidm
                    left outer join pledge_responses on upper(pledge_responses.uniqueid) = szbuniq_unique_id
                group by campus, response
            ';

        return $this->convertKeys($this->dbh->queryall_array($queryString, [$minActionDate->format('Y-m-d'), $minActionDate->format('Y-m-d')]));
    }

    public function convertKeys(array $array): array
    {
        $new = [];

        foreach ($array as $iValue) {
            $new[] = $this->convertArrayKeys($iValue);
        }

        return $new;
    }

    public function convertArrayKeys(array $array): array
    {
        $new = [];

        foreach (array_keys($array) as $orgKey) {
            $newKey = $this->convertString($orgKey);
            $new[$newKey] = $array[$orgKey];
        }

        return $new;
    }

    public function convertString(string $string)
    {
        return lcfirst(str_replace('_', '', ucwords($string, '_')));
    }
}