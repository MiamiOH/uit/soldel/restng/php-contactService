<?php

namespace MiamiOH\RestngContactService\Services;

class ReviewStatus
{

    private $datasource_name = 'MUWS_GEN_PROD';

    private $database;
    private $dbh;
    /** @var WAS */
    private $was;
    private $profile;
    private $totalRecords;

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->database = $database;
        $this->dbh = $this->database->getHandle($this->datasource_name);
    }

    /**
     * @param $was
     */
    public function setWas($was)
    {
        $this->was = $was;
    }

    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    public function setTotalRecords($totalRecords)
    {
        $this->totalRecords = $totalRecords;
    }

    public function getTotalRecords()
    {
        return $this->totalRecords;
    }

    /**
     * @param $type
     *
     * @return mixed
     * @throws \Exception
     */
    public function getReviewStatusList($options, $isPaged, $offset, $limit)
    {
        $type = isset($options['type']) ? $options['type'] : '';
        if ($type && $type !== 'student' && $type !== 'employee') {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Invalid type "' . $type . '" for getReviewStatusList');
        }

        $statusList = isset($options['status']) ? $options['status'] : '';
        $minActionDate = isset($options['minActionDate']) ? $options['minActionDate'] : '';

        $params = [];
        $pcaQueryString = "SELECT * 
            FROM safmgr.person_contact_activity pca
            WHERE 1=1";

        $maxReviewStatusQueryString = "SELECT pidm, MAX(status_id) max_status_id
            FROM safmgr.person_contact_activity
            WHERE 1=1";

        if ($type) {
            $pcaQueryString .= '
                              AND pca.review_type = ?
                ';
            $params[] = $type;
        }

        if ($statusList) {
            $placeHolders = [];
            foreach ($statusList as $status) {
                $placeHolders[] = '?';
                $params[] = $status;
            }

            $plList = implode(', ', $placeHolders);
            $pcaQueryString .= "
                              AND pca.review_status in ($plList)";
        }

        if ($minActionDate) {
            $pcaQueryString .= '
                              AND (review_date is null OR review_date >= to_date(?, \'YYYYMMDD\'))
                ';
            $params[] = $minActionDate;
            $maxReviewStatusQueryString .= '
                              AND (review_date is null OR review_date >= to_date(?, \'YYYYMMDD\'))
                ';
            $params[] = $minActionDate;
        }

        if ($type) {
            $maxReviewStatusQueryString .= '
                              AND review_type = ?
                ';
            $params[] = $type;
        }

        $maxReviewStatusQueryString .= ' GROUP BY pidm';


        $queryString = 'SELECT /*+ ALL_ROWS */
                  pca.status_id as status_id,
                  lower(szbuniq_unique_id) as uniqueid,
                  pca.pidm as pidm,
                  to_char(pca.add_date, \'YYYY-MM-DD\') as add_date,
                  to_char(pca.review_date, \'YYYY-MM-DD\') as review_date,
                  pca.review_status as review_status,
                  pca.review_type as review_type
                FROM ( ';
        $queryString .= $pcaQueryString;
        $queryString .= ') pca, ( ';
        $queryString .= $maxReviewStatusQueryString;
        $queryString .= ') max_status,
                szbuniq
                WHERE pca.pidm    = max_status.pidm
                AND pca.status_id = max_status.max_status_id
                AND szbuniq_pidm  = pca.pidm
                ORDER BY pidm';

        if ($isPaged) {
            $minRowToFetch = $offset;
            $maxRowToFetch = $minRowToFetch + $limit - 1;
            $totalRecords = $this->dbh->queryfirstcolumn("select count(*) from ( $queryString )",
                $params);
            $this->setTotalRecords($totalRecords);

            $queryString = "select status_id, uniqueid, pidm, add_date, review_date, review_status, review_type
                    from ( select a.*, ROWNUM rnum
                          from ( $queryString ) a
                          where ROWNUM <= ? )
                    where rnum  >= ?";

            $params[] = $maxRowToFetch;
            $params[] = $minRowToFetch;

            $sth = $this->dbh->prepare($queryString);

        } else {

            $sth = $this->dbh->prepare($queryString);
        }

        $sth->execute($params);

        $results = [];

        while ($record = $sth->fetchrow_assoc()) {
            $results[] = $this->makeModel($record);
        }

        return $results;
    }

    /**
     * @param $pidm
     */
    public function getCurrentStatus($pidm, $type)
    {
        $model = null;

        $recordExists = $this->dbh->queryfirstcolumn('
                select count(*)
                from safmgr.person_contact_activity
                where pidm = ?
                  and review_type = ?
            ', $pidm, $type);

        if ($recordExists) {
            $record = $this->dbh->queryfirstrow_assoc('
                select status_id, lower(szbuniq_unique_id) as uniqueid, pidm, to_char(add_date, \'YYYY-MM-DD\') as add_date,
                    to_char(review_date, \'YYYY-MM-DD\') as review_date, review_status, review_type
                from safmgr.person_contact_activity pa inner join szbuniq on pa.pidm = szbuniq_pidm
                where pidm = ?
                  and status_id = (
                    select max(status_id)
                    from safmgr.person_contact_activity pb
                    where pa.pidm = pb.pidm
                      and review_type = ?
                  )
            ', $pidm, $type);

            $model = $this->makeModel($record);
        }

        return $model;
    }

    /**
     * @param $model
     *
     * @return bool
     */
    public function setStatus($model)
    {
        /*
         * Always addStatus if no current or current has a reviewDate
         * Update requested to required with no other change
         * Update requested or required to cancelled or complete, set review_date.
         * Setting same status is a noop.
         */

        if (!isset($model['reviewStatus'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest(__CLASS__ . '::setStatus() requires reviewStatus in model');
        }

        if (!in_array($model['reviewStatus'],
            ['requested', 'required', 'complete', 'cancelled'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest(__CLASS__ . '::setStatus() requires valid reviewStatus (requested, required, complete, cancelled');
        }

        // TODO Validate that pidm and uid in model match

        $currentStatus = $this->getCurrentStatus($model['pidm'], $model['type']);

        // Only update WAS if we have a valid state change
        $updateWAS = false;
        if (is_null($currentStatus) || $currentStatus['reviewDate']) {

            $this->addStatus($model);

            $updateWAS = true;

        } elseif ($currentStatus['reviewStatus'] === 'requested' && $model['reviewStatus'] === 'required') {
            if (!isset($model['statusId'])) {
                $model['statusId'] = $currentStatus['statusId'];
            }

            if ((int)$model['statusId'] !== (int)$currentStatus['statusId']) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Provide statusId does not match current statusId, cannot update');
            }

            $model['reviewDate'] = '';

            $this->updateStatus($model);

            $updateWAS = true;

        } elseif (in_array($currentStatus['reviewStatus'],
                ['requested', 'required']) && in_array($model['reviewStatus'],
                ['complete', 'cancelled'])) {

            if (!isset($model['statusId'])) {
                $model['statusId'] = $currentStatus['statusId'];
            }

            if ((int)$model['statusId'] !== (int)$currentStatus['statusId']) {
                throw new \MiamiOH\RESTng\Exception\BadRequest('Provide statusId does not match current statusId, cannot update');
            }

            if (empty($model['reviewDate'])) {
                $model['reviewDate'] = date('Y-m-d');
            }

            $this->updateStatus($model);

            $updateWAS = true;

        }

        if ($updateWAS) {

            $options = [];
            if ($model['reviewStatus'] === 'complete') {
                $options['reviewRequested'] = false;
                $options['reviewRequired'] = false;
                $options['complete'] = true;
            } elseif ($model['reviewStatus'] === 'requested') {
                $options['reviewRequested'] = true;
                $options['reviewRequired'] = false;
                $options['complete'] = false;
            } elseif ($model['reviewStatus'] === 'required') {
                $options['reviewRequested'] = false;
                $options['reviewRequired'] = true;
                $options['complete'] = false;
            } elseif ($model['reviewStatus'] === 'cancelled') {
                $options['reviewRequested'] = false;
                $options['reviewRequired'] = false;
                $options['complete'] = false;
            }
            $this->was->updateWAS($model['uniqueId'], $model['type'], $options);

        }

        return true;
    }

    private function addStatus($model)
    {

        $ldapData = $this->profile->getLDAPAttributes($model['uniqueId']);

        $reviewDate = null;

        if ($model['reviewStatus'] === 'complete') {
            $reviewDate = date('Y-m-d');
        }

        $this->dbh->perform('
                insert into safmgr.person_contact_activity (status_id, pidm, add_date, review_status, 
                  review_date, review_type, campus)
                  values (safmgr.person_contact_activity_seq.nextval, ?, sysdate, ?, to_date(?, \'YYYY-MM-DD\'), ?, ?)
            ', [
            $model['pidm'],
            $model['reviewStatus'],
            $reviewDate,
            $model['type'],
            $ldapData['campus']
        ]);

        return true;
    }

    private function updateStatus($model)
    {
        $this->dbh->perform('
                update safmgr.person_contact_activity set
                  review_status = ?, review_date = to_date(?, \'YYYY-MM-DD\')
                where status_id = ?
            ', [$model['reviewStatus'], $model['reviewDate'], $model['statusId']]);

        return true;
    }

    /**
     * @param $record
     *
     * @return array
     */
    private function makeModel($record)
    {
        $model = [
            'pidm' => $record['pidm'],
            'uniqueId' => $record['uniqueid'],
            'type' => $record['review_type'],
            'reviewStatus' => $record['review_status'],
            'reviewDate' => $record['review_date'] ? $record['review_date'] : '',
            'addDate' => $record['add_date'] ? $record['add_date'] : '',
            'statusId' => $record['status_id'] ? $record['status_id'] : '',
        ];

        return $model;
    }

}