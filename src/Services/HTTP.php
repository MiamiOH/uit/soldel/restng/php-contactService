<?php

// @codeCoverageIgnoreStart

namespace MiamiOH\RestngContactService\Services;

class HTTP
{

    public function get($url)
    {
        return file_get_contents($url);
    }

}

// @codeCoverageIgnoreEnd
