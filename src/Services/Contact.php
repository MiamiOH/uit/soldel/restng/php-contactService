<?php

namespace MiamiOH\RestngContactService\Services;

use Exception;
use libphonenumber\PhoneNumberUtil;
use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Legacy\DB\STH;
use MiamiOH\RestngContactService\Exceptions\ReAddContactException;

class Contact extends \MiamiOH\RESTng\Service
{

    private $datasource_name = 'MUWS_GEN_PROD';

    /**
     * @var DBH
     */
    private $dbh;

    /**
     * @var PhoneHelper
     */
    private $phoneHelper;

    /**
     * @var PhoneNumberUtil
     */
    private $phoneUtil;

    private $filterRelationTypeList = [];
    private $filterContactType = '';

    /**
     * @param $database
     */
    public function setDatabase(DatabaseFactory $database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    /**
     * @param $phoneHelper
     */
    public function setPhoneHelper(PhoneHelper $phoneHelper)
    {
        $this->phoneHelper = $phoneHelper;
        $this->phoneUtil = $phoneHelper->getInstance();
    }

    /**
     * @param $pidm
     *
     * @throws Exception
     */
    public function read($pidm)
    {

        $values = [$pidm];

        $queryString = '
                SELECT spremrg_pidm, spremrg_priority, spremrg_last_name, spremrg_first_name,
                  spremrg_mi, spremrg_phone_area, spremrg_phone_number, spremrg_phone_ext,
                  spremrg_relt_code, spremrg_street_line1, spremrg_street_line2, spremrg_street_line3,
                  spremrg_city, spremrg_stat_code, spremrg_natn_code, spremrg_zip, spremrg_atyp_code,
                  stvrelt_desc
                FROM SPREMRG left outer join stvrelt on (spremrg_relt_code = stvrelt_code)
                where spremrg_pidm IN (' . implode(',',
                array_fill(0, count($values), '?')) . ')
            ';

        if (count($this->filterRelationTypeList) && $this->filterContactType) {
            throw new Exception('Filtering by contact type and relation type cannot be used together');
        }

        if (count($this->filterRelationTypeList)) {
            $placeHolders = [];
            foreach ($this->filterRelationTypeList as $type) {
                $values[] = $type;
                $placeHolders[] = '?';
            }
            $queryString .= '
                    AND spremrg_relt_code in (' . join(', ', $placeHolders) . ')';
        }

        if ($this->filterContactType) {
            switch ($this->filterContactType) {
                case 'missingPerson':
                    $queryString .= '
                        and spremrg_relt_code = ?';
                    $values[] = 'K';
                    break;

                case 'emergency':
                    $queryString .= '
                        and (spremrg_relt_code is null or spremrg_relt_code != ?)';
                    $values[] = 'K';
                    break;

                default:
                    throw new Exception('Unsupported contact type ' . $this->filterContactType);
            }
        }

        $queryString .= '
                ORDER BY spremrg_priority
            ';

        $records = $this->dbh->queryall_array($queryString, $values);

        for ($i = 0; $i < count($records); $i++) {
            $records[$i] = $this->makeModelFromRecord($records[$i]);
        }

        $this->clearFilters();

        return $records;
    }

    public function filterRelationType($type)
    {
        if (is_scalar($type)) {
            $type = [$type];
        }

        $this->filterRelationTypeList = $type;

        return $this;
    }

    public function filterContactType($type)
    {
        $this->filterContactType = $type;

        return $this;
    }

    public function clearFilters()
    {
        $this->filterRelationTypeList = [];
        $this->filterContactType = '';
    }

    /**
     * @param $contact
     * @param string $type
     *
     * @return array
     * @throws BadRequest
     * @throws \libphonenumber\NumberParseException
     * @throws Exception
     */
    public function create($contact, $type = 'emergency')
    {

        if ($type === 'missingPerson') {
            if (!isset($contact['relationCode']) || !$contact['relationCode']) {
                $contact['relationCode'] = 'K';
            }

            if ($contact['relationCode'] !== 'K') {
                throw new BadRequest('Invalid relation code ' . $contact['relationCode'] .
                    ' for missingPerson type');
            }
        } else {
            if (!isset($contact['relationCode'])) {
                $contact['relationCode'] = '';
            }

            if ($contact['relationCode'] === 'K') {
                throw new BadRequest('Invalid relation code ' . $contact['relationCode'] .
                    ' for emergency type');
            }
        }

        if (!(isset($contact['pidm']) && ($contact['pidm'] != '') &&
            isset($contact['phoneNumber']) && ($contact['phoneNumber'] != '') &&
            isset($contact['lastName']) && ($contact['lastName'] != '') &&
            isset($contact['firstName']) && ($contact['firstName'] != ''))) {
            throw new Exception("Missing Required Fields : Phone Number, First Name and Last Name");
        }

        $model['phoneNumber'] = $contact['phoneNumber'];

        $parsedPhoneNumber = $this->phoneUtil->parse($model['phoneNumber'], null,
            null, true);


        if ($this->phoneUtil->isValidNumber($parsedPhoneNumber)) {
            $model['phoneNumberNational'] = $this->phoneUtil->format($parsedPhoneNumber,
                $this->phoneHelper->nationalCode());
            $model['phoneNumberInternational'] = $this->phoneUtil->format($parsedPhoneNumber,
                $this->phoneHelper->internationalCode());
        } else {
            throw new BadRequest('Incorrect Phone Number entered');
        }

        $contact['priority'] = $this->dbh->queryfirstcolumn('
              select nvl(max(spremrg_priority), 0)
                from spremrg
                where spremrg_pidm = ?
            ', [$contact['pidm']]) + 1;

        // Maximum limit to number of emergency and missing person contact is 9.
        // If we try to create new contact with priority greater 9 throws exception
        // ToDo: Update DELETE service to reorder the records after deleting an entry
        if ($contact['priority'] > 9) {
            throw new \Exception('Creating new contact failed as maximum number of contacts reached.');
        }

        /*
         * Validate the relationCode if one was provided. We do allow empty relation
         * codes for emergency contacts, however.
         */
        if (isset($contact['relationCode']) && $contact['relationCode'] &&
            !$this->dbh->queryfirstcolumn('
              select count(*)
                from stvrelt
                where stvrelt_code = ?
              ', [$contact['relationCode']])) {
            throw new \Exception('Invalid relation code: ' . $contact['relationCode']);
        }

        $numberParts = $this->explodePhoneNumber($contact['phoneNumber']);

        $userName = $this->getApiUser()->getUsername();
        $dataOrigin = "RESTng EmergencyContact";


        $query = "
            declare
            BEGIN
            GB_EMERGENCY_CONTACT.P_CREATE(
            p_pidm             =>:P_PIDM,
            p_priority         =>:P_PRIORITY,
            p_last_name        =>:P_LAST_NAME,
            p_first_name       =>:P_FIRST_NAME,
            p_mi               =>:P_MID_NAME,
            p_street_line1     => NULL,
            p_street_line2     => NULL,
            p_street_line3     => NULL,
            p_city             => NULL,
            p_stat_code        => NULL,
            p_natn_code        => NULL,
            p_zip              => NULL,
            p_phone_area       => :P_PHONE_AREA,
            p_phone_number     => :P_PHONE_NUMBER,
            p_phone_ext        => NULL,
            p_relt_code        => :P_RELT_CODE,
            p_atyp_code        => NULL,
            p_data_origin      => :P_DATA_ORIGIN,
            p_user_id          => :P_USER_ID,
            p_surname_prefix   => NULL,
            p_ctry_code_phone  => NULL,
            p_house_number     => NULL,
            p_street_line4     => NULL,
            p_rowid_out        => :P_ROWID_OUT);
            END;";

        $sth = $this->dbh->prepare($query);

        $rowID = $sth->new_descriptor(OCI_D_ROWID);

        $sth->bind_by_name(':P_PIDM', $contact['pidm']);
        $sth->bind_by_name(':P_PRIORITY', $contact['priority']);
        $sth->bind_by_name(':P_LAST_NAME', $contact['lastName']);
        $sth->bind_by_name(':P_FIRST_NAME', $contact['firstName']);
        $sth->bind_by_name(':P_MID_NAME', $contact['middleName']);
        $sth->bind_by_name(':P_PHONE_AREA', $numberParts['areaCode']);
        $sth->bind_by_name(':P_PHONE_NUMBER', $numberParts['number']);
        $sth->bind_by_name(':P_RELT_CODE', $contact['relationCode']);
        $sth->bind_by_name(':P_DATA_ORIGIN', $dataOrigin);
        $sth->bind_by_name(':P_USER_ID', $userName);
        $sth->bind_by_name(':P_ROWID_OUT', $rowID, -1, OCI_B_ROWID);

        $sth->execute();

        // Call this method to build details of the emergency contact: relation,phonenumber
        $contact = $this->makeModelForPostResponse($rowID);

        $rowID->free();

        return $contact;

    }

    /**
     * @param $contact
     *
     * @return array
     * @throws Exception
     */
    public function update($contact)
    {

        if (!(isset($contact['pidm']) && ($contact['pidm'] != '') &&
            isset($contact['phoneNumber']) && ($contact['phoneNumber'] != '') &&
            isset($contact['lastName']) && ($contact['lastName'] != '') &&
            isset($contact['firstName']) && ($contact['firstName'] != ''))) {
            throw new Exception("Missing Required Fields : PIDM, Phone Number, First Name and Last Name");
        }

        if (!$this->dbh->queryfirstcolumn('
              select stvrelt_code
                from stvrelt
                where stvrelt_desc = ?
              ', [$contact['relationCode']])) {
            throw new \Exception('Invalid relation code: ' . $contact['relationCode']);
        }


        $query = "
            declare
            BEGIN
            GB_EMERGENCY_CONTACT.P_UPDATE(
            p_pidm             =>:P_PIDM,
            p_priority         =>:P_PRIORITY,
            p_last_name        =>:P_LAST_NAME,
            p_first_name       =>:P_FIRST_NAME,
            p_mi               =>:P_MID_NAME,
            p_street_line1     => NULL,
            p_street_line2     => NULL,
            p_street_line3     => NULL,
            p_city             => NULL,
            p_stat_code        => NULL,
            p_natn_code        => NULL,
            p_zip              => NULL,
            p_phone_area       => :P_PHONE_AREA,
            p_phone_number     => :P_PHONE_NUMBER,
            p_phone_ext        => NULL,
            p_relt_code        => :P_RELT_CODE,
            p_atyp_code        => NULL,
            p_data_origin      => NULL,
            p_user_id          => NULL,
            p_surname_prefix   => NULL,
            p_ctry_code_phone  => NULL,
            p_house_number     => NULL,
            p_street_line4     => NULL,
            p_rowid        => :P_ROWID);
            END;";

        /**
         * @var STH $sth
         * */
        $sth = $this->dbh->prepare($query);
        $rowID = null;


        $sth->bind_by_name(':P_PIDM', $contact['pidm']);
        $sth->bind_by_name(':P_PRIORITY', $contact['priority']);
        $sth->bind_by_name(':P_LAST_NAME', $contact['lastName']);
        $sth->bind_by_name(':P_FIRST_NAME', $contact['firstName']);
        $sth->bind_by_name(':P_MID_NAME', $contact['middleName']);
        $sth->bind_by_name(':P_PHONE_AREA', $contact['areaCode']);
        $sth->bind_by_name(':P_PHONE_NUMBER', $contact['phoneNumber']);
        $sth->bind_by_name(':P_RELT_CODE', $contact['relationCode']);
        $sth->bind_by_name(':P_ROWID', $rowID, 100);

        $sth->execute();

        return $contact;

    }

    /**
     * @param $contact
     *
     * @param bool $purgeInvalidContacts
     * @return bool
     * @throws ReAddContactException
     */
    public function delete($contact, $purgeInvalidContacts = false)
    {
        $this->dbh->auto_commit(false);
        $contacts = $this->read($contact['pidm']);

        $query = "
                declare
                BEGIN
                GB_EMERGENCY_CONTACT.P_DELETE(
                p_pidm             => :P_PIDM,
                p_priority         => :P_PRIORITY,
                p_rowid            => NULL);
                END;";

        /**
         * @var STH $sth
         * */
        $sth = $this->dbh->prepare($query);
        $sth->bind_by_name(':P_PIDM', $contact['pidm']);

        //delete all entries and save the ones that are to be inserted back
        $priorityToBeDeleted = $contact['priority'];
        $contactsToBeInsertedBack = [];

        foreach ($contacts as $contact) {
            $sth->bind_by_name(':P_PRIORITY', $contact['priority']);
            try {
                $sth->execute();
            } catch (Exception $e) {
                $this->dbh->rollback();
                throw new Exception("Deleting contact failed." . $e->getMessage());
            }

            if ($contact['priority'] != $priorityToBeDeleted) {
                $contactsToBeInsertedBack[] = $contact;
            }
        }

        foreach ($contactsToBeInsertedBack as $contact) {
            if ($contact['relationCode'] === 'K') {
                $type = 'missingPerson';
            } else {
                $type = 'emergency';
            }

            try {
                $this->create($contact, $type);
            } catch (Exception $e) {
                if ($purgeInvalidContacts) {
                    continue;
                }
                $this->dbh->rollback();
                throw new ReAddContactException($e->getMessage());
            }
        }

        $this->dbh->commit();

        return true;
    }


    /**
     * @param $record
     *
     * @return array
     */
    public function makeModelFromRecord($record)
    {
        $model = [];

        $areaCode = preg_replace('/[\s\(\)+-]/', '', $record['spremrg_phone_area']);
        $number = preg_replace('/[\s\(\)+-]/', '', $record['spremrg_phone_number']);

        // if areaCode and number are in USA format then display +1 for USA country code
        if (preg_match('/\A\d{3}\z/', $areaCode) && preg_match('/\A\d{7}\z/',
                $number)) {

            $phoneNumber = '+1' . $areaCode . $number;

        } /* if sprtele_intl_access is not set and areaCode and number are not in USA format, then we assume that inetrnational
           phone number is stored in sprtele_phone_area + sprtele_phone_number. */
        else {
            $phoneNumber = '+' . $areaCode . $number;
        }


        $model['pidm'] = $record['spremrg_pidm'];
        $model['relation'] = isset($record['stvrelt_desc']) ? $record['stvrelt_desc'] : '';
        $model['relationCode'] = isset($record['spremrg_relt_code']) ? $record['spremrg_relt_code'] : '';
        $model['priority'] = isset($record['spremrg_priority']) ? $record['spremrg_priority'] : '';
        $model['firstName'] = isset($record['spremrg_first_name']) ? $record['spremrg_first_name'] : '';
        $model['lastName'] = isset($record['spremrg_last_name']) ? $record['spremrg_last_name'] : '';
        $model['middleName'] = isset($record['spremrg_mi']) ? $record['spremrg_mi'] : '';
        $model['phoneNumber'] = $phoneNumber;
        $model['phoneNumberNational'] = '';
        $model['phoneNumberInternational'] = '';


        try {

            $parsedPhoneNumber = $this->phoneUtil->parse($model['phoneNumber'], null,
                null, true);
            if ($this->phoneUtil->isValidNumber($parsedPhoneNumber)) {
                $model['phoneNumberNational'] = $this->phoneUtil->format($parsedPhoneNumber,
                    $this->phoneHelper->nationalCode());
                $model['phoneNumberInternational'] = $this->phoneUtil->format($parsedPhoneNumber,
                    $this->phoneHelper->internationalCode());
            } else {
                $model['phoneNumberNational'] = $model['phoneNumber'];
                $model['phoneNumberInternational'] = $model['phoneNumber'];
            }

        } catch (\libphonenumber\NumberParseException $e) {
            $model['phoneNumberNational'] = $model['phoneNumber'];
            $model['phoneNumberInternational'] = $model['phoneNumber'];
        }

        return $model;
    }

    public function explodePhoneNumber($number)
    {
        $parts = [
            'areaCode' => '',
            'number' => '',
            'extension' => '',
        ];

        if (preg_match('/\+1(\d{3})(\d{7}) ?(\d+)?/', $number, $matches)) {
            $parts['areaCode'] = $matches[1];
            $parts['number'] = $matches[2];
            $parts['extension'] = isset($matches[3]) ? $matches[3] : '';
        } else {
            // Do not include the '+' here, start at the second character
            // TODO test overall length and make sure it fits, use extension if necessary
            $parts['areaCode'] = substr($number, 1, 3);
            $parts['number'] = substr($number, 4);
        }

        return $parts;
    }

    /**
     * @param $rowID
     *
     * @return array
     * @throws Exception
     */
    public function makeModelForPostResponse($rowID)
    {

        // Read the newly inserted record from database to build the model for post response.
        $queryString = 'SELECT spremrg_pidm, spremrg_priority, spremrg_last_name, spremrg_first_name,
                  spremrg_mi, spremrg_phone_area, spremrg_phone_number, spremrg_phone_ext,
                  spremrg_relt_code, spremrg_street_line1, spremrg_street_line2, spremrg_street_line3,
                  spremrg_city, spremrg_stat_code, spremrg_natn_code, spremrg_zip, spremrg_atyp_code,
                  stvrelt_desc
                FROM SPREMRG left outer join stvrelt on (spremrg_relt_code = stvrelt_code)
                where SPREMRG.rowid = :P_ROWID';

        /**
         * @var STH $sth
         * */
        $sth = $this->dbh->prepare($queryString);

        $sth->bind_by_name(':P_ROWID', $rowID, -1, OCI_B_ROWID);

        $sth->execute();

        $record = $sth->fetchrow_assoc();

        $contact = $this->makeModelFromRecord($record);

        return $contact;

    }

}
