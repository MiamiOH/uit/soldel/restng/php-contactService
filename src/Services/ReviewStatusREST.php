<?php

namespace MiamiOH\RestngContactService\Services;

class ReviewStatusREST extends \MiamiOH\RESTng\Service
{

    /** @var ReviewStatus */
    private $reviewStatus;

    /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
    private $bannerUtil;

    public function setReviewStatus($reviewStatus)
    {
        $this->reviewStatus = $reviewStatus;
    }

    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    public function getReviewStatusList()
    {
        $this->log->debug('Start the review status service');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $isPaged = $request->isPaged();
        $offset = $request->getOffset();
        $limit = $request->getLimit();

        $options = $request->getOptions();

        $records = $this->reviewStatus->getReviewStatusList($options, $isPaged,
            $offset, $limit);
        $totalRecords = $this->reviewStatus->getTotalRecords();
        $response->setTotalObjects($totalRecords);

        $response->setPayload($records);

        return $response;
    }

    public function updateReviewStatus()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);

            return $response;
        }

        $pidm = $bannerId->getPidm();
        $uid = $bannerId->getUniqueId();

        // TODO Validate pidm and uid in data

        $data = $request->getData();

        if (isset($data['pidm']) && $data['pidm'] !== $pidm) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('PIDM provided in URL does not match model pidm attribute');
        }

        if (isset($data['uniqueId']) && $data['uniqueId'] !== $uid) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('UniqueID provided in URL does not match model uniqueId attribute');
        }

        $this->reviewStatus->setStatus($data);

        return $response;
    }

    public function updateReviewStatusCollection()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getData();

        $updateResults = [];
        for ($i = 0; $i < count($data); $i++) {
            try {
                $this->reviewStatus->setStatus($data[$i]);
                $updateResults[] = array(
                    'uniqueId' => $data[$i]['uniqueId'],
                    'code' => \MiamiOH\RESTng\App::API_OK,
                    'message' => 'Update complete'
                );
            } catch (\Exception $e) {
                $updateResults[] = array(
                    'uniqueId' => $data[$i]['uniqueId'],
                    'code' => \MiamiOH\RESTng\App::API_FAILED,
                    'message' => $e->getMessage()
                );
            }
        }

        $response->setPayload($updateResults);

        return $response;
    }

}