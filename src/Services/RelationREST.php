<?php

namespace MiamiOH\RestngContactService\Services;

class RelationREST extends \MiamiOH\RESTng\Service
{

    /** @var Relation */
    private $relation;

    public function setRelation($relation)
    {
        $this->relation = $relation;
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function getRelationList()
    {

        $this->log->debug('Start the relation service.');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        try {
            if (isset($options['relationType']) && $options['relationType']) {
                $this->relation->filterType($options['relationType']);
            }

            $payload = $this->relation->getList();

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        return $response;
    }

}

