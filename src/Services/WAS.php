<?php

namespace MiamiOH\RestngContactService\Services;

class WAS
{
    private $config;
    private $http;

    private $wasUpdateURL;
    private $wasRequiredUpdateURL;

    /** @var  \MiamiOH\RESTng\Util\Configuration $configuration */
    private $configuration;

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
        $this->config = $this->configuration->getConfiguration('PersonContactInfo',
            'Internal Config');
    }

    public function setHttp($http)
    {
        $this->http = $http;
    }

    /**
     * @param $uid
     * @param $type
     * @param array $options
     *
     * @return null
     * @throws \Exception
     */
    public function updateWAS($uid, $type, $options = [])
    {
        if (!$uid) {
            throw new \Exception('Missing uid for WAS update');
        }
        if($type == 'student') {
            if (!$this->config['wasClientUpdateUrl']) {
                throw new \Exception('Missing wasClientUpdateUrl for WAS update');
            }
            if (!$this->config['wasClientRequiredUpdateUrl']) {
                throw new \Exception('Missing wasClientRequiredUpdateUrl for WAS update');
            }
            $this->wasUpdateURL = $this->config['wasClientUpdateUrl'];
            $this->wasRequiredUpdateURL = $this->config['wasClientRequiredUpdateUrl'];
        } elseif ($type == 'employee') {
            if (!$this->config['wasClientUpdateUrlEmployee']) {
                throw new \Exception('Missing wasClientUpdateUrlEmployee for WAS update');
            }
            if (!$this->config['wasClientRequiredUpdateUrlEmployee']) {
                throw new \Exception('Missing wasClientRequiredUpdateUrlEmployee for WAS update');
            }
            $this->wasUpdateURL = $this->config['wasClientUpdateUrlEmployee'];
            $this->wasRequiredUpdateURL = $this->config['wasClientRequiredUpdateUrlEmployee'];

        } else {
            throw new \Exception('Invalid type for WAS update');
        }
        return $this->update($uid, $options);
    }

    private function update($uid, $options = [])
    {
        /*
         * Make sure to update both instances of the WAS app, the first
         * is the optional version which allows the user to continue
         * without review. The second requires them to review.
         */
        $queryParams = [];

        if (isset($options['reviewRequested'])) {
            $queryParams['reviewPending'] = $options['reviewRequested'] ? 1 : 0;
        }

        if (isset($options['complete'])) {
            $queryParams['complete'] = $options['complete'] ? 1 : 0;
        }

        $result = null;
        if ($queryParams) {
            $queryParams['uid'] = $uid;
            $url = $this->wasUpdateURL . '&' . $this->makeQueryString($queryParams);
            $result = $this->http->get($url);
        }

        if (strpos($result, 'logged') !== false) {
            $queryParams = [];

            if (isset($options['reviewRequired'])) {
                $queryParams['reviewPending'] = $options['reviewRequired'] ? 1 : 0;
            }

            if (isset($options['complete'])) {
                $queryParams['complete'] = $options['complete'] ? 1 : 0;
            }

            $result = null;
            if ($queryParams) {
                $queryParams['uid'] = $uid;
                $url = $this->wasRequiredUpdateURL . '&' . $this->makeQueryString($queryParams);

                $result = $this->http->get($url);

            }
        }
        return $result;
    }

    private function makeQueryString($params)
    {
        $pairs = [];

        foreach ($params as $key => $value) {
            $pairs[] = "$key=$value";
        }

        return implode('&', $pairs);
    }
}