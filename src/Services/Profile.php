<?php

namespace MiamiOH\RestngContactService\Services;

class Profile extends \MiamiOH\RESTng\Service
{

    private $dataSourceName = 'MUWS_GEN_PROD';

    private $ldapDataSource = 'RESTng_LDAP';

    private $database = '';

    /** @var  \MiamiOH\RESTng\Connector\LDAPFactory $ldapFactory */
    private $ldapFactory;
    /**
     * @var Term
     */
    private $term;
    private $currentTerm = [];
    /** @var Student */
    private $student;
    /** @var Employee */
    private $employee;

    private $readDataQuerySql = '';
    private $readDataQueryParams = [];

    private $config = [];

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setLdapFactory($ldapFactory)
    {
        $this->ldapFactory = $ldapFactory;
    }

    public function setStudent($student)
    {
        $this->student = $student;
    }

    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    public function setConfiguration($configuration)
    {
        $this->config = array_merge(
            $configuration->getConfiguration('PersonContactInfo', 'Review State'),
            $configuration->getConfiguration('PersonContactInfo', 'Internal Config')
        );
    }

    public function setTerm($term)
    {
        $this->term = $term;
    }


    public function read($pidm, $uniqueId)
    {
        // Get user affiliation, primary campus code and given name.
        $reviewType = $this->getReviewType($pidm);
        $dbh = $this->database->getHandle($this->dataSourceName);

        $this->makeReadQuery($pidm, $reviewType);
        
        $results = $dbh->queryfirstrow_assoc($this->readDataQuerySql,
            $this->readDataQueryParams);

        if ($results !== \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $studentData = [];
            if ($reviewType === 'employee') {
                $this->currentTerm = $this->term->getCurrentReviewTermForEmployees();
            } else {
                //treat everybody else students
                $this->currentTerm = $this->term->getCurrentReviewTermForStudents();
                $this->student->setTermCode($this->currentTerm['termCode']);
                $studentData = $this->student->getStudentData($pidm);
            }
            $results['requested_start_date'] = $this->getRequestStartDate($reviewType);
            $ldapAttributes = $this->getLDAPAttributes($uniqueId);
            $model = $this->makeModel($results, $studentData, $ldapAttributes);

        } else {
            // Set to empty array when there is no model for the pidm.
            $model = [];
        }

        return $model;
    }

    public function readList($pidms, $reviewType)
    {
         if ($reviewType === 'employee') {
            $this->currentTerm = $this->term->getCurrentReviewTermForEmployees();
         } else {
             $this->currentTerm = $this->term->getCurrentReviewTermForStudents();
             $this->student->setTermCode($this->currentTerm['termCode']);
         }

        $dbh = $this->database->getHandle($this->dataSourceName);

        $this->makeReadQuery($pidms, $reviewType);

        $results = $dbh->queryall_array($this->readDataQuerySql,
            $this->readDataQueryParams);

        $collection = [];
        if ($results !== \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            for ($i = 0; $i < count($results); $i++) {
                $studentData = [];
                if($reviewType != 'employee') {
                    $studentData = $this->student->getStudentData($results[$i]['szbuniq_pidm']);
                }
                // Get user affiliation, primary campus code and given name.
                $ldapAttributes = $this->getLDAPAttributes(strtolower($results[$i]['szbuniq_unique_id']));

                $collection[] = $this->makeModel($results[$i], $studentData,
                    $ldapAttributes);
            }
        }

        return $collection;
    }

    private function makeReadQuery($pidms, $reviewType)
    {
        if (is_scalar($pidms)) {
            $pidms = [$pidms];
        }

        $this->readDataQuerySql = '
            SELECT szbuniq_pidm, szbuniq_banner_id, szbuniq_unique_id,
                  spriden_first_name, spriden_last_name,
                  to_char(add_date, \'YYYY-MM-DD\') as add_date,
                  to_char(review_date, \'YYYY-MM-DD\') as review_date,
                  review_status, review_type, excluded_actions, sgrsatt_atts_code
                FROM szbuniq uniq
                  inner join spriden on (spriden_pidm = szbuniq_pidm)
                  left outer join
                    (select *
                      from safmgr.person_contact_activity pca
                      where status_id = (
                        select max(status_id)
                          from safmgr.person_contact_activity pcb
                          where pcb.pidm = pca.pidm
                          and review_type = ?
                      )) pcm on pcm.pidm = szbuniq_pidm
                  left outer join
                    (select *
                      from sgrsatt a
                      where sgrsatt_term_code_eff = (
                            SELECT MAX(c.sgrsatt_term_code_eff)
                            FROM sgrsatt c
                            WHERE c.sgrsatt_pidm = a.sgrsatt_pidm
                            and c.sgrsatt_term_code_eff <= fz_get_term()
                        )
                        and sgrsatt_atts_code = \'COMM\'
                    ) attr on attr.sgrsatt_pidm = szbuniq_pidm
                where szbuniq_pidm in (' . implode(', ',
                array_fill(0, count($pidms), '?')) . ')
                    and spriden_change_ind is null
            ';

        $this->readDataQueryParams = array_merge([$reviewType], $pidms);

        return true;
    }

    public function update($model)
    {

    }

    public function makeModel($record, $studentData, $ldapAttributes)
    {

        $model = [];

        $model['pidm'] = $record['szbuniq_pidm'];
        $model['bannerId'] = $record['szbuniq_banner_id'];
        $model['uniqueId'] = strtolower($record['szbuniq_unique_id']);
        // Use given anme from open LDAP as first name. RTAG takes care of managing given name based on preferred name set or not.
        $model['firstName'] = $ldapAttributes['givenName'];
        $model['lastName'] = $record['spriden_last_name'];
        $model['addDate'] = isset($record['add_date']) ? $record['add_date'] : '';
        $model['reviewDate'] = isset($record['review_date']) ? $record['review_date'] : '';
        $model['reviewStartDate'] = isset($record['requested_start_date']) ? $record['requested_start_date'] : '';
        $model['reviewStatus'] = isset($record['review_status']) ? $record['review_status'] : '';
        $model['reviewType'] = isset($record['review_type']) ? $record['review_type'] : '';
        $model['reviewTermCode'] = $this->currentTerm['termCode'] ?? '';
        $model['reviewTermDescription'] = $this->currentTerm['description'] ?? '';

        $model['primaryAffiliationCode'] = $ldapAttributes['primaryAffiliationCode'];
        $model['primaryAffiliationDescription'] = $ldapAttributes['primaryAffiliationDesc'];
        $model['primaryAffiliationType'] = '';

        $model['primaryMajor'] = $studentData['studentPrimaryMajor'] ?? '';
        $model['offCampusStudent'] = $studentData['offCampusStudent'] ?? '';

        // Decide primary affiliation type based on code
        // undergrad and grad are treated as student and faculty, staff as Employee
        switch ($model['primaryAffiliationCode']) {
            case 'und':
            case 'gra':
                $model['primaryAffiliationType'] = 'student';
                break;
            case 'fac':
            case 'sta' :
                $model['primaryAffiliationType'] = 'employee';
                break;
            default:
                $model['primaryAffiliationType'] = 'other';

        }

        $model['excludedActions'] = empty($record['excluded_actions']) ? [] : explode(',', $record['excluded_actions']);

        $model['requiredActions'] = [];

        if (in_array($model['reviewStatus'], ['requested', 'required'])) {
            $model['requiredActions'][] = 'emergencyContacts';
            $model['requiredActions'][] = 'phoneNumber';
            if (($model['primaryAffiliationCode'] === 'und') || ($model['primaryAffiliationCode'] === 'gra')) {
                $model['requiredActions'][] = 'missingPersonContacts';
                if ($this->requireSchoolAddressReviewForCampus($ldapAttributes['campusCode'])) {
                    $model['requiredActions'][] = 'schoolAddress';
                }

                if( $this->attendanceIsEnabledForOxford()
                    && $this->attendanceIsRequired($ldapAttributes['campusCode'], $studentData['studentLevelCode'] ?? '')) {
                    $model['requiredActions'][] = 'attendanceIntent';
                }
                if ($this->pledgeIsEnabled()) {
                    $model['requiredActions'][] = 'pledgeInfo';
                }
            }
        }

        $model['campusCode'] = $ldapAttributes['campusCode'];
        $model['classes'] = !empty($ldapAttributes['classes']) ? $ldapAttributes['classes'] : [];
        $model['classAttendance'] = !empty($studentData['classAttendance']) ? $studentData['classAttendance'] : [];
        $model['commuter'] = $record['sgrsatt_atts_code'] === 'COMM' ? true : false;
        $model['international'] = $studentData['international'] ?? '';
        $model['studentLevelCode'] = $studentData['studentLevelCode'] ?? '';
        $model['studentTypeCode'] = $studentData['studentTypeCode'] ?? '';
        $model['studentCampusCode'] = $studentData['studentCampusCode'] ?? '';

        return $model;
    }



    private function requireSchoolAddressReviewForCampus(string $campusCode): bool
    {
        return in_array($campusCode, ['oxf', 'ham', 'mid']);
    }

    private function pledgeIsEnabled(): bool
    {
        return !empty($this->config['pledgeMenu.showMenuItem']) && $this->config['pledgeMenu.showMenuItem'] === 'true';
    }

    private function attendanceIsEnabledForOxford(): bool
    {
        return !empty($this->config['attendanceIntention.showMenuItem.oxf']) && $this->config['attendanceIntention.showMenuItem.oxf'] === 'true';
    }

    private function attendanceIsRequired(string $campusCode, string $studentLevelCode): bool
    {
        //Oxford Undergrad Students are required to submit attendance intentions
        return $campusCode == 'oxf' && $studentLevelCode == 'UG';
    }

    /**
     * @param $uid
     */
    public function getLDAPAttributes($uid)
    {
        $ldapAttributes = [
            'primaryAffiliationCode' => '',
            'primaryAffiliationDesc' => '',
            'campusCode' => '',
            'campus' => '',
            'givenName' => '',
            'classes' => []
        ];

        $ldap = $this->ldapFactory->getHandle($this->ldapDataSource);

        // Search for ldap entry for the given uid
        $ldapResult = $ldap->search("uid=" . $uid, [
            'muohioeduprimaryaffiliationcode',
            'muohioeduprimaryaffiliation',
            'muohioeduPrimaryLocationCode',
            'muohioeduPrimaryLocation',
            'givenName',
            'muohioeduCurrentCourseCRN'
        ]);

        // if there is an entry exist, get the primary affiliation code and description
        // ex: 'gra' for grad student, 'und' for undergrad student
        if ($ldapResult === 1) {
            $entry = $ldap->next_entry();
            $ldapAttributes['primaryAffiliationCode'] = $entry->attribute_value('muohioeduprimaryaffiliationcode');
            $ldapAttributes['primaryAffiliationDesc'] = $entry->attribute_value('muohioeduprimaryaffiliation');
            $ldapAttributes['campusCode'] = $entry->attribute_value('muohioeduPrimaryLocationCode');
            $ldapAttributes['campus'] = $entry->attribute_value('muohioeduPrimaryLocation');
            $ldapAttributes['givenName'] = $entry->attribute_value('givenName');
            $ldapAttributes['classes'] = $entry->attribute_values('muohioeduCurrentCourseCRN');
        }

        return $ldapAttributes;

    }

    private function getReviewType($pidm): string
    {
        if( $this->employee->isEmployee($pidm) ) {
            return 'employee';
        }
        return 'student';
    }

    private function getRequestStartDate(string $reviewType): string
    {
        if( !empty($this->currentTerm['termStartDate']) ) {
            if( $reviewType === 'employee') {
                $daysBeforeTermStartRequested = $this->config['daysBeforeTermStartRequestedEmployees'] ?? -7;
            } else {
                $daysBeforeTermStartRequested = $this->config['daysBeforeTermStartRequested'] ?? 7;
            }
            return  date('Y-m-d', strtotime($this->currentTerm['termStartDate'] . ' -' . $daysBeforeTermStartRequested . ' days'));
        }
        return '';
    }

}





