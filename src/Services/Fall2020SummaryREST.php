<?php

namespace MiamiOH\RestngContactService\Services;

class Fall2020SummaryREST extends \MiamiOH\RESTng\Service
{

    /** @var ReviewPeriod */
    private $reviewPeriod;

    /** @var Fall2020Summary */
    private $reviewSummary;

    public function setReviewPeriod($reviewPeriod)
    {
        $this->reviewPeriod = $reviewPeriod;
    }

    public function setFall2020Summary($reviewSummary)
    {
        $this->reviewSummary = $reviewSummary;
    }


    public function getFall2020Data()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        $periodRecords = $this->reviewPeriod->getReviewPeriod($options['type'], false);

        if (empty($periodRecords['state'])) {
            $response->setStatus(404);
            return $response;
        }

        $summaryRecords = $this->reviewSummary->getSummaryDetails($periodRecords);

        $response->setPayload($summaryRecords);

        return $response;
    }
}