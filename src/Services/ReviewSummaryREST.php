<?php

namespace MiamiOH\RestngContactService\Services;

class ReviewSummaryREST extends \MiamiOH\RESTng\Service
{

    /** @var ReviewStatus */
    private $reviewStatus;

    /** @var ReviewPeriod */
    private $reviewPeriod;

    /** @var ReviewSummary */
    private $reviewSummary;

    /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
    private $bannerUtil;

    public function setReviewStatus($reviewStatus)
    {
        $this->reviewStatus = $reviewStatus;
    }

    public function setReviewPeriod($reviewPeriod)
    {
        $this->reviewPeriod = $reviewPeriod;
    }

    public function setReviewSummary($reviewSummary)
    {
        $this->reviewSummary = $reviewSummary;
    }


    public function setBannerUtil($bannerUtil)
    {
        $this->bannerUtil = $bannerUtil;
    }

    public function getReviewSummaryList()
    {

        $this->log->debug('Start the review summary service');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        $periodRecords = $this->reviewPeriod->getReviewPeriod($options['type'],
            false);

        $summaryRecords = $this->reviewSummary->getSummary($periodRecords, $options);

        $response->setPayload($summaryRecords);

        return $response;
    }

    public function getReviewSummaryDetails()
    {

        $this->log->debug('Start the review summary service');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        $periodRecords = $this->reviewPeriod->getReviewPeriod($options['type'],
            false);

        $summaryRecords = $this->reviewSummary->getSummaryDetails($periodRecords,
            $options);

        $response->setPayload($summaryRecords);

        return $response;
    }
}