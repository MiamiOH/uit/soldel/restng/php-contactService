<?php

namespace MiamiOH\RestngContactService\Services;

use Exception;
use MiamiOH\RESTng\Service\Extension\BannerIdNotFound;
use MiamiOH\RESTng\Service\Extension\BannerUtil;
use MiamiOH\RestngContactService\Exceptions\ReAddContactException;

class ContactREST extends \MiamiOH\RESTng\Service
{

    /**
     * @var Contact
     */
    private $contact;

    /**
     * @var BannerUtil
     */
    private $bannerUtil;

    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    public function setBannerUtil($bannerUtil)
    {
        $this->bannerUtil = $bannerUtil;
    }

    public function getContact()
    {
        $this->log->debug('Start the contact service.');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

            $pidm = $bannerId->getPidm();

            if (isset($options['relationCode']) && $options['relationCode']) {
                $this->contact->filterRelationType($options['relationCode']);
            }

            if (isset($options['type']) && $options['type']) {
                $this->contact->filterContactType($options['type']);
            }

            $payload = $this->contact->read($pidm);

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);

        } catch (BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } catch (Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
        }

        return $response;
    }


    public function getMissingPersonContactList()
    {
        $this->log->debug('Start the missing person contact service.');

        $request = $this->getRequest();
        $response = $this->getResponse();

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

            $pidm = $bannerId->getPidm();

            $this->contact->filterContactType('missingPerson');

            $payload = $this->contact->read($pidm);

            /*
             * If the missing person 'K' contact type does not return any contacts, assume
             * that any emergency contacts may be used for missing person.
            */
            if (!$payload) {
                $this->contact->filterContactType('emergency');

                $payload = $this->contact->read($pidm);
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);

        } catch (BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
        }

        return $response;
    }

    public function createContactCollection()
    {

        $this->log->debug('Start the contact service.');

        $request = $this->getRequest();
        $response = $this->getResponse();

        $contacts = $request->getData();

        $options = $request->getOptions();

        $type = isset($options['type']) ? $options['type'] : 'emergency';

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

            $pidm = $bannerId->getPidm();
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        if (isset($pidm)) {
            $payload = [];

            for ($i = 0; $i < count($contacts); $i++) {
                $createContactResult = [
                    'status' => \MiamiOH\RESTng\App::API_CREATED,
                    'message' => '',
                    'contact' => [],
                ];

                if (!isset($contacts[$i]['pidm'])) {
                    $contacts[$i]['pidm'] = $pidm;
                }

                if ($contacts[$i]['pidm'] !== $pidm) {
                    $this->log->info('Resource ID does not match PIDM in contact model');
                    $createContactResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                    $createContactResult['message'] = 'Resource ID does not match PIDM in contact model';
                } else {
                    try {
                        $createContactResult['contact'] = $this->contact->create($contacts[$i],
                            $type);
                    } catch (\MiamiOH\RESTng\Exception\BadRequest $e) {
                        $this->log->info($e->getMessage());
                        $createContactResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                        $createContactResult['message'] = $e->getMessage();
                    } catch (\Exception $e) {
                        $this->log->error($e->getMessage());
                        $createContactResult['status'] = \MiamiOH\RESTng\App::API_FAILED;
                        $createContactResult['message'] = $e->getMessage();
                    }
                }

                $payload[] = $createContactResult;
            }

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);
        }

        return $response;
    }

    public function updateContactCollection()
    {
        //log
        $this->log->debug('Start the contact service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $contacts = $request->getData();


        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));
            $pidm = $bannerId->getPidm();


        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        if (isset($pidm)) {
            $payload = [];


            for ($i = 0; $i < count($contacts); $i++) {
                $updateContactResult = [
                    'status' => \MiamiOH\RESTng\App::API_OK,
                    'message' => '',
                ];

                if (!isset($contacts[$i]['pidm'])) {
                    $contacts[$i]['pidm'] = $pidm;
                }

                if ($contacts[$i]['pidm'] !== $pidm) {
                    $this->log->info('Resource ID does not match PIDM in contact model');
                    $updateContactResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                    $updateContactResult['message'] = 'Resource ID does not match PIDM in contact model';
                } else {
                    try {
                        $this->contact->update($contacts[$i]);
                    } catch (\MiamiOH\RESTng\Exception\BadRequest $e) {
                        $this->log->info($e->getMessage());
                        $updateContactResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                        $updateContactResult['message'] = $e->getMessage();
                    } catch (\Exception $e) {
                        $this->log->error($e->getMessage());
                        $updateContactResult['status'] = \MiamiOH\RESTng\App::API_FAILED;
                        $updateContactResult['message'] = $e->getMessage();
                    }
                }

                $payload[] = $updateContactResult;

            }

            $response->setPayload($payload);
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        }

        return $response;
    }


    public function deleteContactCollection()
    {
        //log
        $this->log->debug('Start the contact service.');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $contacts = $request->getData();

        $purgeInvalidContacts = ($options['purgeInvalidContacts'] ?? '') === 'enabled';

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));
            $pidm = $bannerId->getPidm();
        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        if (isset($pidm)) {

            $payload = [];

            for ($i = 0; $i < count($contacts); $i++) {
                $deleteContactResult = [
                    'status' => \MiamiOH\RESTng\App::API_OK,
                    'message' => '',
                ];

                if (!isset($contacts[$i]['pidm'])) {
                    $contacts[$i]['pidm'] = $pidm;
                }

                if ($contacts[$i]['pidm'] !== $pidm) {
                    $deleteContactResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                    $deleteContactResult['message'] = 'Resource ID does not match PIDM in contact model';
                } else {
                    try {
                        $this->contact->delete($contacts[$i], $purgeInvalidContacts);
                    } catch (\MiamiOH\RESTng\Exception\BadRequest $e) {
                        $this->log->info($e->getMessage());
                        $deleteContactResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                        $deleteContactResult['message'] = $e->getMessage();
                    } catch (ReAddContactException $e) {
                        $this->log->info($e->getMessage());
                        $deleteContactResult['status'] = \MiamiOH\RESTng\App::API_BADREQUEST;
                        $deleteContactResult['message'] = 'Bad contact: ' . $e->getMessage();
                    } catch (\Exception $e) {
                        $this->log->error($e->getMessage());
                        $deleteContactResult['status'] = \MiamiOH\RESTng\App::API_FAILED;
                        $deleteContactResult['message'] = $e->getMessage();
                    }
                }

                $payload[] = $deleteContactResult;
            }

            $response->setPayload($payload);
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        }

        return $response;
    }

    public function splitPhone($contactData)
    {

        $report = [];

        foreach ($contactData as $oneContact) {


            // $phoneNo= str_split($onePhone['phoneNumber'],3);
            $phoneNo[0] = substr($oneContact['phoneNumber'], 2, 3);
            $phoneNo[1] = substr($oneContact['phoneNumber'], 5);
            $areaCode = $phoneNo[0];
            $remainingNumber = $phoneNo[1];


            $internal = array(
                'relation' => ($oneContact['relation']),
                'pidm' => ($oneContact['pidm']),
                'priority' => strtolower($oneContact['priority']),
                'firstName' => strtolower($oneContact['firstName']),
                'lastName' => strtolower($oneContact['lastName']),
                'middleName' => strtolower($oneContact['middleName']),
                'streetAddressLine1' => strtolower($oneContact['streetAddressLine1']),
                'streetAddressLine2' => strtolower($oneContact['streetAddressLine2']),
                'city' => strtolower($oneContact['city']),
                'state' => strtolower($oneContact['state']),
                'postalCode' => strtolower($oneContact['postalCode']),
                'areaCode' => $areaCode,
                'phoneNumber' => $remainingNumber,
                'addressType' => strtolower($oneContact['addressType']),
            );
            $report[] = $internal;

        }

        return $report;
    }
}
