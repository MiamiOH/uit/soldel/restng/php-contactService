<?php

namespace MiamiOH\RestngContactService\Services;

class Relation extends \MiamiOH\RESTng\Service
{

    private $datasource_name = 'MUWS_GEN_PROD';

    private $dbh;

    private $filterType = '';

    /*
    * Helper function to Set the Database source to be Used
    *
    * Inputs:
    * database: Name of database source to use.
    */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }


    /**
     * To get relation type and relation description from relation table(stvrelt)
     *
     * @return mixed
     */
    public function getList()
    {

        //parse the input pidm list
        $queryString = '
            SELECT STVRELT_CODE,STVRELT_DESC 
              FROM STVRELT
              where 1 = 1
            ';

        if ($this->filterType) {
            switch ($this->filterType) {
                case 'ec':
                    // Remove 'missing person' and 'undefined'
                    $queryString .= '
                        and stvrelt_code not in (\'K\', \'W\')
                    ';
            }
        }

        $records = $this->dbh->queryall_array($queryString);

        for ($i = 0; $i < count($records); $i++) {
            $records[$i] = $this->makeModelFromRecord($records[$i]);
        }

        $this->clearFilters();

        return $records;

    }

    public function filterType($type)
    {
        $this->filterType = $type;

        return $this;
    }

    public function clearFilters()
    {
        $this->filterType = '';
    }

    /**
     * @param $record
     *
     * @return array
     */
    public function makeModelFromRecord($record)
    {
        $model = [];

        $model['relationCode'] = $record['stvrelt_code'];
        $model['description'] = $record['stvrelt_desc'];

        return $model;
    }


}

