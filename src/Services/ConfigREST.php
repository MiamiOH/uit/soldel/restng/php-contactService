<?php

namespace MiamiOH\RestngContactService\Services;

use MiamiOH\RESTng\Exception\BadRequest;

class ConfigREST extends \MiamiOH\RESTng\Service
{

    /** @var  \MiamiOH\RESTng\Util\Configuration $configuration */
    private $configuration;

    private $applicationName = 'PersonContactInfo';
    private $categoryList = [
        'uiString' => 'Contact Info Review',
        'test' => 'Test',
    ];

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    public function getConfiguration()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        if (!isset($options['category'])) {
            throw new BadRequest('Missing required configuration category');
        }

        if (!isset($this->categoryList[$options['category']])) {
            throw new BadRequest('Invalid configuration category');
        }

        $payload = $this->configuration->getConfiguration($this->applicationName,
            $this->categoryList[$options['category']]);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

}