<?php

namespace MiamiOH\RestngContactService\Services;

class ProfileREST extends \MiamiOH\RESTng\Service
{

    /** @var Profile */
    private $profile;

    /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
    private $bannerUtil;

    public function setProfile($profile)
    {
        $this->profile = $profile;
    }

    public function setBannerUtil($bannerUtil)
    {
        $this->bannerUtil = $bannerUtil;
    }

    public function getProfile()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

            $pidm = $bannerId->getPidm();
            $uid = $bannerId->getUniqueId();

            $payload = $this->profile->read($pidm, $uid);

            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($payload);

        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        return $response;
    }

    public function updateProfileStatus()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $model = $request->getData();

        try {
            $bannerId = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));

            $pidm = $bannerId->getPidm();

            if (!isset($model['pidm'])) {
                $model['pidm'] = $pidm;
            }

            if ($model['pidm'] !== $pidm) {
                $this->log->info('Resource ID does not match PIDM in phone model');
                throw new \MiamiOH\RESTng\Exception\BadRequest('Resource ID does not match PIDM in profile model');
            }

            $result = $this->profile->update($model);

            if ($result) {
                $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            }

        } catch (\MiamiOH\RESTng\Service\Extension\BannerIdNotFound $e) {
            $this->log->info($e->getMessage());
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        return $response;
    }

    public function getProfileList()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        if (!isset($options['pidm'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Missing required option pidm for profile list');
        }

        $reviewType = $options['reviewType'] ?? 'student';

        $payload = $this->profile->readList($options['pidm'], $reviewType);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

}





