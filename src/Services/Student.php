<?php

namespace MiamiOH\RestngContactService\Services;


use MiamiOH\RestngContactService\Exceptions\StudentRecordNotFoundException;

class Student extends \MiamiOH\RESTng\Service
{

    private $dataSourceName = 'MUWS_GEN_PROD';

    private $database;
    private $dbh;

    private $termCode = '';
    private $reviewState = '';

    private $studentDataQuerySql = '';
    private $studentDataQueryParams = [];

    private $config = [];

    public function setConfiguration($configuration)
    {
        $this->config =  $configuration->getConfiguration('PersonContactInfo', 'Internal Config');
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setTermCode($termCode)
    {
        $this->termCode = $termCode;
    }

    public function setReviewState($reviewState)
    {
        $this->reviewState = $reviewState;
    }

    private function getDbh()
    {
        if (null === $this->dbh) {
            $this->dbh = $this->database->getHandle($this->dataSourceName);
        }

        return $this->dbh;
    }

    public function getStudentData($pidm)
    {
        $studentData = ['pidm' => $pidm];
        try {
            $studentData = array_merge($studentData, $this->getStudentRecord($pidm));
        } catch (StudentRecordNotFoundException $e) {
            // No student record was found, and that's ok
        }

        return $this->makeDataModel($studentData);
    }

    private function getStudentRecord(string $pidm): array
    {
        $dbh = $this->getDbh();

        $query = '
            select baninst1.fz_oxfd_offcampus(sgbstdn_pidm,
                        ' . ($this->termCode ? '\'' . $this->termCode . '\'' : 'fz_get_term()') . ') as off_campus_student,
                    gzv_fed_state_ethn_race.nonres_alien as international,
                    sgbstdn_levl_code as student_level_code,
                    sgbstdn_styp_code as student_type_code,
                    sgbstdn_camp_code as student_campus_code,
                    sgbstdn_majr_code_1 as student_primary_major
              from sgbstdn a inner join gzv_fed_state_ethn_race on gzv_fed_state_ethn_race.pidm = sgbstdn_pidm
              where sgbstdn_pidm = ?
                and sgbstdn_term_code_eff =  
                        (select max(sgbstdn_term_code_eff) from sgbstdn b
                         where b.sgbstdn_pidm=a.sgbstdn_pidm
                           and b.sgbstdn_term_code_eff<='. ($this->termCode ? '\'' . $this->termCode . '\'' : 'fz_get_term()')
                        .')';

        $studentData = $dbh->queryfirstrow_assoc($query, $pidm);

        if ($studentData === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            throw new StudentRecordNotFoundException(sprintf('No student record found for pidm %s', $pidm));
        }

        /** The following logic checks whether a student is a 'Study Abroad', if 'Yes' then we're not allowing
         * him as oxfd_offcampus student. Hence, he will not need to provide LO address. **/
        if ($studentData['off_campus_student'] === 'Y') {
            $studyAbroadQuerySql = '
              select count(*) as study_abroad
                from sfrstcr, ssbsect
                where sfrstcr_term_code = ?
                and sfrstcr_pidm = ?
                and ssbsect_crn = sfrstcr_crn
                and ssbsect_term_code = sfrstcr_term_code
                and ssbsect_subj_code = \'STY\'';

            $studyAbroadStudent = $dbh->queryfirstcolumn($studyAbroadQuerySql, $this->termCode, $pidm);
            if ($studyAbroadStudent) {
                $studentData['off_campus_student'] = 'N';
            }
        }

        $studentData['classAttendance'] = $this->getClassesByType($pidm, $this->termCode);
        return $studentData;
    }

    public function getStudentsForReview()
    {
        $studentData = [];

        // Don't bother querying if we don't have a state and term
        if ($this->termCode && $this->reviewState) {
            $dbh = $this->database->getHandle($this->dataSourceName);

            $this->makeStudentReviewQuery();

            $studentData = $dbh->queryall_array($this->studentDataQuerySql,
                $this->studentDataQueryParams);

            if ($studentData === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
                $studentData = [];
            } else {
                for ($i = 0; $i < count($studentData); $i++) {
                    $studentData[$i] = $this->makeReviewModel($studentData[$i]);
                }
            }
        } else {
            $studentData = [];
        }

        return $studentData;
    }

    private function makeStudentDataQuery($pidms)
    {
        if (is_scalar($pidms)) {
            $pidms = [$pidms];
        }

        $this->studentDataQuerySql = '
            select szbuniq_pidm,
                    baninst1.fz_oxfd_offcampus(szbuniq_pidm,
                        ' . ($this->termCode ? '\'' . $this->termCode . '\'' : 'fz_get_term()') . ') as off_campus_student,
                    gzv_fed_state_ethn_race.nonres_alien as international,
                    sgbstdn_levl_code as student_level_code,
                    sgbstdn_styp_code as student_type_code,
                    sgbstdn_camp_code as student_campus_code
              from szbuniq inner join gzv_fed_state_ethn_race on gzv_fed_state_ethn_race.pidm = szbuniq_pidm
                left outer join sgbstdn on sgbstdn_pidm = szbuniq_pidm
              where szbuniq_pidm in (' . implode(', ', array_fill(0, count($pidms), '?')) . ')
                and sgbstdn_term_code_eff = ' . ($this->termCode ? '\'' . $this->termCode . '\'' : 'fz_get_term()')
            ;

        $this->studentDataQueryParams = $pidms;

        return true;
    }

    private function makeStudentReviewQuery()
    {
        $this->studentDataQuerySql = '';
        $this->studentDataQueryParams = [];

        //select all students that are taking classes for current term and exclude Miami employees
        $this->studentDataQuerySql = '
            (
            select sgbstdn_pidm, szbuniq_unique_id,
                    \'' . $this->reviewState . '\' as review_status
            From sgbstdn a, szbuniq
            where sgbstdn_term_code_eff =
                    (select max(sgbstdn_term_code_eff) from sgbstdn b
                                 where b.sgbstdn_pidm=a.sgbstdn_pidm
                                and b.sgbstdn_term_code_eff<='. ($this->termCode ? '\'' . $this->termCode . '\'' : 'fz_get_term()')
                                .')
            AND sgbstdn_pidm = szbuniq_pidm
            AND sgbstdn_stst_code in (\'AS\',\'NC\')
            AND f_registered_this_term(sgbstdn_pidm, ' . ($this->termCode ? '\'' . $this->termCode . '\'' : 'fz_get_term()') .') = \'Y\'
            AND NOT EXISTS(
                SELECT 1
                FROM pebempl
                WHERE pebempl_empl_status=\'A\'
                AND pebempl_pidm = sgbstdn_pidm
                AND pebempl_egrp_code not in(\'TA\',\'STU\',\'GA\')
                AND EXISTS(
                    select 1 from nbrbjob
                    where nbrbjob_contract_type=\'P\'
                    and (nbrbjob_end_date > sysdate or nbrbjob_end_date is null)
                )          
             )
            )';

        return true;
    }

    private function makeDataModel($record)
    {
        $offCampusIndicator = $record['off_campus_student'] ?? '';
        $internationalIndicator = $record['international'] ?? '';

        $model = [
            'pidm' => $record['pidm'],
            'offCampusStudent' => $offCampusIndicator === 'Y' ? true : false,
            'international' => $internationalIndicator === 'Y' ? true : false,
            'studentLevelCode' => $record['student_level_code'] ?? '',
            'studentTypeCode' => $record['student_type_code'] ?? '',
            'studentCampusCode' => $record['student_campus_code'] ?? '',
            'studentPrimaryMajor' => $record['student_primary_major'] ?? '',
            'classAttendance' => $record['classAttendance'] ?? []
        ];

        return $model;
    }

    private function makeReviewModel($record)
    {
        $model = [
            'pidm' => $record['sgbstdn_pidm'],
            'uniqueId' => strtolower($record['szbuniq_unique_id']),
            'reviewStatus' => $record['review_status'],
        ];

        return $model;
    }

    private function getClassesByType($pidm, $termCode)
    {
        $classFilter = $this->config['attendanceIntention.classFilter'] ?? "
            AND sfrstcr_rsts_code in('RE', 'RW', 'AU', 'RN')
            AND ssbsect_crse_numb NOT LIKE '%77%'
            AND ssbsect_crse_numb NOT LIKE '340%'
            AND SSBSECT_SUBJ_CODE <> 'STY'
            AND SSBSECT_SUBJ_CODE||ssbsect_crse_numb <> 'MBI440'";

        $onlineOnlyCodes = $this->config['attendanceIntention.onlineOnlyCodes'] ?? '["ONLA", "ONLS"]';
        $onlineOnlyCodes = json_decode($onlineOnlyCodes, true);

        $sql = "
        select  
            sfrstcr_crn as CRN,
            ssbsect_subj_code||ssbsect_crse_numb as course_name, 
            CASE WHEN EXISTS ( 
                select ssrattr_crn from  ssrattr
                where sfrstcr_term_code = ssrattr_term_code
                and ssrattr_crn = sfrstcr_crn
                and ssrattr_attr_code in ('" . implode("','", $onlineOnlyCodes) ."')
            ) THEN 1 ELSE 0 END AS online_only
        from sfrstcr
        INNER JOIN 
            ssbsect ON ssbsect_term_code = sfrstcr_term_code AND ssbsect_crn = sfrstcr_crn
        where sfrstcr_term_code = ?
            $classFilter
            AND sfrstcr_pidm = ?";


        $dbh = $this->database->getHandle($this->dataSourceName);


        $records = $dbh->queryall_array($sql, $termCode, $pidm);

        $recordAsModel = [];

        foreach($records as $record) {
            $recordAsModel[] = [
                'CRN' => $record['crn'],
                'courseName' => $record['course_name'],
                'onlineOnly' => $record['online_only']
            ];
        }
        return $recordAsModel;
    }

}





