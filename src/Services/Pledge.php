<?php


namespace MiamiOH\RestngContactService\Services;


use MiamiOH\RESTng\Service;

class Pledge extends Service
{
    private $dataSourceName = 'MUWS_GEN_PROD';
    private $database = '';
    private $queryParams = [];
    private $querySql;


    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function getPledgeByUniqueId()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        $uniqueId = $request->getResourceParam('uniqueId');
        $minActionDate = isset($options['minActionDate']) ? $options['minActionDate'] : '';

        $dbh = $this->database->getHandle($this->dataSourceName);
        $this->buildSQLForQuery($uniqueId, $minActionDate);
        $records = $dbh->queryall_array($this->querySql,
            $this->queryParams);

        $payload = [];
        for ($i = 0; $i < count($records); $i++) {
            $payload[$i] = $this->makeModel($records[$i]);
        }
        $response->setPayload($payload);

        return $response;
    }

    private function buildSQLForQuery($uniqueID, $minActionDate)
    {
        $this->querySql = 'select id, uniqueid,type,response,
                        to_char(response_time, \'YYYY-MM-DD HH24:MI:SS\') as response_time
                        from safmgr.pledge where uniqueId = ?';
        $this->queryParams = [$uniqueID];
        if(!empty($minActionDate)) {
            $this->querySql .= ' and response_time >= to_date(?, \'YYYYMMDD\')';
            $this->queryParams[] = $minActionDate;
        }
        return true;
    }

    private function makeModel($record)
    {
        $model = [];
        $model['id'] = $record['id'];
        $model['uniqueId'] = $record['uniqueid'];
        $model['type'] = $record['type'];
        $model['response'] = $record['response'];
        $model['responseTime'] = $record['response_time'];
        return $model;
    }

    public function createPledgeResponse()
    {
        $request = $this->getRequest();
        $attendanceIntention = $request->getData();
        $record = $this->createRecord($attendanceIntention);
        $returnData = $this->makeModel($record);
        $response = $this->getResponse();
        $response->setPayload($returnData);
        return $response;
    }

    private function createRecord(array $record)
    {

        $dbh = $this->database->getHandle($this->dataSourceName);

        $dbh->perform('
        insert into safmgr.pledge
            (uniqueid, type, response, response_time)
        values
          (?, ?, ?, to_date(?, \'YYYY-MM-DD HH24:MI:SS\'))'
            , $record['uniqueId'], $record['type'], $record['response'], $record['responseTime'] );

        $querySql = 'select id, uniqueid, type, response,
                    to_char(response_time, \'YYYY-MM-DD HH24:MI:SS\') as response_time
                        from safmgr.pledge
                        where uniqueid = ?
                          and type = ?
                          and response = ?
                          and response_time = to_date(?, \'YYYY-MM-DD HH24:MI:SS\')';

        return $dbh->queryfirstrow_assoc($querySql, [$record['uniqueId'], $record['type'], $record['response'],$record['responseTime']]);

    }

}
