<?php

namespace MiamiOH\RestngContactService\Services;

class ReviewPeriodREST extends \MiamiOH\RESTng\Service
{

    /** @var ReviewPeriod */
    private $reviewPeriod;

    /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
    private $bannerUtil;

    public function setReviewPeriod($reviewPeriod)
    {
        $this->reviewPeriod = $reviewPeriod;
    }

    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil $bannerUtil */
        $this->bannerUtil = $bannerUtil;
    }

    public function getCurrentReviewPeriod()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        if (!isset($options['type'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Missing required type option');
        }

        if (!in_array($options['type'], ['student','employee'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Invalid type for reviewPeriod');
        }

        $records = $this->reviewPeriod->getReviewPeriod($options['type']);

        $response->setPayload($records);

        return $response;
    }

}