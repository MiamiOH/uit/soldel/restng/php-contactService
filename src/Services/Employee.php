<?php

namespace MiamiOH\RestngContactService\Services;


class Employee extends \MiamiOH\RESTng\Service
{
    private $applicationName = 'PersonContactInfo';
    private $categoryname = 'Review State';
    private $dataSourceName = 'MUWS_GEN_PROD';

    private $database;
    private $dbh;

    private $termCode = '';
    private $reviewState = '';

    private $employeeDataQuerySql = '';
    private $employeeDataQueryParams = [];

    /** @var  \MiamiOH\RESTng\Util\Configuration $configuration */
    private $configuration;

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setTermCode($termCode)
    {
        $this->termCode = $termCode;
    }

    public function setReviewState($reviewState)
    {
        $this->reviewState = $reviewState;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    private function getDbh()
    {
        if (null === $this->dbh) {
            $this->dbh = $this->database->getHandle($this->dataSourceName);
        }

        return $this->dbh;
    }

    public function getEmployeesForReview()
    {
        // Don't bother querying if we don't have a state and term
        if ($this->termCode && $this->reviewState) {
            $dbh = $this->database->getHandle($this->dataSourceName);

            $this->makeEmployeeReviewQuery();

            $employeeData = $dbh->queryall_array($this->employeeDataQuerySql,
                $this->employeeDataQueryParams);

            if ($employeeData === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
                $employeeData = [];
            } else {
                for ($i = 0; $i < count($employeeData); $i++) {
                    $employeeData[$i] = $this->makeReviewModel($employeeData[$i]);
                }
            }
        } else {
            $employeeData = [];
        }

        return $employeeData;
    }

    public function isEmployee($pidm)
    {
        $query =
            '
            SELECT \'Y\' 
             FROM pebempl
            WHERE pebempl_empl_status=\'A\'
            AND pebempl_pidm = ?
            AND pebempl_egrp_code not in(\'TA\',\'STU\',\'GA\')
            AND EXISTS(
                select 1 from nbrbjob
                where nbrbjob_contract_type=\'P\'
                and nbrbjob_pidm = pebempl_pidm
                and (nbrbjob_end_date > sysdate or nbrbjob_end_date is null)
            )
            ';
        $dbh = $this->database->getHandle($this->dataSourceName);
        $result = $dbh->queryfirstrow_assoc($query, $pidm);

        if ($result !== \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            return true;
        }
        return false;
    }

    private function makeEmployeeReviewQuery()
    {
        $this->employeeDataQuerySql = '';
        $this->employeeDataQueryParams = [];

        $config = $this->configuration->getConfiguration($this->applicationName,
            $this->categoryname);
        $cutOffMonthAndDate = $config['newEmployeeCutOffDate'];
        if( empty($cutOffMonthAndDate)) {
            //set to default value so we can move on
            $cutOffMonthAndDate = '0901';
        }
        //exclude employees that start after a specific date
        $newEmployeeCutOffDate = date('Y'). $cutOffMonthAndDate;

        $this->employeeDataQuerySql = '
            SELECT pebempl_pidm, szbuniq_unique_id, ?  AS review_status
             FROM pebempl, szbuniq
            WHERE pebempl_empl_status=\'A\'
            AND pebempl_pidm = szbuniq_pidm
            AND pebempl_egrp_code not in(\'TA\',\'STU\',\'GA\')
            AND pebempl_current_hire_date < to_date(?, \'YYYYMMDD\')
            AND EXISTS(
                select 1 from nbrbjob
                where nbrbjob_contract_type=\'P\'
                and nbrbjob_pidm = pebempl_pidm
                and (nbrbjob_end_date > sysdate or nbrbjob_end_date is null)
            )
            ';
        $this->employeeDataQueryParams[] = $this->reviewState;
        $this->employeeDataQueryParams[] = $newEmployeeCutOffDate;

        return true;
    }

    private function makeReviewModel($record)
    {
        $model = [
            'pidm' => $record['pebempl_pidm'],
            'uniqueId' => strtolower($record['szbuniq_unique_id']),
            'reviewStatus' => $record['review_status'],
        ];

        return $model;
    }

}





