<?php

namespace MiamiOH\RestngContactService\Services;

class Term
{
    private $applicationName = 'PersonContactInfo';
    private $categoryname = 'Review State';
    private $datasource_name = 'MUWS_GEN_PROD';

    private $dbh;

    /** @var  \MiamiOH\RESTng\Util\Configuration $configuration */
    private $configuration;

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

     public function getCurrentReviewTermForStudents()
     {
         $config = $this->configuration->getConfiguration($this->applicationName,
             $this->categoryname);

         $daysBefore = isset($config['daysBeforeTermStartRequested']) ? $config['daysBeforeTermStartRequested'] : 7;
         $daysBeforeTermEnds = isset($config['daysBeforeTermEndToEndReview']) ? $config['daysBeforeTermEndToEndReview'] : 0;

         $term = $this->dbh->queryfirstrow_assoc('
            select stvterm_code, stvterm_desc, to_char(stvterm_start_date, \'YYYY-MM-DD\') as stvterm_start_date
              from stvterm
              where stvterm_code = (
                        select min(stvterm_code) 
                        from stvterm a
                        where (a.stvterm_code like \'%10\' or a.stvterm_code like \'%20\') -- term is fall or spring
                          and a.stvterm_start_date - ? <= sysdate -- we are within N days of the term start
                          and a.stvterm_end_date - ? >= sysdate -- the review period ends x days before the term ends
                          )
            ', [$daysBefore, $daysBeforeTermEnds]);

         if ($term === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
             $term = [];
         }

         return $this->makeModel($term);
     }

    public function getCurrentReviewTermForEmployees()
    {
        $config = $this->configuration->getConfiguration($this->applicationName,
            $this->categoryname);

        $daysBefore = isset($config['daysBeforeTermStartRequestedEmployees']) ? $config['daysBeforeTermStartRequestedEmployees'] : -7;

        $term = $this->dbh->queryfirstrow_assoc('
            select stvterm_code, stvterm_desc, to_char(stvterm_start_date, \'YYYY-MM-DD\') as stvterm_start_date
              from stvterm
              where stvterm_code = (
                        select min(stvterm_code) 
                        from stvterm a
                        where (a.stvterm_code like \'%10\') -- term is fall
                          and a.stvterm_end_date >= sysdate -- the term has not ended
                          and a.stvterm_start_date - ? <= sysdate -- we are within N days of the term start
                          )
            ', [$daysBefore]);

        if ($term === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $term = [];
        }

        return $this->makeModel($term);
     }

    public function makeModel($record)
    {
        $model = [];

        $model['termCode'] = isset($record['stvterm_code']) ? $record['stvterm_code'] : '';
        $model['description'] = isset($record['stvterm_desc']) ? $record['stvterm_desc'] : '';
        $model['termStartDate'] = isset($record['stvterm_start_date']) ? $record['stvterm_start_date'] : '';

        return $model;
    }

}