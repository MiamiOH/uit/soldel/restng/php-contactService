<?php


namespace MiamiOH\RestngContactService\Services;


use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Service;

class AttendanceIntention extends Service
{
    private $dataSourceName = 'MUWS_GEN_PROD';
    private $database = '';
    private $queryParams = [];
    private $querySql;
    /**
     * @var DBH
     */
    private $dbh;

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dataSourceName);

        $this->database = $database;
    }

    public function getIntention()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();

        $uniqueId = $request->getResourceParam('uniqueId');
        $minActionDate = isset($options['minActionDate']) ? $options['minActionDate'] : '';

        $this->buildSQLForQuery($uniqueId, $minActionDate);
        $records = $this->dbh->queryall_array($this->querySql,
            $this->queryParams);

        $payload = [];
        for ($i = 0; $i < count($records); $i++) {
            $payload[$i] = $this->makeModel($records[$i]);
        }

        $response->setPayload($payload);

        return $response;
    }

    public function createAttendanceIntention()
    {
        $returnData = [];
        $request = $this->getRequest();
        $attendanceIntention = $request->getData();
        $record = $this->createRecord($attendanceIntention);
        $returnData = $this->makeModel($record);
        $response = $this->getResponse();

        $response->setStatus(App::API_CREATED);
        $response->setPayload($returnData);
        return $response;
    }

    private function createRecord($record)
    {
        $this->dbh->perform('
        insert into safmgr.attendance_intention
            (uniqueid, choice, intention_type, delayed_start_date, response_time, additional_choice_info)
        values
          (?, ?, ?, to_date(?, \'YYYY-MM-DD\'), to_date(?, \'YYYY-MM-DD HH24:MI:SS\'),?)'
            , $record['uniqueId'], $record['choice'], $record['intentionType'], $record['delayedStartDate'] ?? null,
            $record['responseTime'], $record['additionalChoiceInfo'] ?? null);

        $querySql = 'select id, uniqueid, choice, to_char(delayed_start_date, \'YYYY-MM-DD\') as delayed_start_date, intention_type,
                            to_char(response_time, \'YYYY-MM-DD HH24:MI:SS\') as response_time, additional_choice_info
                        from safmgr.attendance_intention
                        where uniqueid = ?
                          and choice = ?
                          and intention_type = ?
                          and response_time = to_date(?, \'YYYY-MM-DD HH24:MI:SS\')';
        return $this->dbh->queryfirstrow_assoc($querySql,
            [$record['uniqueId'], $record['choice'], $record['intentionType'], $record['responseTime']]);

    }


    private function buildSQLForQuery($uniqueID, $minActionDate)
    {
        $this->querySql = 'select id, uniqueid, choice, intention_type, to_char(delayed_start_date, \'YYYY-MM-DD\') as delayed_start_date,
                to_char(response_time, \'YYYY-MM-DD HH24:MI:SS\') as response_time, additional_choice_info 
                from safmgr.attendance_intention 
                where uniqueId = ?';

        $this->queryParams = [$uniqueID];
        if(!empty($minActionDate)) {
            $this->querySql .= ' and response_time >= to_date(?, \'YYYYMMDD\')';
            $this->queryParams[] = $minActionDate;
        }
        return true;
    }


    private function makeModel($record)
    {
        $model = [];
        $model['id'] = $record['id'];
        $model['uniqueId'] = $record['uniqueid'];
        $model['choice'] = $record['choice'];
        $model['intentionType'] = $record['intention_type'];
        $model['delayedStartDate'] = $record['delayed_start_date'];
        $model['responseTime'] = $record['response_time'];
        $model['additionalChoiceInfo'] = $record['additional_choice_info'];
        return $model;
    }

    public function deleteIntention()
    {
        $request = $this->getRequest();
        $uniqueId = $request->getResourceParam('uniqueId');

        $this->dbh->perform(
            'delete from safmgr.attendance_intention
                        where uniqueid = ?',
            $uniqueId
        );

        $response = $this->getResponse();
        $response->setStatus(App::API_OK);
        return $response;
    }

}
