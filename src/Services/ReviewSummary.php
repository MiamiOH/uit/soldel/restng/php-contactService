<?php

namespace MiamiOH\RestngContactService\Services;

class ReviewSummary
{

    private $datasource_name = 'MUWS_GEN_PROD';

    private $database;
    private $dbh;
    public $countOfRequestedStatus;
    public $countOfCompletedStatus;
    public $totalNumber;
    private $minActionDate;

    /** @var WAS */
    private $was;

    /**
     * @param $database
     */
    public function setDatabase($database)
    {
        $this->database = $database;
        $this->dbh = $this->database->getHandle($this->datasource_name);
    }

    /**
     * @param $was
     */
    public function setWas($was)
    {
        $this->was = $was;
    }

    /*
    Returns count of profiles which are required/requested to complete the review and the percentages of completed
    and outstanding profiles. */
    /**
     * @param $periodRecords
     * @param array $options
     *
     * @return array
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getSummary($periodRecords, $options = [])
    {

        $minActionDate = $periodRecords['requestedStartDateFormatted'];


        $countOfRequestedStatus = 0;

        $countOfCompletedStatus = 0;


        $type = isset($options['type']) ? $options['type'] : '';
        if ($type && !in_array($type, ['student','employee'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Invalid type "' . $type . '" for getReviewStatusList');
        }


        $params = [];
        $queryString = '
                select review_status, count(*) from (select status_id, lower(szbuniq_unique_id) as uniqueid, pidm, to_char(add_date, \'YYYY-MM-DD\') as add_date,
                    to_char(review_date, \'YYYY-MM-DD\') as review_date, review_status, review_type
                from (select *
                      from safmgr.person_contact_activity pca
                      where status_id = (
                        select max(status_id)
                          from safmgr.person_contact_activity pcb
                          where pcb.pidm = pca.pidm
                          and review_type = ?
                      )) pa
                      inner join szbuniq on pa.pidm = szbuniq_pidm
                where review_status != \'cancelled\'
                    and review_type = ?
    
          ';

        $params[] = $type;
        $params[] = $type;

        if ($minActionDate) {
            $queryString .= '
                              and (review_date is null or review_date >= to_date(?, \'YYYY-MM-DD\'))
                ';
            $params[] = $minActionDate;
        }

        $queryString .= ') group by review_status';

        $sth = $this->dbh->prepare($queryString, $params);
        $sth->execute($params);


        while ($record = $sth->fetchrow_assoc()) {
            $results[] = $this->makeModel($record);
        }

        for ($i = 0; $i < count($results); $i++) {
            if ($results[$i]['reviewStatus'] == 'requested' || $results[$i]['reviewStatus'] == 'required') {
                $countOfRequestedStatus = $results[$i]['count(*)'];

            }
            if ($results[$i]['reviewStatus'] == 'complete') {
                $countOfCompletedStatus = $results[$i]['count(*)'];

            }

        }

        $percentOfCompleted = ($countOfCompletedStatus / ($countOfRequestedStatus + $countOfCompletedStatus)) * 100;

        $percentOfRequestedOrRequired = 100 - $percentOfCompleted;

        $records = [
            'numberOfRequestedOrRequired' => $countOfRequestedStatus,
            'numberOfCompleted' => $countOfCompletedStatus,
            'percentOfRequestedOrRequired' => $percentOfRequestedOrRequired,
            'percentOfCompleted' => $percentOfCompleted,
            'requestedStartDate' => $periodRecords['requestedStartDateFormatted'],
            'requiredStartDate' => $periodRecords ['requiredStartDateFormatted'],
            'state' => $periodRecords['state']
        ];


        return $records;


    }

    /*
    Returns count of profiles which are required/requested to complete the review and the percentages of completed
    and outstanding profiles. */
    /**
     * @param $periodRecords
     * @param array $options
     *
     * @return array
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getSummaryDetails($periodRecords, $options = [])
    {

        $minActionDate = $periodRecords['requestedStartDateFormatted'];


        $countOfRequestedStatus = 0;

        $countOfCompletedStatus = 0;


        $type = isset($options['type']) ? $options['type'] : '';
        if ($type && !in_array($type, ['student','employee'])) {
            throw new \MiamiOH\RESTng\Exception\BadRequest('Invalid type "' . $type . '" for getReviewStatusList');
        }


        $params = [];
        $queryString = '
                select review_status as status, campus, count(*) as count
                from (
                    select status_id, lower(szbuniq_unique_id) as uniqueid, pidm, to_char(add_date, \'YYYY-MM-DD\') as add_date,
                        to_char(review_date, \'YYYY-MM-DD\') as review_date, review_status, review_type, campus
                        from (select *
                                from safmgr.person_contact_activity pca
                                    where status_id = (
                                            select max(status_id)
                                            from safmgr.person_contact_activity pcb
                                            where pcb.pidm = pca.pidm
                                            and review_type = ?
                                )
                            ) pa
                          inner join szbuniq on pa.pidm = szbuniq_pidm
                where  review_type = ?
          ';

        $params[] = $type;
        $params[] = $type;

        if ($minActionDate) {
            $queryString .= '
                              and (review_date is null or review_date >= to_date(?, \'YYYY-MM-DD\'))
                ';
            $params[] = $minActionDate;
        }


        $queryString .= ') group by review_status, campus';

        $details = $this->dbh->queryall_array($queryString, $params);

        $records = [
            'state' => $periodRecords['state'],
            'requestedStartDate' => $periodRecords['requestedStartDateFormatted'],
            'requiredStartDate' => $periodRecords ['requiredStartDateFormatted'],
            'details' => $details,
        ];


        return $records;


    }

    /**
     * @param $record
     *
     * @return array
     */
    private function makeModel($record)
    {
        $model = [
            'reviewStatus' => $record['review_status'],
            'count(*)' => $record['count(*)']
        ];

        return $model;
    }


}