<?php

namespace MiamiOH\RestngContactService\Services;

class ReviewPeriod
{

    /** @var Term */
    private $term;
    /** @var Student */
    private $student;
    /** @var Employee */
    private $employee;

    /** @var  \MiamiOH\RESTng\Util\Configuration $configuration */
    private $configuration;

    private $config = [];

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
        $this->config = $this->configuration->getConfiguration('PersonContactInfo',
            'Review State');
    }

    public function setTerm($term)
    {
        $this->term = $term;
    }

    public function setStudent($student)
    {
        $this->student = $student;
    }

    public function setEmployee($employee)
    {
        $this->employee = $employee;
    }

    /**
     * @param $type
     *
     * @return mixed
     * @throws \Exception
     */
    public function getReviewPeriod($type, $includePeople = true)
    {
        $period = [
            'state' => '',
            'requestedStartDate' => '',
            'requestedStartDateFormatted' => '',
            'requiredStartDate' => '',
            'requiredStartDateFormatted' => '',
            'type' => '',
        ];

        if ($includePeople) {
            $period['people'] = [];
        }

        switch ($type) {
            case 'student':
                $period['type'] = $type;
                $currentReviewTerm = $this->term->getCurrentReviewTermForStudents();
                $termStartDate = $currentReviewTerm['termStartDate'];

                if ($termStartDate) {
                    $today = date('Ymd');
                    $period['requestedStartDate'] = date('Ymd',
                        strtotime($termStartDate . ' -' .
                            $this->config['daysBeforeTermStartRequested'] . ' days'));
                    $period['requiredStartDate'] = date('Ymd',
                        strtotime($termStartDate . ' +' .
                            $this->config['daysAfterTermStartRequired'] . ' days'));

                    $period['requestedStartDateFormatted'] = date('Y-m-d',
                        strtotime($termStartDate . ' -' .
                            $this->config['daysBeforeTermStartRequested'] . ' days'));
                    $period['requiredStartDateFormatted'] = date('Y-m-d',
                        strtotime($termStartDate . ' +' .
                            $this->config['daysAfterTermStartRequired'] . ' days'));
                }

                if (!$termStartDate) {
                    $period['state'] = '';
                } elseif ($today >= $period['requiredStartDate']) {
                    $period['state'] = 'required';
                } elseif ($today >= $period['requestedStartDate']) {
                    $period['state'] = 'requested';
                }

                if ($includePeople) {
                    $this->student->setTermCode($currentReviewTerm['termCode']);
                    $this->student->setReviewState($period['state']);

                    $period['people'] = $this->student->getStudentsForReview();
                }

                break;
            case 'employee':
                $period['type'] = $type;

                $currentReviewTerm = $this->term->getCurrentReviewTermForEmployees();
                $termStartDate = $currentReviewTerm['termStartDate'];

                if ($termStartDate) {
                    $today = date('Ymd');
                    $period['requestedStartDate'] = date('Ymd',
                        strtotime($termStartDate . ' -' .
                            $this->config['daysBeforeTermStartRequestedEmployees'] . ' days'));
                    $period['requiredStartDate'] = date('Ymd',
                        strtotime($termStartDate . ' +' .
                            $this->config['daysAfterTermStartRequiredEmployees'] . ' days'));
                    $period['requestedStartDateFormatted'] = date('Y-m-d',
                        strtotime($termStartDate . ' -' .
                            $this->config['daysBeforeTermStartRequestedEmployees'] . ' days'));
                    $period['requiredStartDateFormatted'] = date('Y-m-d',
                        strtotime($termStartDate . ' +' .
                            $this->config['daysAfterTermStartRequiredEmployees'] . ' days'));

                    if ($today >= $period['requiredStartDate']) {
                        $period['state'] = 'required';
                    } elseif ($today >= $period['requestedStartDate']) {
                        $period['state'] = 'requested';
                    }
                } else {
                    $period['state'] = '';
                }

                if ($includePeople) {
                    $this->employee->setTermCode($currentReviewTerm['termCode']);
                    $this->employee->setReviewState($period['state']);

                    $period['people'] = $this->employee->getEmployeesForReview();
                }
                break;
            default:
                throw new \Exception('Unsupported review required list type ' . $type);
        }

        return $period;
    }

}