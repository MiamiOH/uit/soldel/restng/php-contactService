<?php

namespace MiamiOH\RestngContactService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ReviewPeriodResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Person.Contact.ReviewPeriodEntry',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'reviewStatus' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.ReviewPeriod',
            'type' => 'object',
            'properties' => array(
                'state' => array(
                    'type' => 'string',
                ),
                'requestedStartDate' => array(
                    'type' => 'string',
                ),
                'requiredStartDate' => array(
                    'type' => 'string',
                ),
                'type' => array(
                    'enum' => ['student','employee'],
                    'type' => 'string',
                ),
                'people' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/Person.Contact.ReviewPeriodEntry',
                )
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonReviewPeriod',
            'class' => 'MiamiOH\RestngContactService\Services\ReviewPeriod',
            'description' => 'Provide review required list from population',
            'set' => array(
                'student' => array(
                    'type' => 'service',
                    'name' => 'PersonContactProfileStudent'
                ),
                'employee' => array(
                    'type' => 'service',
                    'name' => 'PersonContactProfileEmployee'
                ),
                'term' => array(
                    'type' => 'service',
                    'name' => 'PersonContactProfileTerm'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonReviewPeriodREST',
            'class' => 'MiamiOH\RestngContactService\Services\ReviewPeriodREST',
            'description' => 'Provide review required list from population',
            'set' => array(
                'reviewPeriod' => array(
                    'type' => 'service',
                    'name' => 'PersonReviewPeriod'
                ),
                'bannerUtil' => array(
                    'type' => 'service',
                    'name' => 'MU\BannerUtil'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.reviewPeriod.v1.current',
            'description' => 'Get review required list from population',
            'tags' => array('Person'),
            'pattern' => '/person/contact/reviewPeriod/v1/current',
            'service' => 'PersonReviewPeriodREST',
            'method' => 'getCurrentReviewPeriod',
            'returnType' => 'model',
            'options' => array(
                'type' => array(
                    'required' => true,
                    'enum' => ['student','employee'],
                    'description' => 'Type of resources to return which has review status of "requested" or "required"'
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'Person-Review',
                    'key' => 'view'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A review period object containing a list of people',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.ReviewPeriod',
                    )
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}