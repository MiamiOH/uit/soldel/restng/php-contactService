<?php

namespace MiamiOH\RestngContactService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ConfigResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Person.Contact.Config.Item',
            'type' => 'object',
            'properties' => array(
                'key' => array(
                    'type' => 'string',
                ),
                'value' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.Config.Category',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Contact.Config.Item'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonContactConfigREST',
            'class' => 'MiamiOH\RestngContactService\Services\ConfigREST',
            'description' => 'REST resources for the contact config service',
            'set' => array(
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.config.v1',
            'description' => 'Person contact configuration resource',
            'tags' => array('Person'),
            'pattern' => '/person/contact/config/v1',
            'service' => 'PersonContactConfigREST',
            'method' => 'getConfiguration',
            'options' => array(
                'category' => array(
                    'required' => true,
                    'enum' => ['uiString', 'state'],
                    'description' => 'The configuration category to fetch'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of configuration items',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Contact.Config.Category',
                    )
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}