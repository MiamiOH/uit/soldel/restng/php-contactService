<?php

namespace MiamiOH\RestngContactService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class PledgeResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'Person.Pledge',
            'type' => 'object',
            'properties' => array(
                'id' => array(
                    'type' => 'integer',
                ),
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'type' => array(
                    'type' => 'string',
                ),
                'response' => array(
                    'type' => 'string'
                ),
                'responseTime' => array(
                    'type' => 'string'
                )
            )
        ));
        $this->addDefinition(array(
            'name' => 'Person.Create.Pledge',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'type' => array(
                    'type' => 'string',
                ),
                'response' => array(
                    'type' => 'string'
                ),
                'responseTime' => array(
                    'type' => 'string',
                )

            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Pledge.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Pledge'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonPledge',
            'class' => 'MiamiOH\RestngContactService\Services\Pledge',
            'description' => 'Provide handling of Community Pledge Information',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
            )

        ));
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Get the Community Pledge Information for a particular User',
            'name' => 'person.pledge.get.id',
            'tags' => array('Person'),
            'pattern' => '/person/pledge/v1/:uniqueId',
            'service' => 'PersonPledge',
            'method' => 'getPledgeByUniqueId',
            'params' => array(
                'uniqueId' => array(
                    'description' => 'A student uniqueid',
                ),
            ),
            'options' => array(
                'minActionDate' => array(
                    'description' => 'Minimum date of action to be included (YYYYMMDD)'
                )
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Pledge',
                        'key' => 'view'
                    ),
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'The newly created Community Pledge object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Pledge.Collection',
                    )
                ),
            )


        ]);

        $this->addResource([
            'action' => 'create',
            'description' => 'Create the Community Pledge for a particular User',
            'name' => 'person.pledge.v1.create',
            'tags' => array('Person'),
            'pattern' => '/person/pledge/v1',
            'service' => 'PersonPledge',
            'method' => 'createPledgeResponse',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Pledge',
                        'key' => 'create'
                    ),
                )
            ),
            'body' => array(
                'description' => 'A person AttendanceIntention model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Create.Pledge'
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'The newly created Community Pledge object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Pledge',
                    )
                ),
            )

        ]);

    }

    public function registerOrmConnections(): void
    {
        // TODO: Implement registerOrmConnections() method.
    }
}
