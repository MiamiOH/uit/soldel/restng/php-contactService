<?php

namespace MiamiOH\RestngContactService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ReviewSummaryResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Person.Contact.ReviewSummary',
            'type' => 'object',
            'properties' => array(
                'numberOfOutstanding' => array(
                    'type' => 'string'
                ),
                'numberOfRequestedOrRequired' => array(
                    'type' => 'string'
                ),
                'numberOfCompleted' => array(
                    'type' => 'string'
                ),
                'percentOfOutstanding' => array(
                    'type' => 'string'
                ),
                'percentOfCompleted' => array(
                    'type' => 'string'
                ),
                'requestedStartDate' => array(
                    'type' => 'string'
                ),
                'requiredStartDate' => array(
                    'type' => 'string'
                ),
                'state' => array(
                    'type' => 'string'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.ReviewSummary.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Contact.ReviewSummary'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.ReviewSummaryPeriod',
            'type' => 'object',
            'properties' => array(
                'state' => array(
                    'type' => 'string'
                ),
                'requestedStartDate' => array(
                    'type' => 'string'
                ),
                'requiredStartDate' => array(
                    'type' => 'string'
                ),
                'details' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/Person.Contact.ReviewSummaryDetail.Collection',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.ReviewSummaryDetail',
            'type' => 'object',
            'properties' => array(
                'status' => array(
                    'type' => 'string'
                ),
                'campus' => array(
                    'type' => 'string'
                ),
                'count' => array(
                    'type' => 'integer'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.ReviewSummaryDetail.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Contact.ReviewSummaryDetail'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonReviewSummary',
            'class' => 'MiamiOH\RestngContactService\Services\ReviewSummary',
            'description' => 'Provide review summary list from population',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'was' => array('type' => 'service', 'name' => 'PersonReviewWAS'),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonReviewSummaryREST',
            'class' => 'MiamiOH\RestngContactService\Services\ReviewSummaryREST',
            'description' => 'Provide review summary list from population',
            'set' => array(
                'reviewPeriod' => array(
                    'type' => 'service',
                    'name' => 'PersonReviewPeriod'
                ),
                'reviewStatus' => array(
                    'type' => 'service',
                    'name' => 'PersonReviewStatus'
                ),
                'reviewSummary' => array(
                    'type' => 'service',
                    'name' => 'PersonReviewSummary'
                ),
                'bannerUtil' => array(
                    'type' => 'service',
                    'name' => 'MU\BannerUtil'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.reviewSummary.v1',
            'description' => 'Get review summary list from population',
            'tags' => array('Person'),
            'pattern' => '/person/contact/reviewSummary/v1',
            'service' => 'PersonReviewSummaryREST',
            'method' => 'getReviewSummaryList',
            'returnType' => 'collection',
            'options' => array(
                'type' => array(
                    'enum' => array('student','employee'),
                    'default' => 'student',
                    'description' => 'Return records for review type'
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'Person-Review-Summary',
                    'key' => 'view'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of population with review summary',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Contact.ReviewSummary.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.reviewSummary.v2',
            'description' => 'Get review summary list from population',
            'tags' => array('Person'),
            'pattern' => '/person/contact/reviewSummary/v2',
            'service' => 'PersonReviewSummaryREST',
            'method' => 'getReviewSummaryDetails',
            'returnType' => 'collection',
            'options' => array(
                'type' => array(
                    'enum' => array('student', 'employee'),
                    'default' => 'student',
                    'description' => 'Return records for review type'
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'Person-Review-Summary',
                    'key' => 'view'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of population with review summary',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Contact.ReviewSummaryPeriod',
                    )
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}