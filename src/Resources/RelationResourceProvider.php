<?php

namespace MiamiOH\RestngContactService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class RelationResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Person.Contact.Relation',
            'type' => 'object',
            'properties' => array(
                'relationCode' => array(
                    'type' => 'string',
                ),
                'description' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Relation.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Contact.Relation'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonRelation',
            'class' => 'MiamiOH\RestngContactService\Services\Relation',
            'description' => 'Provide relation list services.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonRelationREST',
            'class' => 'MiamiOH\RestngContactService\Services\RelationREST',
            'description' => 'Provide relation list services.',
            'set' => array(
                'relation' => array('type' => 'service', 'name' => 'PersonRelation'),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonReviewRelation',
            'class' => 'MiamiOH\RestngContactService\Services\Relation',
            'description' => 'Provide relation list',
            'set' => array(),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.relation.v1',
            'description' => 'Person relation resource',
            'tags' => array('Person'),
            'pattern' => '/person/contact/relation/v1',
            'service' => 'PersonRelationREST',
            'method' => 'getRelationList',
            'options' => array(
                'relationType' => array(
                    'enum' => ['ec'],
                    'description' => 'Type of relations to return'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of relations',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Relation.Collection',
                    )
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}