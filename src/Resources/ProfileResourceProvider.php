<?php

namespace MiamiOH\RestngContactService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ProfileResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Person.Contact.Profile',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'bannerId' => array(
                    'type' => 'string',
                ),
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'firstName' => array(
                    'type' => 'string',
                ),
                'lastName' => array(
                    'type' => 'string',
                ),
                'addDate' => array(
                    'type' => 'date',
                ),
                'reviewStartDate' => array(
                    'type' => 'date',
                ),
                'reviewDate' => array(
                    'type' => 'date',
                ),
                'reviewStatus' => array(
                    'type' => 'string',
                ),
                'reviewTermCode' => array(
                    'type' => 'string',
                ),
                'reviewTermDescription' => array(
                    'type' => 'string',
                ),
                'hasResidenceHallAssignment' => array(
                    'type' => 'boolean',
                ),
                'requiredActions' => array(
                    'type' => 'array',
                    'items' => [
                        'type' => 'string'
                    ]
                ),
                'excludedActions' => array(
                    'type' => 'array',
                    'items' => [
                        'type' => 'string'
                    ]
                ),
                'primaryAffiliationCode' => array(
                    'type' => 'string',
                ),
                'primaryAffiliationDescription' => array(
                    'type' => 'string',
                ),
                'primaryAffiliationType' => array(
                    'type' => 'string',
                ),
                'offCampusAddressRequired' => array(
                    'type' => 'boolean',
                ),
                'campusCode' => array(
                    'type' => 'string'
                ),
                'reviewType' => array(
                    'type' => 'string'
                ),
                'commuter' => array(
                    'type' => 'boolean'
                ),
                'international' => array(
                    'type' => 'string'
                ),
                'studentLevelCode' => array(
                    'type' => 'string'
                ),
                'studentTypeCode' => array(
                    'type' => 'string'
                ),
                'studentCampusCode' => array(
                    'type' => 'string'
                ),
                'classes' => array(
                    'type' => 'array',
                    'items' => [
                        'type' => 'string'
                    ]
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.Profile.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Contact.Profile'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.Population.Entry',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'profileActivityDate' => array(
                    'type' => 'string',
                ),
                'reviewStatus' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.Population.Entry.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Contact.Population.Entry'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonContactProfileTerm',
            'class' => 'MiamiOH\RestngContactService\Services\Term',
            'description' => 'Provide handling of term for contact profile.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonContactProfileStudent',
            'class' => 'MiamiOH\RestngContactService\Services\Student',
            'description' => 'Provide handling of student data queries for contact profile.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonContactProfileEmployee',
            'class' => 'MiamiOH\RestngContactService\Services\Employee',
            'description' => 'Provide handling of employee data queries for contact profile.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonContactProfile',
            'class' => 'MiamiOH\RestngContactService\Services\Profile',
            'description' => 'Provide handling of person contact profile data.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'term' => array(
                    'type' => 'service',
                    'name' => 'PersonContactProfileTerm'
                ),
                'student' => array(
                    'type' => 'service',
                    'name' => 'PersonContactProfileStudent'
                ),
                'employee' => array(
                    'type' => 'service',
                    'name' => 'PersonContactProfileEmployee'
                ),
                'ldapFactory' => array(
                    'type' => 'service',
                    'name' => 'APILDAPFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonContactProfileREST',
            'class' => 'MiamiOH\RestngContactService\Services\ProfileREST',
            'description' => 'Provide REST methods for person contact profile',
            'set' => array(
                'bannerUtil' => array(
                    'type' => 'service',
                    'name' => 'MU\BannerUtil'
                ),
                'profile' => array(
                    'type' => 'service',
                    'name' => 'PersonContactProfile'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.profile.v1',
            'description' => 'Person  resource',
            'tags' => array('Person'),
            'pattern' => '/person/contact/profile/v1',
            'service' => 'PersonContactProfileREST',
            'method' => 'getProfileList',
            'options' => array(
                'pidm' => array(
                    'type' => 'list',
                    'required' => true,
                    'description' => 'A list of pidms'
                ),
                'reviewType' => array(
                    'enum' => ['student','employee'],
                    'description' => 'Return records for the given review type. Default: student',
                    'default' => 'student'
                )
            ),

            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'Person-ContactProfile',
                    'key' => 'view'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of profiles',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Contact.Profile.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.profile.v1.muid',
            'description' => 'Person contact info profile',
            'tags' => array('Person'),
            'pattern' => '/person/contact/profile/v1/:muid',
            'service' => 'PersonContactProfileREST',
            'method' => 'getProfile',
            'params' => array(
                'muid' => array(
                    'description' => 'A student uniqueid',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-ContactProfile',
                        'key' => 'view'
                    ),
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A profile',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Contact.Profile',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'person.contact.profile.v1.muid.update',
            'description' => 'Update person profile status',
            'tags' => array('Person'),
            'pattern' => '/person/contact/profile/v1/:muid',
            'service' => 'PersonContactProfileREST',
            'method' => 'updateProfileStatus',
            'params' => array(
                'muid' => array(
                    'description' => 'A student uniqueid',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-ContactProfile',
                        'key' => 'update'
                    ),
                ),
            ),
            'body' => array(
                'description' => 'A person contact model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Contact.Profile'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Update status reponse',
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}