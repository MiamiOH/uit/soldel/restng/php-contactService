<?php

namespace MiamiOH\RestngContactService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ReviewStatusResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Person.Contact.ReviewStatus',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'string',
                ),
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'reviewStatus' => array(
                    'type' => 'string',
                    'enum' => ['requested', 'required', 'complete', 'cancelled'],
                ),
                'addDate' => array(
                    'type' => 'string',
                ),
                'reviewDate' => array(
                    'type' => 'string',
                ),
                'type' => array(
                    'type' => 'string',
                    'enum' => ['student', 'employee'],
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.ReviewStatus.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Contact.ReviewStatus'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonReviewHTTP',
            'class' => 'MiamiOH\RestngContactService\Services\HTTP',
            'description' => 'Provide basic http functionality',
        ));

        $this->addService(array(
            'name' => 'PersonReviewWAS',
            'class' => 'MiamiOH\RestngContactService\Services\WAS',
            'description' => 'Provide methods to interact with WAS for updates',
            'set' => array(
                'http' => array('type' => 'service', 'name' => 'PersonReviewHTTP'),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonReviewStatus',
            'class' => 'MiamiOH\RestngContactService\Services\ReviewStatus',
            'description' => 'Provide review status list from population',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'was' => array('type' => 'service', 'name' => 'PersonReviewWAS'),
                'profile' => array(
                    'type' => 'service',
                    'name' => 'PersonContactProfile'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonReviewStatusREST',
            'class' => 'MiamiOH\RestngContactService\Services\ReviewStatusREST',
            'description' => 'Provide review status list from population',
            'set' => array(
                'reviewStatus' => array(
                    'type' => 'service',
                    'name' => 'PersonReviewStatus'
                ),
                'bannerUtil' => array(
                    'type' => 'service',
                    'name' => 'MU\BannerUtil'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.reviewStatus.v1',
            'description' => 'Get review status list from population',
            'tags' => array('Person'),
            'pattern' => '/person/contact/reviewStatus/v1',
            'service' => 'PersonReviewStatusREST',
            'method' => 'getReviewStatusList',
            'isPageable' => true,
            // Setting defaultPageLimit causes RESTng to always treat a request as paged, even if you don’t ask for it
            //'defaultPageLimit' => 200,
            'maxPageLimit' => 1000,
            'returnType' => 'collection',
            'options' => array(
                'type' => array(
                    'enum' => ['student','employee'],
                    'description' => 'Return records for review type'
                ),
                'minActionDate' => array(
                    'description' => 'Minimum date of action to be included (YYYYMMDD)'
                ),
                'status' => array(
                    'type' => 'list',
                    'enum' => ['requested', 'required', 'cancelled', 'complete'],
                    'description' => 'Return records for review status'
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'Person-Review',
                    'key' => 'view'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of population with review status',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.ReviewStatus.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'person.contact.reviewStatus.v1.muid.update',
            'description' => 'Update the review status for the given user',
            'tags' => array('Person'),
            'pattern' => '/person/contact/reviewStatus/v1/:muid',
            'service' => 'PersonReviewStatusREST',
            'method' => 'updateReviewStatus',
            'returnType' => 'none',
            'params' => array(
                'muid' => array(
                    'description' => 'Miami identifier',
                    'alternateKeys' => ['uid', 'pidm']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid'
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Review',
                        'key' => 'update'
                    ),
                ),
            ),
            'body' => array(
                'description' => 'A person ReviewStatus model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Contact.ReviewStatus'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Successfully updated the status',
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'person.contact.reviewStatus.v1.update',
            'description' => 'Update the review status for a collection of users',
            'tags' => array('Person'),
            'pattern' => '/person/contact/reviewStatus/v1',
            'service' => 'PersonReviewStatusREST',
            'method' => 'updateReviewStatusCollection',
            'returnType' => 'none',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'Person-Review',
                    'key' => 'update'
                ),
            ),
            'body' => array(
                'description' => 'A collection of person ReviewStatus models',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Contact.ReviewStatus.Collection'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Successfully updated the status',
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}