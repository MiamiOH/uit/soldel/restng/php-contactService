<?php

namespace MiamiOH\RestngContactService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class Fall2020SummaryResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Person.Contact.Fall2020Period',
            'type' => 'object',
            'properties' => array(
                'state' => array(
                    'type' => 'string'
                ),
                'requestedStartDate' => array(
                    'type' => 'string'
                ),
                'requiredStartDate' => array(
                    'type' => 'string'
                ),
                'details' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/Person.Contact.Fall2020Detail.Collection',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.Fall2020Detail',
            'type' => 'object',
            'properties' => array(
                'type' => array(
                    'type' => 'string'
                ),
                'choice' => array(
                    'type' => 'string'
                ),
                'campus' => array(
                    'type' => 'string'
                ),
                'count' => array(
                    'type' => 'integer'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.Fall2020Detail.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Contact.Fall2020Detail'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonFallSummary2020',
            'class' => 'MiamiOH\RestngContactService\Services\Fall2020Summary',
            'description' => 'Provide review summary list from population',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonFall2020SummaryREST',
            'class' => 'MiamiOH\RestngContactService\Services\Fall2020SummaryREST',
            'description' => 'Provide review summary list from population',
            'set' => array(
                'reviewPeriod' => array(
                    'type' => 'service',
                    'name' => 'PersonReviewPeriod'
                ),
                'fall2020Summary' => array(
                    'type' => 'service',
                    'name' => 'PersonFallSummary2020'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.fall2020Summary.v1',
            'description' => 'Get review summary list from population',
            'tags' => array('Person'),
            'pattern' => '/person/contact/fall2020Summary/v1',
            'service' => 'PersonFall2020SummaryREST',
            'method' => 'getFall2020Data',
            'returnType' => 'collection',
            'options' => array(
                'type' => array(
                    'enum' => array('student'),
                    'default' => 'student',
                    'description' => 'Return records for review type'
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'WebServices',
                    'module' => 'Person-Review-Summary',
                    'key' => 'view'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of population with review summary',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Contact.Fall2020.Collection',
                    )
                ),
            )
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}