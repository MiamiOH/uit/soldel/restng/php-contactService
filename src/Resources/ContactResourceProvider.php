<?php

namespace MiamiOH\RestngContactService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class ContactResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Person.Contact',
            'type' => 'object',
            'properties' => array(
                'relation' => array(
                    'type' => 'string',
                ),
                'relationCode' => array(
                    'type' => 'string',
                ),
                'pidm' => array(
                    'type' => 'number',
                ),
                'priority' => array(
                    'type' => 'integer',
                ),
                'firstName' => array(
                    'type' => 'string',
                ),
                'lastName' => array(
                    'type' => 'string',
                ),
                'middleName' => array(
                    'type' => 'string',
                ),
                'phoneNumber' => array(
                    'type' => 'string',
                ),
                'phoneNumberNational' => array(
                    'type' => 'string',
                ),
                'phoneNumberInternational' => array(
                    'type' => 'string',
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Contact.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Contact'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonContactPhoneHelper',
            'class' => 'MiamiOH\RestngContactService\Services\PhoneHelper',
            'description' => 'Phone number parsing, validation and formatting.',
        ));

        $this->addService(array(
            'name' => 'PersonContact',
            'class' => 'MiamiOH\RestngContactService\Services\Contact',
            'description' => 'Provide the student information for the given uid.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'phoneHelper' => array(
                    'type' => 'service',
                    'name' => 'PersonContactPhoneHelper'
                ),
            ),
        ));

        $this->addService(array(
            'name' => 'PersonContactREST',
            'class' => 'MiamiOH\RestngContactService\Services\ContactREST',
            'description' => 'Provide the student information for the given uid.',
            'set' => array(
                'bannerUtil' => array(
                    'type' => 'service',
                    'name' => 'MU\BannerUtil'
                ),
                'contact' => array('type' => 'service', 'name' => 'PersonContact'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.v1.muid',
            'description' => 'Person contact resource',
            'tags' => array('Person'),
            'pattern' => '/person/contact/v1/:muid',
            'service' => 'PersonContactREST',
            'method' => 'getContact',
            'params' => array(
                'muid' => array(
                    'description' => 'A student uniqueid',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Contact',
                        'key' => 'view'
                    ),
                ),
            ),
            'options' => array(
                'type' => array(
                    'enum' => ['emergency', 'missingPerson'],
                    'description' => 'Type of contact(s) to return'
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of contacts',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Contact.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'create',
            'name' => 'person.contact.v1.muid.create',
            'description' => 'Create person contact object',
            'tags' => array('Person'),
            'pattern' => '/person/contact/v1/:muid',
            'service' => 'PersonContactREST',
            'method' => 'createContactCollection',
            'params' => array(
                'muid' => array(
                    'description' => 'A student uniqueid',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Contact',
                        'key' => 'create'
                    ),
                ),
            ),
            'options' => array(
                'type' => array(
                    'enum' => ['emergency', 'missingPerson'],
                    'description' => 'Type of contact(s) to create',
                    'required' => true
                ),
            ),
            'body' => array(
                'description' => 'A person contact model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Contact.Collection'
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'The newly create contact object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Contact.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'update',
            'name' => 'person.contact.v1.muid.update',
            'description' => 'Update person contact object',
            'tags' => array('Person'),
            'pattern' => '/person/contact/v1/:muid',
            'service' => 'PersonContactREST',
            'method' => 'updateContactCollection',
            'params' => array(
                'muid' => array(
                    'description' => 'A student uniqueid',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Contact',
                        'key' => 'update'
                    ),
                ),
            ),
            'options' => array(
                'type' => array(
                    'enum' => ['emergency', 'missingPerson'],
                    'description' => 'Type of contact(s) to update',
                    'required' => true
                ),
            ),
            'body' => array(
                'description' => 'A person contact model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Contact.Collection'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'The newly update contact object',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.Contact.Collection',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'delete',
            'name' => 'person.contact.v1.muid.Delete',
            'description' => 'Delete a person contact resource',
            'tags' => array('Person'),
            'pattern' => '/person/contact/v1/:muid',
            'service' => 'PersonContactREST',
            'method' => 'deleteContactCollection',
            'params' => array(
                'muid' => array(
                    'description' => 'A student uniqueid',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),
            'options' => array(
                'purgeInvalidContacts' => array(
                    'enum' => ['enabled'],
                    'description' => 'Indicates if all invalid contacts should be purged during delete operation',
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-Contact',
                        'key' => 'delete'
                    ),
                ),
            ),
            'body' => array(
                'description' => 'A collection of contacts to be deleted',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Contact.Collection'
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'No body content returned',
                    'returns' => array(
                        'type' => 'model',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'person.contact.v1.muid.missingPerson',
            'description' => 'Person missing person contact resource',
            'tags' => array('Person'),
            'pattern' => '/person/contact/v1/:muid/missingPerson',
            'service' => 'PersonContactREST',
            'method' => 'getMissingPersonContactList',
            'params' => array(
                'muid' => array(
                    'description' => 'A student uniqueid',
                    'alternateKeys' => ['uniqueId', 'pidm']
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-MissingPerson',
                        'key' => 'view',
                    ),
                ),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of contacts',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Person.Contact.Collection',
                    )
                ),
            )
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}