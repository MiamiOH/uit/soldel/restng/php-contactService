<?php

namespace MiamiOH\RestngContactService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class AttendanceIntentionResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'Person.AttendanceIntention',
            'type' => 'object',
            'properties' => array(
                'id' => array(
                    'type' => 'integer',
                ),
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'choice' => array(
                    'type' => 'string',
                ),
                'intentionType' => array(
                    'type' => 'string',
                ),
                'delayedStartDate' => array(
                    'type' => 'string',
                ),
                'responseTime' => array(
                    'type' => 'string',
                ),
                'additionalChoiceInfo' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Create.AttendanceIntention',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array(
                    'type' => 'string',
                ),
                'choice' => array(
                    'type' => 'string',
                ),
                'intentionType' => array(
                    'type' => 'string',
                ),
                'delayedStartDate' => array(
                    'type' => 'string',
                ),
                'responseTime' => array(
                    'type' => 'string',
                ),
                'additionalChoiceInfo' => array(
                    'type' => 'string',
                )

            )
        ));
        $this->addDefinition(array(
            'name' => 'Person.AttendanceIntention.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.AttendanceIntention'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'PersonAttendanceIntention',
            'class' => 'MiamiOH\RestngContactService\Services\AttendanceIntention',
            'description' => 'Provide handling of Attendance Intention Information',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
            )
        ));
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Get the Attendance Intention Information for a particular User',
            'name' => 'person.attendance.intention.v1.get.id',
            'tags' => array('Person'),
            'pattern' => '/person/attendanceIntention/v1/:uniqueId',
            'service' => 'PersonAttendanceIntention',
            'method' => 'getIntention',
            'params' => array(
                'uniqueId' => array(
                    'description' => 'A student uniqueid',
                )
            ),
            'options' => array(
                'minActionDate' => array(
                    'description' => 'Minimum date of action to be included (YYYYMMDD)'
                )
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-AttendanceIntention',
                        'key' => 'view'
                    ),
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'Collection of AttendanceIntention objects',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.AttendanceIntention.Collection',
                    )
                ),
            )
        ]);

        $this->addResource([
            'action' => 'create',
            'description' => 'Create the Attendance Intention Information for a particular User',
            'name' => 'person.attendance.intention.v1.create',
            'tags' => array('Person'),
            'pattern' => '/person/attendanceIntention/v1',
            'service' => 'PersonAttendanceIntention',
            'method' => 'createAttendanceIntention',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-AttendanceIntention',
                        'key' => 'create'
                    ),
                )
            ),
            'body' => array(
                'description' => 'A person AttendanceIntention model',
                'required' => true,
                'schema' => array(
                    '$ref' => '#/definitions/Person.Create.AttendanceIntention'
                )
            ),
            'responses' => array(
                App::API_CREATED => array(
                    'description' => 'The newly created AttendanceIntention objects',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Person.AttendanceIntention',
                    )
                ),
            )

        ]);

        $this->addResource([
            'action' => 'delete',
            'description' => 'Delete the Attendance Intention Information for a particular User',
            'name' => 'person.attendance.intention.v1.delete.id',
            'tags' => array('Person'),
            'pattern' => '/person/attendanceIntention/v1/:uniqueId',
            'service' => 'PersonAttendanceIntention',
            'method' => 'deleteIntention',
            'params' => array(
                'uniqueId' => array(
                    'description' => 'A student uniqueid',
                ),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                    array(
                        'application' => 'WebServices',
                        'module' => 'Person-AttendanceIntention',
                        'key' => 'delete'
                    ),
                )
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'No body content returned',
                    'returns' => array(
                        'type' => 'model',
                    )
                ),
            )
        ]);


    }

    public function registerOrmConnections(): void
    {
        // TODO: Implement registerOrmConnections() method.
    }
}
