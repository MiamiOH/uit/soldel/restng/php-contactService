BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE offcampus_status';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE offcampus_status
   (
    pidm number,
    term varchar2(10),
    response varchar2(1)
   );

