declare

  v_authman_app_name varchar2(64) := null;
  v_authman_module_name varchar2(64) := null;
  v_authman_entity_name varchar2(64) := null;
  v_authman_grantkey varchar2(64) := null;

begin

  v_authman_app_name := 'CM-PersonContactInfo';
  v_authman_module_name := 'Contact Info Review';
  v_authman_entity_name := 'CONTACT_WS_USER';
  v_authman_grantkey := 'view';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

end;
/
