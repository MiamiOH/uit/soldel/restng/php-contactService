declare

  v_authman_app_name varchar2(64) := null;
  v_authman_module_name varchar2(64) := null;
  v_authman_entity_name varchar2(64) := null;
  v_authman_grantkey varchar2(64) := null;

begin

  v_authman_app_name := 'WebServices';
  v_authman_module_name := 'Person-Contact';
  v_authman_entity_name := 'CONTACT_WS_USER';
  v_authman_grantkey := 'view';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_module_name := 'Person-ContactProfile';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_grantkey := 'update';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');
  v_authman_grantkey := 'view';

  v_authman_module_name := 'Person-MissingPerson';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_module_name := 'Person-Review';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_grantkey := 'update';
  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

  v_authman_entity_name := 'TEST_SELF';
  v_authman_grantkey := 'view';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

   v_authman_module_name := 'Person-Address';
   v_authman_entity_name := 'CONTACT_WS_USER';
   v_authman_grantkey := 'view';
   createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,

                      v_authman_grantkey, 'doej');

    v_authman_grantkey := 'create';
    createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,

                      v_authman_grantkey, 'doej');

    v_authman_module_name := 'Person-Phone';
    v_authman_grantkey := 'view';
    createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                         v_authman_grantkey, 'doej');

    v_authman_grantkey := 'create';
    createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                         v_authman_grantkey, 'doej');

    v_authman_module_name := 'Person-Contact';
    v_authman_grantkey := 'create';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

   v_authman_module_name := 'Person-Contact';
    v_authman_grantkey := 'delete';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');

   v_authman_module_name := 'Person-Contact';
   v_authman_grantkey := 'update';

  createAuthorizations(v_authman_app_name,v_authman_module_name,v_authman_entity_name,
                       v_authman_grantkey, 'doej');
end;
/
