create or replace FUNCTION            "FZ_CLASS_CALC" (pidm number, levl_code varchar2, term_code varchar2,in_progress_ind  varchar2)
RETURN VARCHAR2 AS
      --
      -- Declare Section.
      -- ===================================================================
      clas_code   varchar2(2); 

  BEGIN
      --
      -- Initialize.
      -- ===================================================================
      clas_code := NULL;

      IF pidm = 990033
      THEN
        clas_code := 'NC';
      END IF;

      IF pidm = 990034
       THEN
        clas_code := '99';
       END IF; 

      RETURN clas_code;
END fz_class_calc;
/
 
 