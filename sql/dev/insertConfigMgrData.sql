set define off;

delete from cm_config where config_application = 'PersonContactInfo';

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('PersonContactInfo', 'wasClientUpdateUrl',
        'text', 'scalar', 'Contact Info Review',
        'Base URL for updating review status in WAS for messaging',
        'http://ws/contactInfo/updateWAS.php?extapp=StudentContactInformation&ss=bob',
        sysdate, 'doej');

insert into cm_config (config_application, config_key,
        config_data_type, config_data_structure, config_category,
        config_desc, config_value, activity_date, activity_user)
    values ('PersonContactInfo', 'wasClientRequiredUpdateUrl',
        'text', 'scalar', 'Internal Config',
        'Base URL for updating review status in WAS for messaging',
        'http://ws/contactInfo/updateWAS.php?extapp=StudentContactInformationRequired&ss=bob',
        sysdate, 'doej');

