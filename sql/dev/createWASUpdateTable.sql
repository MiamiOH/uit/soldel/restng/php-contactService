BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE contactinfo_test_update';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/
  CREATE TABLE contactinfo_test_update
   (
    contactinfo_uid varchar2(8),
    contactinfo_app varchar2(64),
    contactinfo_reviewRequested varchar2(2),
    contactinfo_complete varchar2(2),
    contactinfo_completed_date varchar2(24)
   ) ;
