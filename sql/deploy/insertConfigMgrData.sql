set define off;

delete from cm_config where config_application = 'PersonContactInfo'
                            and config_key not in ('wasClientUpdateUrl', 'wasClientRequiredUpdateUrl');

--insert into cm_config (config_application, config_category, config_key,
--         config_value,
--         config_desc,
--         config_data_type, config_data_structure, activity_date, activity_user)
--    values ('PersonContactInfo', 'Internal Config', 'wasClientUpdateUrl',
--        'https://was.miamioh.edu/perl/was/extapplog?extapp=StudentContactInformation&ss=contact',
--        'Base URL for updating review status in WAS for messaging',
--        'text', 'scalar', sysdate, 'doej');

-- insert into cm_config (config_application, config_category, config_key,
--         config_value,
--         config_desc,
--         config_data_type, config_data_structure, activity_date, activity_user)
--    values ('PersonContactInfo', 'Internal Config', 'wasClientRequiredUpdateUrl',
--        'http://ws/contactInfo/updateWAS.php?extapp=StudentContactInformationRequired&ss=contact',
--        'Base URL for updating requried review status in WAS for messaging',
--        'text', 'scalar', sysdate, 'doej');

insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Review State', 'daysBeforeTermStartRequested',
        '7',
        'Number of days before the start of a Fall/Spring term to make reviews requested.',
        'text', 'scalar', sysdate, 'doej');

insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Review State', 'daysAfterTermStartRequired',
        '14',
        'Number of days after the start of a Fall/Spring term to make reviews required.',
        'text', 'scalar', sysdate, 'doej');

insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.heading',
        'Emergency Contacts',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.introMessage',
        '<p>Miami University requires every student to have <em>at least one</em> current emergency contact with a valid phone number listed that the university can use in case of an emergency. If you have questions about the emergency contact information, students should contact the Dean of Students via <a href=mailto:deanofstudents@miamiOH.edu?subject=Emergency+Contact+question>email</a> or phone (513) 529-1877, employees should contact Human Resources at (513) 529-3131, and faculty should contact Academic Personnel at (513) 529-6724.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.noContactsMessage',
        '<p>You do not have any emergency contacts listed.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.heading',
        'Missing Person Contacts',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.introMessage',
        '<p>In addition to registering your emergency contacts, you have the option to identify an individual to be confidentially contacted by Miami staff in the event you are suspected to be missing. If you choose not to provide a contact for this purpose, we will notify your emergency contact(s).</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.noContactsMessage',
        '<p>You do not have any missing person contacts listed. Your emergency contacts will be used instead.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'residentialAddress.heading',
        'Residential Address',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'residentialAddress.introMessage',
        '<p>An accurate residential address allows Miami to locate you in the event of an emergency situation.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.heading',
        'Local Address',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.introMessage',
        '<p>An accurate local address allows Miami to locate you in the event of an emergency situation.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.noAddressMessage',
        '<p>You do not have a local address on record.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.streetLine1PH',
        'Enter Street Line1',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.streetLine2PH',
        'Enter Street Line2',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.statePH',
        'Enter State',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.cityPH',
        'Enter City',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.postalCodePH',
        'Enter Postal Code',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.heading',
        'Cell Phone Number',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.introMessage',
        '<p>Miami will use a cell phone number to contact you via voice or text message for official university business. Cell phone numbers provided here are not published or shared with any third parties.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.noPhonesMessage',
        '<p>You do not have any cell phone numbers on record.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'home.title',
        'Personal Contact Information',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'home.content',
        '<p>Miami requires accurate emergency contact information, local address, and we ask for a cell phone number in order to contact you (or your emergency contact) in the event of an emergency or official university business. You should review the information on each page (Emergency Contacts, Missing Person Contacts, School Address, and Cell Phone Number). If the information is correct, you can simply click in the "Confirm" box, and each of the action items to the left will get checkmarked. If you need to update information, follow the instructions on each page to make those updates, then check the "Confirm" box on each page. Once all pages have been confirmed, you will need to check the "Complete Review" button in the "Action Items" box to finish the process. Thank you for taking the time to complete this review.</p><p>Note: If a student living in a residence hall needs to update their school address, they will need to contact the <a href="https://www.miamioh.edu/home/" target="_blank">H.O.M.E. Office</a> (opens in a new window) to correct that information. Commuter students will see their permanent mailing address, and if you need to edit that, you should visit <a href="https://miamioh.edu/onestop/your-info/index.html#/" target="_blank">One Stop - Your Personal Information</a> (opens in a new window) to learn how to edit that information. That page also has instructions on managing your directory information.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');

insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.deleteBtn.label',
        'delete',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.newPhone.label',
        'New cell phone',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.newPhoneBtn.label',
        'Add Cell Phone',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.otherNumbers.heading',
        'Other (non-cell) phone numbers',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.otherNumbers.message',
        '<p>Other numbers may be edited in BannerWeb, for instructions on managing your personal information, visit the <a href="https://miamioh.edu/onestop/your-info/index.html#/" target="_blank">One Stop - Your Personal Information</a> (will open in new window).</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddress.title',
        'Set Local Off Campus Address',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddress.message',
        '<p><strong>Only for current students who live off-campus</strong>, this is the address in the vicinity of the campus attended. You do not need (and should not have) a Local (Off-Campus) address if you are living in a residence hall or commuting from your permanent mailing address residence.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddress.currentAddressWarning',
        '<p>Providing a new address will end any current or future Local (Off-Campus) addresses you have set.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddress.street1.label',
        'Street Line 1',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddress.street2.label',
        'Street Line 2',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddress.city.label',
        'City',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddress.state.label',
        'State',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddress.postalCode.label',
        'Postal Code',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddress.cleanAddressFailureMsg',
        'Enter a valid address',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddress.cleanAddressSuggestionIntroMsg',
        'Address Suggestions:',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.newAddressBtn.label',
        'Add Address',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'schoolAddress.heading',
        'School Address',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'schoolAddress.introMessage.student',
        '<p>Oxford campus students are expected to have either a residence hall address, a local off-campus address for residential students, or permanent home address for cummuter students. Only residential, off-campus students should enter a local address. If you are living in the residence hall and the address is wrong, contact the <a href="http://miamioh.edu/hdrbs/home/index.html" target="_blank">H.O.M.E. Office</a> (opens in new window). If you are a commuter student and the address is wrong, contact the <a href="https://miamioh.edu/onestop/" target="_blank">One Stop</a> for assistance (opens in new window).</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'schoolAddress.introMessage.employee',
        '<p>School address information is applicable only for <em>students</em>.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'schoolAddress.introMessage.other',
        '<p>School address information is applicable only for <em>students</em>.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');

insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.deleteBtn.label',
        'Delete',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.firstName.label',
        'First name',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.lastName.label',
        'Last name',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.middleName.label',
        'Middle name',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.phoneNumber.label',
        'Phone Number',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.relation.label',
        'Relation',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.addContactBtn.label',
        'Add Contact',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.summary.label',
        'Action Items',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.commit.label',
        'Complete Review',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'relation.select.label',
        'please select one',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.deleteBtn.label',
        'Delete',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.firstName.label',
        'First name',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.lastName.label',
        'Last name',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.middleName.label',
        'Middle name',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.phoneNumber.label',
        'Phone Number',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.addContactBtn.label',
        'Add Contact',
        '',
        'text', 'scalar', sysdate, 'doej');

insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.Emergency.label',
        'Emergency contacts',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.MissingPerson.label',
        'Misssing person contacts',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.SchoolAddress.label',
        'School Address',
        '',
        'text', 'scalar', sysdate, 'doej');
-- insert into cm_config (config_application, config_category, config_key,
--                        config_value,
--                        config_desc,
--                        config_data_type, config_data_structure, activity_date, activity_user)
-- values ('PersonContactInfo', 'Contact Info Review', 'checkList.phoneNumber.label',
--         'PhoneNumber',
--         '',
--         'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.EmergencyConfirm.label',
        'My emergency contact information is correct',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.MissingPersonConfirm.label',
        'My missing person contact information is correct',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.SchoolAddressConfirm.label',
        'My school address information is correct',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.phoneNumberConfirm.label',
        'My phone number information is correct',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'todo.emergencyContacts.label',
        'Emergency Contacts',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'todo.missingPersonContacts.label',
        'Missing Person Contacts',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'todo.schoolAddress.label',
        'School Address',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'todo.phoneNumber.label',
        'Cell Phone Number',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'about.title',
        'Getting Started',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.title',
        'Emergency Contacts',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.addContact.heading',
        'Add a Contact',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.addContact.message',
        '<p>Provide a name, number and optional relation for the new contact. To make a correction, delete the current entry and add the correct information.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.title',
        'Missing Person Contacts',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.addContact.heading',
        'Add a Contact',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.addContact.message',
        '<p>Provide a name and number for the new contact. To make a correction, delete the current entry and add the correct information.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.title',
        'Cell Phone Number',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.addPhone.heading',
        'Add a Cell Phone',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'schoolAddress.title',
        'School Address',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'completeModal.pageTitle',
        'Review Complete!',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'completeModal.title',
        'Review Complete!',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'completeModal.content',
        '<p>Your contact information review has been completed for this semester.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'BannerID.label',
        'Banner ID',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'main.appTitle',
        'Contact Information',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.currentContactTable.summary',
        'This table contains a list of your current emergency contacts, including name, relation (if provided) and phone number.',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContact.currentContactTable.caption',
        'Your current emergency contacts.',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPerson.currentContactTable.summary',
        'This table contains a list of your current missing person contacts, including name and phone number:',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPerson.currentContactTable.caption',
        'Your current missing person contacts.',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPerson.currentEmergencyContactsTable.summary',
        'This table contains a list of your current emergency contacts, which will be used for missing person contacts..',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPerson.currentEmergencyContactsTable.caption',
        'Your current emergency contacts.',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.reviewStatus.label',
        'Status:',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checkList.message',
        '<p>Confirm each item listed below. Once all items are confirmed, click "Complete Review" to complete the process.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'home.studentReviewInstructions',
        '<p>Students are required to review and confirm their contact information each semester. Please review each section, make any changes and check the box to indicate your confirmation. After all sections are confirmed, you will be able to complete the review.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.noEndDateMsg',
        '(No end date)',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.dateMsg',
        'Address active range: ',
        '',
        'text', 'scalar', sysdate, 'doej');

insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'Requiredfield.label',
        '* Indicates required fields',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checklist.noPendingReview.message',
        '<p>You do not have any required review actions at this time.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyTable.Name.label',
        'Name',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyTable.Relation.label',
        'Relation',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyTable.Phone.label',
        'Phone',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingpersonTable.Name.label',
        'Name',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingpersonTable.Relation.label',
        'Relation',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingpersonTable.Phone.label',
        'Phone',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.UseAsCell.label',
        'use as cell phone',
        '',
        'text', 'scalar', sysdate, 'doej');

insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPersonContact.nonStudentIntroMessage',
        '<p>Missing person information is applicable only for <em>Students</em>. If you are a student and need to enter this information, Please contact Office.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'checklist.reviewCompleted.message',
        '<em>Thanks!</em> You have successfully completed your review. You may close this window.',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'emergencyContacts.reviewActionInstructions.student',
        '<p>You must provide at least one emergency contact. If your contact information needs updated, use the "Add a Contact" option below to add a new contact. Once your information is accurate, check the box below to ccomplete this step.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'missingPerson.reviewActionInstructions.student',
        '<p>Either provide a missing person contact or confirm use of your having emergency contacts which will be used for this purpose.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'localAddress.reviewActionInstructions.student',
        '<p>Oxford residential students should have either a residence hall address or an address in Oxford. Check the box below to confirm your address above or enter a new address and then check the box to complete this step.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'phoneList.reviewActionInstructions.student',
        '<p>Miami encourages students to provide a cell phone number for university business. Providing a cell phone number is optional and you may complete this step without doing so.</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'schoolAddress.otherAddress.heading',
        'Addresses',
        '',
        'text', 'scalar', sysdate, 'doej');
insert into cm_config (config_application, config_category, config_key,
                       config_value,
                       config_desc,
                       config_data_type, config_data_structure, activity_date, activity_user)
values ('PersonContactInfo', 'Contact Info Review', 'schoolAddress.otherAddress.message',
        '<p>Students may not have a residence hall or local address for various reasons, including commuting, taking distance learning classes or studying abroad. If you have concerns regarding your status or address, please contact the <a href="https://miamioh.edu/onestop/" target="_blank">One Stop</a> (will open in new window).</p>',
        '',
        'text', 'scalar', sysdate, 'doej');
