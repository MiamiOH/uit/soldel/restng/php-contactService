--pledge table
create table safmgr.pledge (
  id integer generated always as identity,
  uniqueid VARCHAR2(8) not null,
  type VARCHAR2(200) not null,
  response VARCHAR2(200) not null,
  response_time DATE not null
);
comment on table safmgr.pledge
  is 'Capture responses to the pledge';
comment on column safmgr.pledge.id
  is 'Unique record identifier';
comment on column safmgr.pledge.uniqueid
  is 'UniqueId of the person who submitted a pledge response';
comment on column safmgr.pledge.type
  is 'Pledge type';
comment on column safmgr.pledge.response
  is 'Response given to the pledge';
comment on column safmgr.pledge.response_time
  is 'Time pledge response was submitted';

commit;


grant select,insert,update,delete on safmgr.pledge to muws_gen;

grant select on safmgr.pledge to mur_reg;

--test insert:
--insert into safmgr.pledge (uniqueid, type, response, response_time)
--  values ('millse','oxf-student','Agree',SYSDATE);
