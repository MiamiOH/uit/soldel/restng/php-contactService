alter table safmgr.person_contact_activity add (campus VARCHAR2(30));

update safmgr.person_contact_activity set campus = 'Oxford' where campus is null;
