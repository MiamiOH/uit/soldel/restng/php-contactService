
ALTER TABLE safmgr.attendance_intention
    ADD additional_choice_info varchar2(200);


comment on column safmgr.attendance_intention.additional_choice_info
    is 'Additional information related to the selected choice.';

commit;
