BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE safmgr.attendance_intention';
EXCEPTION
    WHEN OTHERS THEN
        IF SQLCODE != -942 THEN
            RAISE;
        END IF;
END;
/

--attendance_intention table
create table safmgr.attendance_intention (
  id integer generated always as identity,
  uniqueid VARCHAR2(8) not null,
  intention_type VARCHAR2(200) not null,
  choice VARCHAR2(200) not null,
  delayed_start_date DATE,
  response_time DATE not null
);

comment on table safmgr.attendance_intention
  is 'Capture intent for mode of attendance';
comment on column safmgr.attendance_intention.id
  is 'Unique record identifier';
comment on column safmgr.attendance_intention.uniqueid
  is 'UniqueId of the person who submitted their intention';
comment on column safmgr.attendance_intention.intention_type
  is 'Type of intention that is being signed';
comment on column safmgr.attendance_intention.choice
  is 'Choice made on what the attendance intention is';
comment on column safmgr.attendance_intention.delayed_start_date
  is 'Nullable field that may contain a date for a delayed start if that intention is chosen';
comment on column safmgr.attendance_intention.response_time
  is 'Date and time that the intention for attendance was submitted';

commit;


grant select,insert,update,delete on safmgr.attendance_intention to muws_gen;

grant select on safmgr.attendance_intention to mur_reg;

--test insert:
--insert into safmgr.attendance_intention (uniqueid, choice, delayed_start_date, response_time)
--  values ('millse','delayed_start','06-SEP-20',SYSDATE);
