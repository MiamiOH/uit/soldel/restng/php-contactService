BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE safmgr.person_contact_activity';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/

drop sequence safmgr.person_contact_activity_seq;

create sequence safmgr.person_contact_activity_seq;

CREATE TABLE safmgr.person_contact_activity
 (
   status_id number primary key,
   PIDM NUMBER(8,0) NOT NULL ENABLE,
   ADD_DATE DATE NOT NULL ENABLE,
   REVIEW_DATE DATE,
   REVIEW_STATUS VARCHAR2(24 BYTE) NOT NULL ENABLE,
   review_type varchar2(16) not null enable
);

create index safmgr.person_contact_activity_pidm on safmgr.person_contact_activity (pidm);

create index safmgr.person_contact_activity_type on safmgr.person_contact_activity (review_type);

grant all on safmgr.person_contact_activity to baninst1 with grant option;
grant select on safmgr.person_contact_activity_seq to baninst1;

grant select,insert,update,delete on safmgr.person_contact_activity to MUWS_GEN_RL;
grant select,insert,update,delete on safmgr.person_contact_activity to MUWS_SEC_RL;
grant select on safmgr.person_contact_activity_seq to MUWS_GEN_RL;
grant select on safmgr.person_contact_activity_seq to MUWS_SEC_RL;
