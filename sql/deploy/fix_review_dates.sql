-- Apply review date to complete records which do not have one.
-- These were created when people review their info when not actually
-- to do so. The missing review date causes the review process to
-- skip these records in the future. This is a one time fix to be
-- done after the code is updated to address this issue.
update safmgr.person_contact_activity set review_date = add_date
  where review_status = 'complete' and review_date is null;